FROM node:lts-alpine

ENV NODE_ENV=production

COPY ./dist/patch.js /home/node/app/patch.js

WORKDIR /home/node/app
CMD [ "node", "patch.js" ]