FROM node:lts-alpine

ENV NODE_ENV=production

COPY ./dist/auth.js /home/node/app/auth.js

WORKDIR /home/node/app
CMD [ "node", "auth.js" ]