FROM node:lts-alpine

ENV NODE_ENV=production

EXPOSE 11000
EXPOSE 11001
EXPOSE 12000
EXPOSE 12001

COPY ./dist/gate.js /home/node/app/gate.js

WORKDIR /home/node/app
CMD [ "node", "gate.js" ]