FROM node:lts-alpine

ENV NODE_ENV=production

COPY ./dist/world.js /home/node/app/world.js

WORKDIR /home/node/app
CMD [ "node", "world.js" ]