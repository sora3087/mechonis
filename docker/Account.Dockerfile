FROM node:lts-alpine

ENV NODE_ENV=production

COPY ./dist/account.js /home/node/app/account.js
COPY ./e2default.bin /home/node/app/e2default.bin

WORKDIR /home/node/app
CMD [ "node", "account.js" ]