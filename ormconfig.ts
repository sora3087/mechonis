import { ConnectionOptions } from 'typeorm'

const config: ConnectionOptions = {
  "name": "default",
  "type": "mysql",
  "host": "127.0.0.1",
  "port": 3306,
  "username": "mechonis",
  "password": "phantasy",
  "database": "mechonis",
  "synchronize": true,
  "logging": false,
  "entityPrefix": "mechonis_",
  "entities": [
    "src/DB/entities/*.ts"
  ],
  "migrationsTableName": "mechonis_migrations",
  "migrations": [
    "src/DB/migrations/*.ts"
  ],
  "cli": {
    "entitiesDir": "src/DB/entities",
    "migrationsDir": "src/DB/migrations"
  }
}

export default config