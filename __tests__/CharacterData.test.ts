import { readFile } from 'fs'
import { promisify } from 'util'
import { PlayerLanguage } from '../src/Account/classes/BlockedGuildcard'
import { CharacterData } from '../src/Account/packets/CharacterData'
import { CharacterClass } from '../src/Common/enum/CharacterClass'
import { SectionId } from '../src/Common/enum/SectionId'
import { ItemType } from '../src/Common/enum/ItemType'
import { hexy } from 'hexy'

let character: CharacterData

beforeAll(async () => {
  character = new CharacterData(await promisify(readFile)('__tests__/Vision.bin', {}))
})

describe('character', () => {
  test('serialnumber', () => {
    expect(character.serialNumber).toBe(42000002)
  })
  test('name', () => {
    expect(character.name).toBe('\tEVision')
  })
  test('section', () => {
    expect(character.sectionId).toBe(SectionId.Whitill)
  })
  test('class', () => {
    expect(character.class).toBe(CharacterClass.HUcast)
  })
  test('desc', () => {
    expect(character.guildcardDesc).toBe('')
  })
  test('teamname', () => {
    expect(character.teamName).toBe('')
  })
  test('infoboard', () => {
    expect(character.infoboard).toBe('')
  })
  test('autoreply', () => {
    expect(character.autoreply).toBe('')
  })
})

describe('inventory', () => {
  test('item count', () => {
    expect(character.inventory.itemCount).toBe(30)
  })
  test('hp mats', () => {
    expect(character.inventory.hpMats).toBe(0)
  })
  test('tp mats', () => {
    expect(character.inventory.tpMats).toBe(0)
  })
  test('language', () => {
    expect(character.inventory.language).toBe(PlayerLanguage.Japanese)
  })
  test.each([
    [0, true],
    [1, true],
    [2, true],
    [3, true],
    [4, true],
  ])('equipped', (i: number, expected: boolean) => {
    expect(character.inventory.items[i].equipped).toBe(expected)
  })
  test.each([
    [0, ItemType.Consumable],
    [1, ItemType.Consumable],
    [2, ItemType.Consumable],
    [3, ItemType.Consumable],
    [4, ItemType.Consumable],
  ])('inventory item', (i: number, expected: ItemType) => {
    expect(character.inventory.items[i].itemType).toBe(expected)
  })
})

// describe('stats', () => {

// })
