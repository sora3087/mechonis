import { PSOEncryption } from '../src/Gate/classes/PSOEncryption'
import { randomBufferFill } from '../src/Util/Buffer'
import { WelcomePacket } from '../src/Auth/packets/WelcomePacket'
import { LoginRedirect } from '../src/Auth/packets/LoginRedirect'

const serverIV = Buffer.alloc(4)
randomBufferFill(serverIV)
const clientIv = Buffer.from(serverIV)

const serverCipher = new PSOEncryption(serverIV)
const clientCipher = new PSOEncryption(clientIv)

const welcomePacket = new WelcomePacket("Hello and welcome to Mechonis Server")
const redirectPacket = new LoginRedirect('127.0.0.1', 11001)

test.each([
  Buffer.from([0x04, 0x00, 0x04, 0x00]),
  welcomePacket.buffer,
  redirectPacket.buffer
])('packet chain', (input: Buffer) => {
  const expected = Buffer.from(input)
  serverCipher.encrypt(input)
  clientCipher.decrypt(input)
  expect(input).toStrictEqual(expected)
})
