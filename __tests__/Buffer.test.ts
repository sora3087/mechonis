import { bufferGrow, byteAlign } from '../src/Util/Buffer'

const PSORequestLogin = Buffer.from([0x04, 0x00, 0x04, 0x04])
const PSORedirect = Buffer.from([0x0c, 0x00, 0x14, 0x00,
  0x7f, 0x00, 0x00, 0x01,
  0x2a, 0xf9, 0x00, 0x00])

describe('byte alignment', () => {
  
  test('length0', () => {
    const buf = byteAlign(Buffer.alloc(0), 8)
    expect(buf.length).toEqual(0)
  })
  test('length1', () => {
    const buf = byteAlign(Buffer.alloc(1), 8)
    expect(buf.length).toEqual(8)
  })
  test('length2', () => {
    const buf = byteAlign(Buffer.alloc(2), 8)
    expect(buf.length).toEqual(8)
  })
  test('length3', () => {
    const buf = byteAlign(Buffer.alloc(3), 8)
    expect(buf.length).toEqual(8)
  })
  test('length4', () => {
    const buf = byteAlign(Buffer.alloc(4), 8)
    expect(buf.length).toEqual(8)
  })
  test('length5', () => {
    const buf = byteAlign(Buffer.alloc(5), 8)
    expect(buf.length).toEqual(8)
  })
  test('length6', () => {
    const buf = byteAlign(Buffer.alloc(6), 8)
    expect(buf.length).toEqual(8)
  })
  test('length7', () => {
    const buf = byteAlign(Buffer.alloc(7), 8)
    expect(buf.length).toEqual(8)
  })
  test('length8', () => {
    const buf = byteAlign(Buffer.alloc(8), 8)
    expect(buf.length).toEqual(8)
  })
  describe('pso login', () => {
    let buf = Buffer.from(PSORequestLogin)
    buf = byteAlign(buf, 4)
    test('len', () => {
      expect(buf.length).toEqual(4)
    })
    test('data', () => {
      expect(buf).toStrictEqual(PSORequestLogin)
    })
  })
  describe('pso redirect', () => {
    let buf = Buffer.from(PSORedirect)
    buf = byteAlign(buf, 4)
    test('len', () => {
      expect(buf.length).toEqual(12)
    })
    test('data', () => {
      expect(buf).toStrictEqual(PSORedirect)
    })
  })
  describe('grow', () => {
    let buf = Buffer.from(PSORequestLogin)
    let exp = Buffer.alloc(10)
    PSORequestLogin.copy(exp)
    buf = bufferGrow(buf, 10)
    test('len', () => {
      expect(buf.length).toEqual(10)
    })
    test('data', () => {
      expect(buf).toStrictEqual(exp)
    })
  })
})