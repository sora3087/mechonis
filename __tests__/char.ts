import { hexy } from 'hexy'
import { createConnection } from 'typeorm'
import { InventoryWeapon } from '../src/Account/classes/InventoryWeapon'
import { CharacterData } from '../src/Account/packets/CharacterData'
import { ItemList } from '../src/Common/dictionaries/ItemList'
import { WeaponSpecial } from '../src/Common/enum/WeaponSpecial'

let guildcard: number
let slot: number
if (process.argv[2] && process.argv[3]) {
  guildcard = parseInt(process.argv[2], 10)
  slot = parseInt(process.argv[3], 10)
}

if (!guildcard) {
  console.log('no input file specified')
  process.exit()
}

console.log(guildcard)

createConnection({
  type: 'mysql',
  host: '127.0.0.1',
  port: 3306,
  username: 'root',
  password: 'dev',
  database: 'tethealla',
  logging: true
})
.catch(err => {
  console.log('Could not connect to mysql')
})
.then(async (db) => {
  if (db) {
    const rows: Array<{data: Buffer}> = await db.manager
    .query(
      'SELECT data FROM character_data WHERE guildcard = ? AND slot = ? LIMIT 0, 1',
      [guildcard, slot]
    )
    if (rows[0]) {
      const char = new CharacterData(rows[0].data)
      char.inventory.items.forEach(item => {
        console.log(
          item.constructor.name,
          ((item instanceof InventoryWeapon && item.special !== WeaponSpecial.None)
            ? WeaponSpecial[item.special] + ' ' :
            ''
          ) +
          ItemList[item.itemIdentifier]
        )
        console.log(item.getSerialized())
        console.log(hexy(item.buffer))
      })
    } else {
      console.log('could not find specified character')
    }
    db.close()
  }
})
