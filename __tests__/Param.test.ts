
import { readFileSync } from 'fs'
import { InitParamChunks, ParamHeader } from '../src/Account/routes/GetParamFiles'

const fileNames = [
  'param/ItemMagEdit.prs',
  'param/ItemPMT.prs',
  'param/BattleParamEntry.dat',
  'param/BattleParamEntry_on.dat',
  'param/BattleParamEntry_lab.dat',
  'param/BattleParamEntry_lab_on.dat',
  'param/BattleParamEntry_ep4.dat',
  'param/BattleParamEntry_ep4_on.dat',
  'param/PlyLevelTbl.prs',
]

let Header: Buffer
beforeAll(async () => {
  Header = readFileSync('__tests__/packets/TethParamHeader.bin')
  await InitParamChunks(fileNames)
})

test('header', () => {
  expect(ParamHeader.buffer).toEqual(Header)
})
