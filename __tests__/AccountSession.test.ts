import { AccountSession } from '../src/Account/classes/AccountSession'
import { PlayerLanguage } from '../src/Account/classes/BlockedGuildcard'
import { CharacterClass } from '../src/Common/enum/CharacterClass'
import { SectionId } from '../src/Common/enum/SectionId'

const expectedRegistered = Buffer.alloc(0x1BC)
/**
 *  struct {
 *    uint32_t guildcard;     0x04
 *    uint16_t name[0x18];    0x30
 *    uint16_t team[0x10];    0x20
 *    uint16_t desc[0x58];    0xB0
 *    uint8_t reserved1;      0x01
 *    uint8_t language;       0x01
 *    uint8_t section;        0x01
 *    uint8_t ch_class;       0x01
 *    uint32_t padding;       0x04
 *    uint16_t comment[0x58]; 0xB0
 *  } entries[104]; 1BC
 */
expectedRegistered.writeUInt32LE(1000001, 0)
expectedRegistered.write('Tiavara', 0x04, 'utf16le')
expectedRegistered[0x105] = PlayerLanguage.English
expectedRegistered[0x106] = SectionId.Viridia
expectedRegistered[0x107] = CharacterClass.FOmar

/**
 * struct {
 *   uint32_t guildcard;  0x04
 *   uint16_t name[0x18]; 0x30
 *   uint16_t team[0x10]; 0x20
 *   uint16_t desc[0x58]; 0xB0
 *   uint8_t reserved1;   0x01
 *   uint8_t language;    0x01
 *   uint8_t section;     0x01
 *   uint8_t ch_class;    0x01
 * } blocked[29];
 */
const expectedBlocked = Buffer.alloc(0x108)
expectedBlocked.writeUInt32LE(1000002, 0)
expectedBlocked.write('Jerkwad', 0x04, 'utf16le')
expectedBlocked[0x108 - 3] = PlayerLanguage.Spanish
expectedBlocked[0x108 - 2] = SectionId.Whitill
expectedBlocked[0x108 - 1] = CharacterClass.RAmar

const gcDataLength =
  0x114
  + (0x108 * 29)
  + 0x78
  + (0x1BC * 104)
  + 0x1BC/*(105th registered user?)*/

describe('add registered', () => {
  const account = new AccountSession()
  account.registered[0].name = 'Tiavara'
  account.registered[0].guildcard = 1000001
  account.registered[0].class = CharacterClass.FOmar
  account.registered[0].section = SectionId.Viridia
  account.registered[0].launguage = PlayerLanguage.English

  test('formatted buffer', () => {
    expect(account.registered[0].buffer).toStrictEqual(expectedRegistered)
  })
  test('full data', () => {
    const gcData = Buffer.alloc(gcDataLength)
    expectedRegistered.copy(gcData, 0x114 + (0x108 * 29) + 0x78)
    expect(account.guildcardData).toStrictEqual(gcData)
  })
})

describe('add blocked', () => {
  const account = new AccountSession()
  account.blocked[0].name = 'Jerkwad'
  account.blocked[0].guildcard = 1000002
  account.blocked[0].class = CharacterClass.RAmar
  account.blocked[0].section = SectionId.Whitill
  account.blocked[0].launguage = PlayerLanguage.Spanish

  test('formatted buffer', () => {
    expect(account.blocked[0].buffer).toStrictEqual(expectedBlocked)
  })
  test('full data', () => {
    const gcData = Buffer.alloc(gcDataLength)
    expectedBlocked.copy(gcData, 0x114)
    expect(account.guildcardData).toStrictEqual(gcData)
  })
})
