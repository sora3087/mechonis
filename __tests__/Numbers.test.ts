import { BBTable } from '../src/Gate/classes/BBTable'
import { UInt16, UIntGetByte } from '../src/Util/NumberTypes'
import { swapBytes16 } from '../src/Util/Endianness'
import { roundIncrement } from '../src/Util/Numbers'

const p = [
  [0x243F6A88, 0x6A88],
  [0x85A308D3, 0x08D3],
  [0x13198A2E, 0x8A2E],
  [0x03707344, 0x7344],
  [0xA4093822, 0x3822],
  [0x299F31D0, 0x31D0],
  [0x082EFA98, 0xFA98],
  [0xEC4E6C89, 0x6C89],
  [0x452821E6, 0x21E6],
  [0x38D01377, 0x1377],
  [0xBE5466CF, 0x66CF],
  [0x34E90C6C, 0x0C6C],
  [0xC0AC29B7, 0x29B7],
  [0xC97C50DD, 0x50DD],
  [0x3F84D5B5, 0xD5B5],
  [0xB5470917, 0x0917],
  [0x9216D5D9, 0xD5D9],
  [0x8979FB1B, 0xFB1B],
]
test.each(p)('Uint32 -> UInt16', (a, expected) => {
  expect(UInt16(a)).toBe(expected)
})

const flipped = [
  [0x6A88, 0x886A],
  [0x08D3, 0xD308],
  [0x8A2E, 0x2E8A],
  [0x7344, 0x4473],
  [0x3822, 0x2238],
  [0x31D0, 0xD031],
  [0xFA98, 0x98FA],
  [0x6C89, 0x896C],
  [0x21E6, 0xE621],
  [0x1377, 0x7713],
  [0x66CF, 0xCF66],
  [0x0C6C, 0x6C0C],
  [0x29B7, 0xB729],
  [0x50DD, 0xDD50],
  [0xD5B5, 0xB5D5],
  [0x0917, 0x1709],
  [0xD5D9, 0xD9D5],
  [0xFB1B, 0x1BFB],
]
test.each(flipped)('UInt16 swap', (a, expected) => {
  expect(swapBytes16(a)).toBe(expected)
})
test.each([
  [0, 0x69],
  [1, 0x8a],
  [2, 0x54],
  [3, 0xac]
])('UInt Get Byte', (byte, expected) => {
  expect(UIntGetByte(0xac548a69, byte)).toBe(expected)
})

test.each([
  [0, 0],
  [1, 8],
  [2, 8],
  [3, 8],
  [4, 8],
  [5, 8],
  [6, 8],
  [7, 8],
  [8, 8],
  [9, 16],
])('increment align number', (amount, expected) => {
  expect(roundIncrement(amount,8)).toBe(expected)
})