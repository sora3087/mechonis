import { PSOEncryption } from '../src/Gate/classes/PSOEncryption'

let serverCipher: PSOEncryption
let clientCipher: PSOEncryption

beforeAll(() => {
  const clientIV = Buffer.alloc(4)
  clientIV.writeUInt32LE(0xD93353A0, 0)
  serverCipher = new PSOEncryption(clientIV)
  const serverIV = Buffer.alloc(4)
  serverIV.writeUInt32LE(0x45904987, 0)
  clientCipher = new PSOEncryption(serverIV)
})

test('server send', () => {
  const data = Buffer.from([0x4, 0x0, 0x4, 0x0])
  serverCipher.encrypt(data)
  expect(data).toEqual(Buffer.from([0x02, 0xf4, 0xee, 0x65]))
})

test('server recieve', () => {
  const data = Buffer.from([0x84, 0x99, 0x8B, 0x6D])
  clientCipher.decrypt(data)
  expect(data).toEqual(Buffer.from([0x4, 0x0, 0x2, 0x0]))
})