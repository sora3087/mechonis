const path = require('path');

module.exports = {
  // node: {
  //   console: true,
  //   global: true,
  //   process: true,
  //   __filename: 'mock',
  //   __dirname: 'mock',
  //   Buffer: true,
  //   setImmediate: true,
  //   net: false
  // },
  target: 'node',
  mode: 'production',
  entry: {
    'gate': './src/Gate/index.ts',
    'patch': './src/Patch/index.ts',
    'auth': './src/Auth/index.ts',
    'account': './src/Account/index.ts',
    'world': './src/World/index.ts'
  },
  stats: {warnings:false},
  // devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    hot: true
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  // output: {
  //   filename: 'bundle.js',
  //   path: path.resolve(__dirname, './wp-content/themes/ccs/dist')
  // },
  optimization: {
    usedExports: true,
    chunkIds: 'natural',
    splitChunks: {
      // default splitChunks config
      chunks: 'async',
      minSize: 20000,
      // minRemainingSize: 0,
      minChunks: 1,
      maxAsyncRequests: 30,
      maxInitialRequests: 30,
      enforceSizeThreshold: 50000,
      cacheGroups: {
        defaultVendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          reuseExistingChunk: true,
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
      },
    }
  }
};