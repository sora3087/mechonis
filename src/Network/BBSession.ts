import { Socket } from 'net'
import BBCrypt from './BBCrypto'
import CommandRouter from './CommandRouter'
import * as colors from 'colors/safe'
import FIFOBuffer from '../Util/FIFOBuffer'
import Logger from '../Util/Logger'
import ISession from './ISession'

export default abstract class Session implements ISession {
  public get remoteAddress() {
    return this.socket.remoteAddress
  }
  public disconnectAfterSend: boolean = false
  public userName: string = ''
  protected clientCipher: BBCrypt
  protected serverCipher: BBCrypt
  protected socket: Socket
  protected encrypted: boolean = false
  private router: CommandRouter
  private incomingBuffer: FIFOBuffer
  private reading: boolean
  constructor(socket: Socket, router: CommandRouter) {
    this.socket = socket
    this.router = router
    this.incomingBuffer = new FIFOBuffer()
    socket.on('data', this.OnData)
  }
  public abstract SendHandshake(PacketTemplate: Buffer): void
  public Write(data: Buffer | Array<Buffer>) {
    if (!(data instanceof Buffer)) {
      data = Buffer.concat(data)
    }
    try {
      Logger.info(`\n[${colors.blue((new Date()).toISOString())}] ${colors.black(colors.bgGreen(' SEND '))} => ${this.socket.remoteAddress}`)
      if (data.length > 0x60) {
        Logger.debug(data.slice(0, 0x60))
        Logger.debug(`Buffer logging was truncated from ${data.length} bytes`)
      } else {
        Logger.debug(data)
      }
    } catch (e) {
      // the socket got disconnected unexpectedly
      Logger.error('Trying to send to a disconnected socket')
      return
    }

    if (this.encrypted) {
      if (data.length % 8 !== 0) {
        Logger.debug('Padding bytes for encryption')
        data = Buffer.concat([
          data,
          Buffer.alloc((8 - (data.length % 8)))
        ])
      }
      this.serverCipher.Encrypt(data, 0, data.length)
    }
    this.socket.write(data)
    if (this.disconnectAfterSend) {
      this.socket.end()
      this.socket.destroy()
    }
  }
  private recieveParse() {
    this.reading = true
    let peekBuf
    // tslint:disable-next-line:no-conditional-assignment
    while (peekBuf = this.incomingBuffer.peek(2)) {
      const messageLength = peekBuf.readUInt16LE(0)
      if (messageLength === 0 ) {
        if (this.incomingBuffer.size >= 2) {
          this.incomingBuffer.deq(2)
        } else {
          this.incomingBuffer.drain()
        }
        break
      }
      if (this.incomingBuffer.size < messageLength) {
        // message length includes size
        break
      }
      const message = this.incomingBuffer.deq(messageLength)
      Logger.verbose(`\n[${colors.blue((new Date()).toISOString())}] ${colors.black(colors.bgYellow(' PARSED '))} `)
      Logger.debug(message)
      // this.router.process(message, this)
    }
    this.reading = false
  }
  private OnData = (data: Buffer) => {
    if (this.encrypted) {
      this.clientCipher.Decrypt(data, 0, data.length)
    }
    // tslint:disable-next-line:max-line-length
    Logger.info(`\n[${colors.blue((new Date()).toISOString())}] ${colors.black(colors.bgRed(' RECV '))} <= ${this.socket.remoteAddress}`)
    Logger.debug(data)
    this.incomingBuffer.enq(data)
    if (!this.reading) {
      this.recieveParse()
    }
  }
}
