import { Socket } from 'net'
import {CommandRouter} from './CommandRouter'
import Crypto from './Crypto'
import { WelcomeTemplate } from '../Patch/Packets'
import * as colors from 'colors/safe'
import FIFOBuffer from '../Util/FIFOBuffer'
// import CONFIG from '../Patch/helpers/ConfigStore'
import Logger from '../Util/Logger'
// let { uploadRate } = CONFIG.all
let uploadRate = 600

let rateLimited = false

if (typeof uploadRate === 'number' && uploadRate > 10) {
  uploadRate = Math.ceil(uploadRate / 10)
  rateLimited = true
}

function msleep(n: number) {
  Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n)
}
function sleep(n: number) {
  msleep(n * 1000)
}

export default class Session {
  public disconnectAfterSend: boolean = false
  public userName: string = ''
  private socket: Socket
  private router: CommandRouter
  private serverCipher: Crypto
  private clientCipher: Crypto
  private encrypted: boolean = false
  private outgoingBuffer: FIFOBuffer
  private incomingBuffer: FIFOBuffer
  private writing: boolean
  private reading: boolean
  constructor(socket: Socket, router: CommandRouter) {
    this.socket = socket
    this.router = router
    this.outgoingBuffer = new FIFOBuffer()
    this.incomingBuffer = new FIFOBuffer()
    socket.on('data', this.OnData)
  }
  public SendHandshake() {
    const serverVector = (Math.round(Math.random() * Date.now()) & 0xffffffff) >>> 0
    const clientVector = (Math.round(Math.random() * Date.now()) & 0xffffffff) >>> 0
    const Handshake = Buffer.from(WelcomeTemplate)
    Handshake.writeUInt32LE(serverVector, 0x44)
    Handshake.writeUInt32LE(clientVector, 0x48)

    this.serverCipher = new Crypto()
    this.serverCipher.createKeys(serverVector)
    this.clientCipher = new Crypto()
    this.clientCipher.createKeys(clientVector)

    this.socket.write(Handshake)
    this.encrypted = true

  }
  public Write(data: Buffer | Array<Buffer>) {
    if (!(data instanceof Buffer)) {
      data = Buffer.concat(data)
    }
    try {
      Logger.verbose(`\n[${colors.blue((new Date()).toISOString())}] ${colors.black(colors.bgGreen(' SEND '))} => ${this.socket.remoteAddress}`)
      if (data.length > 0x60) {
        Logger.debug(data.slice(0, 0x60))
        Logger.debug(`Buffer logging was truncated from ${data.length} bytes`)
      } else {
        Logger.debug(data)
      }
    } catch (e) {
      // the socket got disconnected unexpectedly
      Logger.error('Trying to send to a disconnected socket')
      return
    }

    if (this.encrypted) {
      // pad bytes to mod 4
      if (data.length % 4 !== 0) {
        data = Buffer.concat([
          data,
          Buffer.from([].fill(0, (4 - (data.length % 4))))
        ])
      }
      this.serverCipher.cryptData(data, 0, data.length)
    }
    if (rateLimited) {
      this.outgoingBuffer.enq(data)
      if (!this.writing) {
        this.limitedWrite()
      }
    } else {
      this.socket.write(data)
    }
    if (this.disconnectAfterSend) {
      this.socket.end()
      this.socket.destroy()
    }
  }
  public get remoteAddress() {
    return this.socket.remoteAddress
  }
  private limitedWrite() {
    this.writing = true
    while (this.outgoingBuffer.size > 0) {
      if (this.outgoingBuffer.size < uploadRate) {
        this.socket.write(this.outgoingBuffer.drain())
      } else {
        this.socket.write(this.outgoingBuffer.deq(uploadRate))
        msleep(100)
      }
    }
    this.writing = false
  }
  private recieveParse() {
    this.reading = true
    let peekBuf
    // tslint:disable-next-line:no-conditional-assignment
    while (peekBuf = this.incomingBuffer.peek(2)) {
      const messageLength = peekBuf.readUInt16LE(0)
      if (this.incomingBuffer.size < messageLength) {
        // message length includes size
        break
      }
      const message = this.incomingBuffer.deq(messageLength)
      Logger.debug(`\n[${colors.blue((new Date()).toISOString())}] ${colors.black(colors.bgYellow(' PARSED '))} `)
      Logger.debug(message)
      // this.router.process(message.slice(2), this)
    }
    this.reading = false
  }
  private OnData = (data: Buffer) => {
    if (this.encrypted) {
      this.clientCipher.cryptData(data, 0, data.length)
    }
    // tslint:disable-next-line:max-line-length
    Logger.verbose(`\n[${colors.blue((new Date()).toISOString())}] ${colors.black(colors.bgRed(' RECV '))} <= ${this.socket.remoteAddress}`)
    Logger.debug(data)
    this.incomingBuffer.enq(data)
    if (!this.reading) {
      this.recieveParse()
    }
  }
}
