import axios from 'axios'
import { networkInterfaces } from 'os'
import Logger from '../Util/Logger'

export const getPublicIp = async () => {
  Logger.info('getting public ip')
  return axios.get('https://icanhazip.com')
    .then(resp => resp.data.replace('\n', ''))
    .catch(() => {
      return Promise.resolve(localIp())
    })
}

export const localIp = () => {
  let local = ''
  const interfaces = networkInterfaces()
  Object.keys(interfaces).forEach(key => {
    const adapter = interfaces[key].filter(inf => inf.family === 'IPv4')[0]
    if (adapter && adapter.cidr !== '127.0.0.1/8' && adapter.mac !== "00:00:00:00:00:00") {
      local = adapter.address
    }
  })
  return local
}
