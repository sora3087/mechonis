import { catchError, map, Observable, of } from 'rxjs'
import { HttpClient } from '../Network/HttpClient'
import Logger from '../Util/Logger'

export type Node = {
  ID: string
  Node: string
  Address: string
  Datacenter: string
  TaggedAddresses?: { [tag: string]: string },
  Meta?: { [key: string]: string }
}

export type Instance = {
  ID: string,
  Node: string,
  Address: string,
  Datacenter: string,
  TaggedAddresses: {[tag: string]: string},
  NodeMeta: {[key: string]: string},
  ServiceKind: string,
  ServiceID: string,
  ServiceName: string,
  ServiceTags: string[],
  ServiceAddress: string,
  ServiceTaggedAddresses: {[tag: string]: Address}
}

type Address = {
  Address: string
  Port: number
}

type Weight = {
  Passing: number
  Warning: number
}

export type ServiceRegistrationOptions = {
  Name: string
  Id?: string
  Tags?: Array<string>
  Address?: string
  TaggedAddresses?: { [tag: string]: Address }
  Meta?: { [key: string]: string }
  ns?: string
  Port?: number
  Kind?: string
  Proxy?: {
    destination_service_name: string
  }
  EnableTagOverride?: boolean
  Weights?: Array<Weight>
}

export type CheckOptions = {
  Name: string
  ServiceId?: string
  Status?: string
  DeregisterCriticalServiceAfter?: string
}

export type IntervalCheck = CheckOptions & {
  Interval: string
}

export type HTTPCheckOptions = IntervalCheck & {
  HTTP: string
  Method?: string
  Body?: string
}

export type TCPCheckOptions = IntervalCheck & {
  TCP: string
}

export type TTLCheckOptions = CheckOptions & {
  TTL: string
}

export type ProxyRegistrationOptions = ServiceRegistrationOptions & {
  Kind: 'connect-proxy'
  Proxy: {
    destination_service_name: string
  }
  Port: number
}

type ServiceCheck = TCPCheckOptions | HTTPCheckOptions | TTLCheckOptions

export type CheckedRegistrationOptions = ServiceRegistrationOptions & {
  Check: ServiceCheck
}

export type MutlipleCheckRegistrationOptions = ServiceRegistrationOptions & {
  Checks: Array<ServiceCheck>
}

type RegistrationOptionsType = ServiceRegistrationOptions
                               | ProxyRegistrationOptions
                               | CheckedRegistrationOptions
                               | MultiCacheQueryOptions

type CheckUpdateOptions = {
  Status: 'warning' | 'critical' | 'passing'
  Output: string
  ns?: string
}

type NodeFilters = {
  Address?: string
  Datacenter?: string
  ID?: string
  Node?: string
}

type ServiceResponse = { [serivce: string]: Array<string> }

const protocolReg = /^https?:\/\//

export class ConsulClient {
  private http: HttpClient
  constructor(host: string, port: number = 8500) {
    if (!host.match(protocolReg)) {
      host = 'http://' + host
    }
    Logger.info('Creating consul client for ' + host + `:${port}/v1`)
    this.http = new HttpClient(host + `:${port}/v1`)
  }
  public register(opts: RegistrationOptionsType): Observable<undefined> {
    return this.http.put<undefined>('/agent/service/register', {
      data: opts
    })
    .pipe(
      map(resp => resp.data)
    )
  }
  public deregister(id: string): Observable<undefined> {
    return this.http.put<undefined>(`/agent/service/deregister/${id}`)
    .pipe(
      map(resp => resp.data)
    )
  }
  public registerCheck(opts: ServiceCheck): Observable<undefined> {
    return this.http.put<undefined>('/agent/check/register', {
      data: opts
    })
    .pipe(
      map(resp => resp.data)
    )
  }
  public deregisterCheck(id: string): Observable<undefined> {
    return this.http.put<undefined>(`/agent/check/deregister/${id}`)
    .pipe(
      map(resp => resp.data)
    )
  }
  public check(status: 'warn' | 'pass' | 'fail', checkId: string): Observable<undefined> {
    return this.http.put<undefined>(`/agent/check/${status}/${checkId}`)
    .pipe(
      map(resp => resp.data)
    )
  }
  public checkPass(checkId: string): Observable<undefined> {
    return this.check('pass', checkId)
  }
  public checkWarn(checkId: string): Observable<undefined> {
    return this.check('warn', checkId)
  }
  public checkFail(checkId: string): Observable<undefined> {
    return this.check('fail', checkId)
  }
  public checkUpdate(checkId: string, opts: CheckUpdateOptions): Observable<undefined> {
    return this.http.put<undefined>(`/agent/check/update/${checkId}`, {
      data: opts
    })
    .pipe(
      map(resp => resp.data)
    )
  }
  public getNodes(): Observable<Node[]> {
    return this.http.get<Node[]>(`/catalog/nodes`/* , opts */)
    .pipe(
      map(resp => resp.data),
      catchError(err => {
        return of([])
      })
    )
  }
  public getServices(): Observable<ServiceResponse> {
    return this.http.get<ServiceResponse>('/catalog/services')
    .pipe(
      map(resp => resp.data),
      catchError(err => {
        return of({})
      })
    )
  }
  public serviceinstances(service: string): Observable<Array<Instance>> {
    return this.http.get<Array<Instance>>(`/catalog/service/${service}`)
    .pipe(
      map(resp => resp.data),
      catchError(err => {
        return of([])
      })
    )
  }
}
