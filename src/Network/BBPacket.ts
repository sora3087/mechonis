import { Packet } from './Packet'

export class BBPacket extends Packet {
  public get flags(): number {
    return this.buffer.readUInt32LE(0x04)
  }
  public set flags(val: number) {
    this.buffer.writeUInt32LE(val, 0x04)
  }
}
