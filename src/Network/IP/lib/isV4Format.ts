const ipv4Regex = /^(\d{1,3}\.){3,3}\d{1,3}$/

export const isV4Format = (ip: string) => {
  return ipv4Regex.test(ip);
};