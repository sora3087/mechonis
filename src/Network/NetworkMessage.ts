export type NetworkMessage = {
  session: string
  gate: string
  data: Buffer
  disconnect?: boolean
  redirect?: boolean
}
