import { createClient, RedisClientType } from 'redis'
import { User } from '../DB/entities/User'

const NAMESPACE = 'MECHONIS:'

export class RedisClient {
  public client: RedisClientType<any>
  public init(host: string, port: number = 6379): Promise<void> {
    this.client = createClient({
      url: `redis://${host}:${port}`
    })
    return this.client.connect()
  }
  public createSession(uuid: string, user: User): void {
    this.client.set(`${NAMESPACE}session:${uuid}`, JSON.stringify(user))
  }
  public getSession(uuid: string): Promise<User> {
    return this.client.get(`${NAMESPACE}session:${uuid}`)
      .then((user) => {
        const obj = JSON.parse(user)
        obj.controls = Buffer.from(obj.controls.data)
        return obj
      })
      .catch(() => Promise.resolve(null))
  }
  public deleteSession(uuid: string): Promise<number> {
    return this.client.del(`${NAMESPACE}session:${uuid}`)
  }
}

export const redis = new RedisClient()
