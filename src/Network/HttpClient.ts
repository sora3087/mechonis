import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'
import { from, Observable } from 'rxjs'

export type AxiosErrorResponse = AxiosResponse & { data: { Message: string } }

export class HttpClient {
  private client: AxiosInstance
  constructor(baseURL: string) {
    this.client = axios.create({
      baseURL,
      timeout: 60 * 1000
    })
  }
  public setToken(token: string): void {
    this.client.defaults.headers.common = {
      ...this.client.defaults.headers.common,
      Authorization: 'Bearer ' + token
    }
  }
  public get<ResponseType>(url: string):
  Observable<AxiosResponse<ResponseType>> {
    return this.execute<ResponseType>(url, {
      method: 'GET'
    })
  }
  public post<ResponseType>(url: string, config?: AxiosRequestConfig):
  Observable<AxiosResponse<ResponseType>> {
    const newConfig: AxiosRequestConfig = config || {}
    return this.execute<ResponseType>(url, {
      ...newConfig,
      method: 'POST'
    })
  }
  public put<ResponseType>(url: string, config?: AxiosRequestConfig):
  Observable<AxiosResponse<ResponseType>> {
    const newConfig: AxiosRequestConfig = config || {}
    return this.execute<ResponseType>(url, {
      ...newConfig,
      method: 'PUT'
    })
  }
  public delete<ResponseType>(url: string, config?: AxiosRequestConfig):
  Observable<AxiosResponse<ResponseType>> {
    const newConfig: AxiosRequestConfig = config || {}
    return this.execute<ResponseType>(url, {
      ...newConfig,
      method: 'DELETE'
    })
  }
  private execute<ResponseType>(url: string, config: AxiosRequestConfig):
  Observable<AxiosResponse<ResponseType>> {
    return from(
      this.client(url, config)
      .catch((reason) => {
        if (reason.response) {
          return Promise.reject(reason.response)
        }
        return Promise.reject({
          data: undefined
        }) as Promise<AxiosErrorResponse>
      })
    )
  }
}
