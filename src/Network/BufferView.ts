// to emulate structs
export abstract class BufferView {
  protected readonly size: number = 0
  constructor(protected _buffer: Buffer, protected offset: number) {}
  get buffer(): Buffer {
    return this._buffer.slice(this.offset, this.offset + this.size)
  }
  set buffer(val: Buffer) {
    if (val.length !== this.size) {
      throw new Error('Tried setting incorrect buffer size for ' + this.constructor.name)
    }
    val.copy(this._buffer, this.offset)
  }
  public readInt8(offset: number): number {
    return this._buffer.readInt8(offset + this.offset)
  }
  public readInt16LE(offset: number): number {
    return this._buffer.readInt16LE(offset + this.offset)
  }
  public readInt16BE(offset: number): number {
    return this._buffer.readInt16BE(offset + this.offset)
  }
  public readInt32LE(offset: number): number {
    return this._buffer.readInt16LE(offset + this.offset)
  }
  public readInt32BE(offset: number): number {
    return this._buffer.readInt16BE(offset + this.offset)
  }
  public writeInt8(value: number, offset: number): number {
    return this._buffer.writeInt8(value, offset + this.offset)
  }
  public writeInt16LE(value: number, offset: number): number {
    return this._buffer.writeInt16LE(value, offset + this.offset)
  }
  public writeInt16BE(value: number, offset: number): number {
    return this._buffer.writeInt16BE(value, offset + this.offset)
  }
  public writeInt32LE(value: number, offset: number): number {
    return this._buffer.writeInt16LE(value, offset + this.offset)
  }
  public writeInt32BE(value: number, offset: number): number {
    return this._buffer.writeInt16BE(value, offset + this.offset)
  }
  public readUInt8(offset: number): number {
    return this._buffer.readUInt8(offset + this.offset)
  }
  public readUInt16LE(offset: number): number {
    return this._buffer.readUInt16LE(offset + this.offset)
  }
  public readUInt16BE(offset: number): number {
    return this._buffer.readUInt16BE(offset + this.offset)
  }
  public readUInt32LE(offset: number): number {
    return this._buffer.readUInt16LE(offset + this.offset)
  }
  public readUInt32BE(offset: number): number {
    return this._buffer.readUInt16BE(offset + this.offset)
  }
  public writeUInt8(value: number, offset: number): number {
    return this._buffer.writeUInt8(value, offset + this.offset)
  }
  public writeUInt16LE(value: number, offset: number): number {
    return this._buffer.writeUInt16LE(value, offset + this.offset)
  }
  public writeUInt16BE(value: number, offset: number): number {
    return this._buffer.writeUInt16BE(value, offset + this.offset)
  }
  public writeUInt32LE(value: number, offset: number): number {
    return this._buffer.writeUInt16LE(value, offset + this.offset)
  }
  public writeUInt32BE(value: number, offset: number): number {
    return this._buffer.writeUInt16BE(value, offset + this.offset)
  }
  public slice(offset: number, end?: number): Buffer {
    return this._buffer.slice(offset + this.offset, end ? this.offset + end : undefined)
  }
  public write(string: string, encoding?: BufferEncoding): number
  public write(string: string, offset: number, encoding?: BufferEncoding): number
  public write(string: string, offset: number, length: number, encoding?: BufferEncoding): number
  public write(
    string: string,
    param1?: number | BufferEncoding,
    param2?: number | BufferEncoding,
    encoding?: BufferEncoding
  ): number {
    let offset: number = this.offset
    let length: number = string.length

    if (typeof param1 === 'string') {
      encoding = param1
    }
    if (typeof param2 === 'string') {
      offset = this.offset + (param1 as number)
      encoding = param2
    }
    if (encoding) {
      offset = this.offset + (param1 as number)
      length = param2 as number
    }

    return this._buffer.write(string, offset, length, encoding)
  }
}
