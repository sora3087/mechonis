import { bgGreen, black } from 'colors/safe'
import { hexy } from 'hexy'
import { Socket } from 'socket.io-client'
import Logger from '../Util/Logger'
import { GetPropertiesOf } from '../Util/GetPropertiesOf'
import { NetworkMessage } from './NetworkMessage'
import { Packet } from './Packet'

export type commandNextFn = (data: Buffer | Packet, disconnectAfter?: boolean, redirect?: boolean) => void

export type CommandRoute = (msg: NetworkMessage, next?: commandNextFn) => void

export class CommandRouter {
  private commandListeners: {
    [command: number]: Array<CommandRoute>
  } = {}
  private useListeners: Array<CommandRoute> = []
  public on(command: number, callback: CommandRoute): void {
    if (!this.commandListeners[command]) {
      this.commandListeners[command] = []
    }
    this.commandListeners[command].push(callback)
  }
  public use(callback: CommandRoute): void {
    this.useListeners.push(callback)
  }
  public nextFactory = (msg: NetworkMessage, socket: Socket): commandNextFn => {
    return (data: Packet, disconnect?: boolean, redirect?: boolean) => {
      Logger.verbose(`${black(bgGreen(` SEND ${data.constructor.name} `))} => ${msg.session}\ndisconnect?: ${msg.disconnect ? 'true' : 'false'}`)
      const props: any = GetPropertiesOf(data)
      Object.keys(props).forEach(key => {
        Logger.debug(`${key}: ${props[key] instanceof Buffer ? '\n' + hexy(props[key]) : props[key]}`)
      })
      socket.emit(redirect ? 'service-redirect' : 'service-respond', {
        ...msg,
        data: data.buffer,
        disconnect
      })
    }
  }
  public process(msg: NetworkMessage, socket: Socket): void {
    const listener = this.commandListeners[msg.data.readUInt16BE(2)] || this.commandListeners[msg.data[2]]
    if (listener) {
      listener.forEach((handler) => {
        Logger.info(`Router action ${handler.name} for ${socket.id}`)
        // always build a next func, there is no access to arguments in strict mode
        try {
          handler(msg, this.nextFactory(msg, socket))
        } catch (err) {
          Logger.error(`${handler.name} failed while handling message`)
          if (typeof err === 'object') {
            Logger.error(err.message)
            Logger.verbose(err.stack)
          } else {
            Logger.error(err)
          }
          this.nextFactory(msg, socket)(new Packet(8, 0), true)
        }
      })
    }
  }
  public get commands(): Array<number> {
    return Object.keys(this.commandListeners).map((command) => parseInt(command, 10))
  }
}
