import { roundIncrement } from '../Util/Numbers'

export interface IPacket {
  buffer: Buffer
  length: number
  code: number
}

export class Packet implements IPacket {
  public buffer: Buffer
  constructor(data: Buffer, byteAlign?: number)
  constructor(length: number, code?: number, subId?: number, byteAlign?: number)
  constructor(param: number | Buffer, codeOrAlign?: number, subId?: number, byteAlign?: number) {
    if (!(param instanceof Buffer)) {
      // if the param was a length then create a buffer
      const originalLength = param
      if (byteAlign && byteAlign > 1) {
        param = roundIncrement(param, byteAlign)
      }
      this.buffer = Buffer.alloc(param)
      this.buffer.writeUInt16LE(originalLength, 0)
      if (codeOrAlign) {
        this.buffer[2] = codeOrAlign
      }
      if (subId) {
        this.buffer[3] = subId
      }
    } else {
      if (codeOrAlign) {
        this.buffer = Buffer.alloc(roundIncrement(param.length, codeOrAlign))
        param.copy(this.buffer)
      } else {
        this.buffer = param
        if (this.length !== this.buffer.length) {
          throw new Error('Tried creating a packet with malformed packet lenght')
        }
      }
    }
  }
  get length(): number {
    return this.buffer.readUInt16LE(0)
  }
  get code(): number {
    return this.buffer[0x02]
  }
  set code(value: number) {
    this.buffer.writeUInt8(value, 0x02)
  }
  get subId(): number {
    return this.buffer[0x03]
  }
  set subId(value: number) {
    this.buffer.writeUInt8(value, 0x03)
  }
}
