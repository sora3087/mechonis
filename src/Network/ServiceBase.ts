import axios from 'axios'
import { bgRed, black } from 'colors/safe'
import { hexy } from 'hexy'
import { io, Socket } from 'socket.io-client'
import { v4 as uuidv4 } from 'uuid'
import Logger from '../Util/Logger'
import { CommandRouter } from './CommandRouter'
import { EncryptionType } from './EncryptionType'
import { IServiceRegistration } from './IServiceRegistration'
import { localIp } from './localIp'
import { NetworkMessage } from './NetworkMessage'
import { subnet } from 'ip'
import {Alvis} from './Alvis'
import { AlvisServiceInfo } from './types/AlvisServiceInfo'
import { AlvisRedisConfig } from './types/AlvisConfig'

const LOCAL_IP = localIp()

export type PortConfig = {
  type: EncryptionType, redirect: boolean
}

export type ServiceConfig = {
  serviceName: string,
  hostId: string,
  ports: Record<number, PortConfig>
  redis: AlvisRedisConfig
}

export class Service {
  public ports: { [port: number]: PortConfig } = {}
  protected gates: { [id: string]: Socket } = {}
  private serviceId: string
  private alvis: Alvis
  constructor(
    private config: ServiceConfig,
    private router: CommandRouter
  ) {
    process.title = `Mechonis ${config.serviceName} service`
    this.serviceId = uuidv4()
    this.ports = config.ports
    axios.get('https://icanhazip.com')
    .then(resp => resp.data.replace('\n', ''))
    .catch(() => {
      return Promise.resolve(LOCAL_IP)
    })
    .then(ip => {
      this.init(ip)
    })
  }
  public destroy(): Promise<boolean> {
    // unnecessary check deregister because we assigned the initial check as associated with the service ID
    // unregistering the service will remove associated checks
    return this.alvis.deregister()
  }
  public searchForGate(): void {
    Logger.verbose('Searching for gates to connect to')
    this.alvis.getLocalServices('gate') // this should typically only be one
    .then((gates) => {
      gates.forEach((info: AlvisServiceInfo) => {
        if (!this.gates[info.id]) {
          this.connectToGate(info)
        }
      })
    })
  }
  public connectToGate(info: AlvisServiceInfo): void {
    const {lan, wan} = info.taggedAddresses
    let address = wan.Address
    if (lan.Address === LOCAL_IP) {
      address = '127.0.0.1'
    } else if (
      lan
      && subnet(LOCAL_IP, '255.255.255.0')
      .contains(lan.Address)
    ) {
      address = lan.Address
    }

    Logger.verbose(`Connecting to gate: ${info.id} at http://${address}:3000`)
    const socket = io('http://' + address + ':3000')
    socket.on('error', (err) => {
      Logger.error(err)
    })
    socket.connect()
    socket.on('connect', () => {
      Logger.info('Connected to gate: ' + info.id)
      const serviceRegistration: IServiceRegistration = {
        name: this.config.serviceName,
        ports: this.ports,
        commands: this.router.commands
      }
      Logger.verbose('Registering with gate: ' + info.id)
      socket.emit('service-register', serviceRegistration, () => {
        Logger.info('Registered with gate: ' + info.id)
        if (info.id) {
          this.gates[info.id] = socket
        }
      })
      socket.on('disconnect', () => {
        socket.off('disconnect')
        socket.disconnect()
        Logger.warn('Disconnected from gate: ' + info.id)
        this.gates[info.id] = undefined
        delete this.gates[info.id]
      })
      socket.on('message', (msg: NetworkMessage) => {
        Logger.verbose(`${black(bgRed(' RECV '))} <=
session: ${msg.session}
disconnect?: ${msg.disconnect ? 'true' : 'false'}
data:`)
        Logger.debug('\n' + hexy(msg.data))
        this.router.process(msg, socket)
      })
    })
  }
  private async init(wan: string): Promise<void> {
    this.alvis = new Alvis({
      hostId: this.config.hostId,
      serviceName: this.config.serviceName,
      redis: this.config.redis,
      taggedAddresses: {
        wan: { Address: wan, Port: 3000 },
        lan: { Address: LOCAL_IP, Port: 3000 }
      }
    })
    await this.alvis.init().then(() => {
      this.searchForGate()
      setTimeout(() => this.searchForGate(), 60 * 1000)
      return this.alvis.register()
    })
  }
}
