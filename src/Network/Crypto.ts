export default class Crypto {
  private keys: Uint32Array
  private position: number

  constructor() {
    this.keys = new Uint32Array(56)
  }

  public createKeys(IV: number) {
    let index

    let key2 = 1
    this.keys[55] = IV

    for (let i1 = 0x15; i1 <= 0x46E; i1 += 0x15) {
        index = (i1 % 55)
        IV -= key2
        this.keys[index] = key2
        key2 = IV
        IV = this.keys[index]
    }

    this.mixKeys()
    this.mixKeys()
    this.mixKeys()
    this.mixKeys()
    this.position = 56
  }
  public mixKeys() {
    for (let i1 = 0x18, i2 = 0x01; i1 > 0; i1--, i2++) {
        this.keys[i2] -= this.keys[i2 + 0x1F]
    }
    for (let i1 = 0x1F, i2 = 0x19; i1 > 0; i1--, i2++) {
        this.keys[i2] -= this.keys[i2 - 0x18]
    }
  }
  public cryptData(data: Buffer, index: number, length: number) {
    length += index
    for (let i1 = index; i1 < length; i1 += 4) {
      this.cryptU32(data, i1)
    }
  }
  private UInt32GetByte(uint32: number, byteNum: number) {
    return uint32 >>> (byteNum * 8) & 0xff
  }
  private cryptU32(data: Buffer, offset: number) {
      if (this.position === 56) {
          this.mixKeys()
          this.position = 1
      }
      const key = this.keys[this.position]
      data[offset + 0] = (data[offset + 0] ^ this.UInt32GetByte(key, 0))
      data[offset + 1] = (data[offset + 1] ^ this.UInt32GetByte(key, 1))
      data[offset + 2] = (data[offset + 2] ^ this.UInt32GetByte(key, 2))
      data[offset + 3] = (data[offset + 3] ^ this.UInt32GetByte(key, 3))
      this.position++
  }
}
