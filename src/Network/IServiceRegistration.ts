import { EncryptionType } from './EncryptionType'

export interface IServiceRegistration {
  name: string
  ports: { [port: number]: {type: EncryptionType, redirect: boolean} }
  commands: Array<number>
}
