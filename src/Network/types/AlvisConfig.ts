export type AlvisTaggedAddress = {
  Address: string
  Port: number
}

export type AlvisRedisConfig = {
  host: string
  port?: number
  dc?: number
}

export type AlvisConfig = {
  serviceName: string
  serviceDesc?: string
  hostId: string
  taggedAddresses: Record<string, AlvisTaggedAddress>
  cacheServices?: boolean
  redis: AlvisRedisConfig
}
