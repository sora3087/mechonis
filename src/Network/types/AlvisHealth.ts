export enum AlvisHealthStatus {
  Healthy = 'Healthy',
  Unhealth = 'Unhealthy'
}

export type AlvisServiceHealth = {
  health: AlvisHealthStatus,
  startTime: string,
  updated: string
}