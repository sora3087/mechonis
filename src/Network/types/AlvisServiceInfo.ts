import { AlvisConfig } from './AlvisConfig';

export type AlvisServiceInfo = Omit<AlvisConfig, 'redis'> & {
  id: string
}
