import { createClient, RedisClientType } from 'redis'
import { v4 as uuidv4 } from 'uuid'
import Logger from '../Util/Logger'
import { AlvisConfig } from './types/AlvisConfig'
import { AlvisServiceHealth, AlvisHealthStatus } from './types/AlvisHealth'
import { AlvisServiceInfo } from './types/AlvisServiceInfo'

const SERVICE_TTL = 5 * 60 // 5 minutes
const SERVICE_CHECKIN = 4 * 60 // 4 minutes

const NAMESPACE = 'alvis:'

// enum RedisKeyEvent {
//   Set = 'set',
//   Del = 'del'
// }

const rediskeyReg = /^__key(event|space)@(\d+)__:([^$]+)/

export class Alvis {
  private redis: RedisClientType<any>
  private id: string
  private ttlTimer: NodeJS.Timer
  private serviceKey: string
  private startTime = new Date()
  private health: AlvisHealthStatus = AlvisHealthStatus.Healthy

  private cachedServices: AlvisServiceInfo[] = []

  constructor(private config: AlvisConfig) {
    this.ttlTimer = setTimeout(() => false, 1000 * 60 * 1000)
    this.id = uuidv4()
    this.redis = createClient({
      url: `redis://${config.redis.host}:${config.redis.port || 6379}`
    })
  }
  public init(): Promise<void> {
    return this.redis.connect()
  }
  public getClient(): RedisClientType<any> {
    return this.redis.duplicate()
  }
  public async register(): Promise<void> {
    const {serviceName, hostId} = this.config
    this.serviceKey = `${NAMESPACE}service:${serviceName}:${this.id}`
    const info: AlvisServiceInfo = {...this.config, id: this.id}
    const hset: [string, string, string] = [`${NAMESPACE}service:nodes`, this.id, JSON.stringify(info)]
    await Promise.all([
      this.redis.set(`${NAMESPACE}${serviceName}:service`, serviceName),
      this.redis.hSet(...hset),
      this.redis.sAdd(
        `${NAMESPACE}service:host:${hostId}`,
        this.id
      ),
      this.redis.set(`${this.serviceKey}:ttl`, this.id, {
        EX: SERVICE_TTL
      }),
      this.redis.set(`${this.serviceKey}:health`, JSON.stringify(this.getHealthInfo())),
      this.redis.set(this.serviceKey, JSON.stringify(info)),
    ])
    await this.getServices()
    // this.subscribe()
    this.schedule()
    return
  }
  public async deregister(): Promise<boolean> {
    clearInterval(this.ttlTimer)
    return Promise.all([
      this.redis.del(this.serviceKey),
      this.redis.hDel(`${NAMESPACE}service:nodes`, this.id),
      this.redis.del(`${this.serviceKey}:ttl`),
      this.redis.del(`${this.serviceKey}:health`),
      this.redis.sRem(`${NAMESPACE}service:host:${this.config.hostId}`, this.id)
    ])
    .then(() => true)
    .catch(() => false)
  }
  public async getServices(serviceName?: string): Promise<AlvisServiceInfo[]> {
    if (!this.redis.isOpen) {
      Logger.fatal('Redis connection is closed')
      process.exit()
      return
    }
    if (this.cachedServices.length > 0) {
      if (serviceName) {
        return Promise.resolve(this.getFilteredServices(serviceName))
      }
      return Promise.resolve(this.cachedServices)
    }
    return this.redis.hGetAll(`${NAMESPACE}service:nodes`).then(nodes => {
      const ret: AlvisServiceInfo[] = []
      for (const key in nodes) {
        if (Object.prototype.hasOwnProperty.call(nodes, key)) {
          const info = JSON.parse(nodes[key]) as AlvisServiceInfo
          ret.push(info)
        }
      }
      this.cachedServices = ret
      if (serviceName) {
        return Promise.resolve(this.getFilteredServices(serviceName))
      }
      return Promise.resolve(this.cachedServices)
    })
  }
  public getLocalServices(serviceName?: string): Promise<AlvisServiceInfo[]> {
    return this.getServices(serviceName)
    .then((services) => {
      return services.filter(service => {
        return (service.hostId === this.config.hostId && (!serviceName || service.serviceName === serviceName))
      })
    })
  }
  public setHealth(health: AlvisHealthStatus): void {
    this.health = health
  }
  public getHealthInfo(): AlvisServiceHealth {
    return {
      startTime: this.startTime.toISOString(),
      health: this.health,
      updated: (new Date()).toISOString()
    }
  }
  private getFilteredServices(name: string): AlvisServiceInfo[] {
    return this.cachedServices.filter(service => service.serviceName === name)
  }
  // private async subscribe(): Promise<void> {
  //   const subscriber = this.getClient()
  //   await subscriber.connect()
  //   subscriber.pSubscribe(
  //     [
  //       `__keyspace*__:${NAMESPACE}service:nodes`
  //     ], // subscribe to key events
  //     this.handleNewService
  //   )
  // }
  // private handleNewService = (event: string, key: string) => {
  //   const keyParse = rediskeyReg.exec(key)
  //   if (keyParse) {
  //     this.clearServiceCache()
  //     this.getServices()
  //   }
  // }
  // private removeCachedService(key: string): void {
  //   delete this.cachedServices[key]
  // }
  // private cacheService(key: string, service: AlvisServiceInfo): void {
  //   this.cachedServices[key] = service
  // }
  // private clearServiceCache(): void {
  //   this.cachedServices = []
  // }
  private schedule(): void {
    clearInterval(this.ttlTimer)
    this.ttlTimer = setInterval(() => this.ttl(), SERVICE_CHECKIN * 1000)
  }
  private ttl(): Promise<[boolean, string]> {
    return Promise.all([
      this.redis.expire(`${this.serviceKey}:ttl`, SERVICE_TTL),
      this.redis.set(`${this.serviceKey}:health`, JSON.stringify(this.getHealthInfo()))
    ])
  }
}
