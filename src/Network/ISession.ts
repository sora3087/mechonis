export default interface ISession {
  Write: (data: Buffer | Array<Buffer>) => void
  remoteAddress: string
}
