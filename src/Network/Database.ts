import { MongoClient, Collection, Db } from "mongodb";
import Logger from "../Util/Logger";

export interface IUserMeta {
  guildcard: number
  key: string
  value: any
}

export interface IUser {
  username: string
  password: string
  guildcard: number
  regTimestamp: number
  meta: { [key: string]: any }
}

export interface IDatabaseConnection {
  connect: (options: IDatabaseConnectionOptions) => Promise<any>
  getUsers: () => Promise<Array<IUser>>
  getUser: {
    (username: string): Promise<IUser>
    (guildcard: number): Promise<IUser>
  }
}

export interface IDatabaseConnectionOptions{
  host: string
  port?: number
  name: string
  user: string
  pass: string
}

export class DatabaseConnection implements IDatabaseConnection{
  private db: Db
  private users: Collection<IUser>
  private usermeta: Collection<IUserMeta>
  public connect(options: IDatabaseConnectionOptions) {
    const opts = {
      host: 'localhost',
      user: '',
      pass: '',
      name: 'mechonis',
      port: 27017,
      ...options
    }
    return MongoClient.connect(this.createConnectionString(opts))
    .then((client: MongoClient) => {
      this.db = client.db(opts.name)
      this.users = this.db.collection('users')
      this.usermeta = this.db.collection('usermeta')
    })
  }
  public getUsers(){
    return this.users.find({}).toArray()
  }
  public getUser(username: string): Promise<IUser>
  public getUser(guildcard: number): Promise<IUser>
  public getUser(param: string | number) {
    if (typeof param === 'number'){
      return this.users.findOne({ guildcard: param })
    } else {
      return this.users.findOne({ username: param })
    }
  }
  private createConnectionString(opts: IDatabaseConnectionOptions){
    Logger.debug(`mongodb://${(opts.user !== '')? `${opts.user}:${opts.pass}@`: ''}${opts.host}:${opts.port}/${opts.name}`)
    return `mongodb://${(opts.user !== '')? `${opts.user}:${opts.pass}@`: ''}${opts.host}:${opts.port}/${opts.name}`
  }
}

const Database = new DatabaseConnection()

export default Database