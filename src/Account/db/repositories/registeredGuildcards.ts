import { Repository } from 'typeorm'
import { dbConnection } from '../../../DB'
import { RegisteredGuildcard } from '../../../DB/entities/RegisteredGuildcard'

export let registeredGuildcards: Repository<RegisteredGuildcard>

dbConnection.then(db => {
  if (db) {
    registeredGuildcards = db.getRepository(RegisteredGuildcard)
  }
})
