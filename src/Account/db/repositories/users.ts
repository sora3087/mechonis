import { Repository } from 'typeorm'
import { dbConnection } from '../../../DB'
import { User } from '../../../DB/entities/User'

export let users: Repository<User>

dbConnection.then(db => {
  if (db) {
    users = db.getRepository(User)
  }
})
