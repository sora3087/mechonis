import { Repository } from 'typeorm'
import { dbConnection } from '../../../DB'
import { Character } from '../../../DB/entities/Character'

export let characters: Repository<Character>

dbConnection.then(db => {
  if (db) {
    characters = db.getRepository(Character)
  }
})
