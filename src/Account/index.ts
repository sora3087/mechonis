import { glob } from 'glob'
import { promisify } from 'util'
import { dbConnection } from '../DB'
import { EncryptionType } from '../Network/EncryptionType'
import { redis } from '../Network/RedisClient'
import { Service } from '../Network/ServiceBase'
import { createAccountRouter } from './router'
import { InitParamChunks } from './routes/GetParamFiles'

const SERVICE_NAME = 'account'

const {
  MECHONIS_MACHINE_ID,
  MECHONIS_REDIS_HOST
} = process.env

const start = async () => {
  await InitParamChunks([
    ...await promisify(glob)('param/*.prs'),
    ...await promisify(glob)('param/*.dat')
  ])
  await dbConnection
  await redis.init(MECHONIS_REDIS_HOST)
  const router = createAccountRouter()
  const service = new Service(
    {
      serviceName: SERVICE_NAME,
      hostId: MECHONIS_MACHINE_ID || '',
      redis: {
        host: MECHONIS_REDIS_HOST
      },
      ports: {
        12001: { type: EncryptionType.BB, redirect: false }
      }
    },
    router
  )

  const exit = async () => {
    const destroyed = await service.destroy()
    console.log('awaited destroy', destroyed)
    process.exit()
  }

  process.on('beforeExit', exit)
  process.on('SIGINT', exit)
}

start()
