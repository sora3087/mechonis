import { BlockedGuildcard } from './BlockedGuildcard'

// Blocked Guildcard with more fields
// uint32_t padding;
// uint16_t comment[0x58];

export class RegisteredGuildcard extends BlockedGuildcard {
  public static dataLength = 0x1BC
  constructor(buffer: Buffer, offset: number) {
    super(buffer, offset)
  }
  set comment(val: string) {
    // if val is longer than 0x58
    this._buffer.write(val, 0x10C, 'utf16le')
  }
}
