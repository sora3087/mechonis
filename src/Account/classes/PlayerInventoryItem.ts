import { ItemType } from '../../Common/enum/ItemType'
import { BufferView } from '../../Network/BufferView'

export class PlayerInventoryItem extends BufferView {
  protected readonly size = 0x1C
  get isSet(): boolean {
    return this.readUInt16LE(0) === 1
  }
  set isSet(val: boolean) {
    this.writeUInt16LE(val ? 1 : 0, 0)
  }
  get flags(): number {
    return this.readUInt32LE(0x04)
  }
  set flags(val: number) {
    this.writeUInt32LE(val, 0x04)
  }
  get equipped(): boolean {
    // equipped is 4th bit in flags
    return (this.flags & 0x08) !== 0
  }
  get itemIdentifier(): string {
    return this.slice(0x08, 0x08 + 3).toString('hex').toUpperCase()
  }
  get itemType(): ItemType {
    return this.readUInt8(0x08)
  }
  set itemType(val: ItemType) {
    this.writeUInt8(val, 0x08)
  }
  get itemId(): number {
    return this.readUInt8(0x09)
  }
  set itemId(val: number) {
    this.writeUInt8(val, 0x09)
  }
  get itemVariant(): number {
    return this.readUInt8(0x0A)
  }
  set itemVariant(val: number) {
    this.writeUInt8(val, 0x0A)
  }
  get itemIdx(): number {
    return this.readUInt32LE(0x14)
  }
  set itemIdx(val: number) {
    this.writeUInt32LE(val, 0x14)
  }
  public getSerialized(): string {
    return JSON.stringify(this.getSerializableObject(), null, 2)
  }
  protected getSerializableObject(): object {
    return {
      isSet: this.isSet,
      flags: this.flags,
      equipped: this.equipped,
      itemType: this.itemType,
      itemId: this.itemId,
      itemVariant: this.itemVariant
    }
  }
  // private getProperties(obj: object): any[] {
  //   const descriptors = Object.getOwnPropertyDescriptors.call(this, Object.getPrototypeOf(obj))
  //   return Object.keys(descriptors)
  //   .filter((name: string) => (name !== 'constructor' && typeof descriptors[name].get === 'function') )
  //   .map(name => {
  //     return descriptors[name].get()
  //   })
  // }
}
