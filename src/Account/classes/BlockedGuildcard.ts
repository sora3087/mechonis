import { CharacterClass } from '../../Common/enum/CharacterClass'
import { SectionId } from '../../Common/enum/SectionId'
import { BufferView } from '../../Network/BufferView'

export enum PlayerLanguage {
  Japanese,
  English,
  German,
  French,
  Spanish,
  ChineseSimplified,
  ChineseTraditional,
  Korean,
}

// uint32_t guildcard; 4
// uint16_t name[0x18]; 48
// uint16_t team[0x10]; 32
// uint16_t desc[0x58]; 176
// uint8_t reserved1; 1
// uint8_t language; 1 enum
// uint8_t section; 1 enum
// uint8_t ch_class; 1 enum

export class BlockedGuildcard extends BufferView {
  protected readonly size = 0x108
  constructor(buffer: Buffer, offset: number) {
    super(buffer, offset)
  }
  get guildcard(): number {
    return this._buffer.readUInt32LE(this.offset)
  }
  set guildcard(val: number) {
    this._buffer.writeUInt32LE(val, this.offset)
  }
  set name(val: string) {
    // if val is longer than 24
    this._buffer.write(val, this.offset + 0x04, 'utf16le')
  }
  set team(val: string) {
    // if val is longer than 16
    this._buffer.write(val, this.offset + 0x34, 'utf16le')
  }
  set desc(val: string) {
    // if val is longer than 88
    this._buffer.write(val, this.offset + 0x54, 'utf16le')
  }
  set launguage(val: PlayerLanguage) {
    this._buffer[this.offset + 0x105] = (val & 0xFF) >>> 0
  }
  set section(val: SectionId) {
    this._buffer[this.offset + 0x106] = (val & 0xFF) >>> 0
  }
  set class(val: CharacterClass) {
    this._buffer[this.offset + 0x107] = (val & 0xFF) >>> 0
  }
}
