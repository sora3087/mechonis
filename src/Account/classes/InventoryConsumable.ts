import { PlayerInventoryItem } from './PlayerInventoryItem';

export class InventoryConsumable extends PlayerInventoryItem {
  get qty(): number {
    return this.readUInt8(0x0D)
  }
  set qty(val: number) {
    this.writeUInt8(val, 0x0D)
  }
  protected getSerializableObject(): object {
    return {
      ...super.getSerializableObject(),
      qty: this.qty
    }
  }
}
