import { Character } from '../../DB/entities/Character'
import { User } from '../../DB/entities/User'
import { BlockedGuildcard } from './BlockedGuildcard'
import { RegisteredGuildcard } from './RegisteredGuildcard'

/**
 * From Sylverant <3
 * typedef struct bb_guildcard_data {
 *    uint8_t unk1[0x0114];
 *    struct {
 *        uint32_t guildcard;
 *        uint16_t name[0x18];
 *        uint16_t team[0x10];
 *        uint16_t desc[0x58];
 *        uint8_t reserved1;
 *        uint8_t language;
 *        uint8_t section;
 *        uint8_t ch_class;
 *    } blocked[29];
 *    uint8_t unk2[0x78];
 *    struct {
 *        uint32_t guildcard;
 *        uint16_t name[0x18];
 *        uint16_t team[0x10];
 *        uint16_t desc[0x58];
 *        uint8_t reserved1;
 *        uint8_t language;
 *        uint8_t section;
 *        uint8_t ch_class;
 *        uint32_t padding;
 *        uint16_t comment[0x58];
 *    } entries[104];
 *    uint8_t unk3[0x01BC];
 * } bb_gc_data_t;
 */

export class AccountSession {
  public lastChunkSent = -1
  public user: User
  public characters: Character[] = []
  public blocked: BlockedGuildcard[] = []
  public registered: RegisteredGuildcard[] = []
  public guildcardData: Buffer
  constructor() {
    this.guildcardData = Buffer.alloc(
      0x114
      + 0x108 * 29
      + 0x78
      + 0x1BC * 104
      + 0x1BC/*(105th registered user?)*/
    )
    let offset = 0x114
    this.characters.length = 4
    for (let i = 0; i < 29; i++) {
      this.blocked[i] = new BlockedGuildcard(this.guildcardData, offset)
      offset += 0x108
    }
    offset += 0x78
    for (let i = 0; i < 104; i++) {
      this.registered[i] = new RegisteredGuildcard(this.guildcardData, offset)
      offset += 0x1BC
    }
  }
}
