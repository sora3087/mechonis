import { PlayerInventoryItem } from './PlayerInventoryItem'
import { WeaponBonusType } from '../../Common/enum/WeaponBonusType'
import { WeaponSpecial } from '../../Common/enum/WeaponSpecial'

export type WeaponBonus = [WeaponBonusType, number]

export class InventoryWeapon extends PlayerInventoryItem {
  get bonuses(): WeaponBonus[] {
    const ret: WeaponBonus[] = []
    for (let i = 0; i < 3; i++) {
      const type = this.readUInt8(0x0E + i * 2)
      if (type > 0) {
        ret.push([type, this.readUInt8(0xf + i * 2)])
      }
    }
    return ret
  }
  set bonuses(val: WeaponBonus[]) {
    const bonuses = Buffer.alloc(6)
    val.forEach((bonus, id) => {
      bonuses[id * 2] = bonus[0]
      bonuses[1 + id * 2] = bonus[1]
    })
    bonuses.copy(this._buffer, this.offset + 0x0E)
  }
  get special(): WeaponSpecial {
    return this.readUInt8(0x0C)
  }
  set special(val: WeaponSpecial) {
    this.writeUInt8(val, 0x0C)
  }
  protected getSerializableObject(): object {
    return {
      ...super.getSerializableObject(),
      bonuses: this.bonuses,
      special: this.special
    }
  }
}
