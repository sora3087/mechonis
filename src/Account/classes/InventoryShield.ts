import { PlayerInventoryItem } from './PlayerInventoryItem'

export class InventoryShield extends PlayerInventoryItem {
  get dfpBonus(): number {
    return this.readUInt16LE(0x0E)
  }
  set dfpBonus(val: number) {
    this.writeUInt16LE(val, 0x0E)
  }
  get evpBonus(): number {
    return this.readUInt16LE(0x10)
  }
  set evpBonus(val: number) {
    this.writeUInt16LE(val, 0x10)
  }
  protected getSerializableObject(): object {
    return {
      ...super.getSerializableObject(),
      dfpBonus: this.dfpBonus,
      evpBonus: this.evpBonus,
    }
  }
}
