import { InventoryShield } from './InventoryShield'

export class InventoryFrame extends InventoryShield {
  get slots(): number {
    return this.readUInt8(0x0D)
  }
  set slots(val: number) {
    this.writeUInt8(val, 0x0D)
  }
  protected getSerializableObject(): object {
    return {
      ...super.getSerializableObject(),
      slots: this.slots
    }
  }
}
