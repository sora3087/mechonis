import { PlayerInventoryItem } from './PlayerInventoryItem'

export class InventoryMag extends PlayerInventoryItem {

  get def(): [number, number] {
    return this.getStat(0x0C)
  }
  set def(val: [number, number]) {
    this.setStat(val, 0x0C)
  }
  get pow(): [number, number] {
    return this.getStat(0x0E)
  }
  set pow(val: [number, number]) {
    this.setStat(val, 0x0E)
  }
  get dex(): [number, number] {
    return this.getStat(0x10)
  }
  set dex(val: [number, number]) {
    this.setStat(val, 0x10)
  }
  get mind(): [number, number] {
    return this.getStat(0x12)
  }
  set mind(val: [number, number]) {
    this.setStat(val, 0x12)
  }
  get synchro(): number {
    return this.readUInt8(0x18)
  }
  set synchro(val: number) {
    this.writeUInt8(val, 0x18)
  }
  get iq(): number {
    return this.readUInt8(0x19)
  }
  set iq(val: number) {
    this.writeUInt8(val, 0x19)
  }

  get color(): number {
    return this.readUInt8(0x19)
  }
  set color(val: number) {
    this.writeUInt8(val, 0x19)
  }
  get mflags(): number {
    return this.readUInt8(0x1A)
  }
  set mflags(val: number) {
    this.writeUInt8(val, 0x1A)
  }
  get photonBlasts(): [null|number, null|number, null|number] {
    const flags = this.mflags
    const blasts = this.readUInt8(0x0B)
    return [
      ((flags & 4) === 4) ? ((blasts >>> 6) & 0x07) : null,
      (flags & 1) ? (blasts & 0x07) : null,
      ((flags & 2) === 2) ? ((blasts >>> 3) & 0x07) : null,
    ]
  }
  set photonBlasts(val: [null|number, null|number, null|number]) {
    //
  }
  protected getSerializableObject() {
    return {
      ...super.getSerializableObject(),
      mflags: this.mflags,
      def: this.def,
      pow: this.pow,
      dex: this.dex,
      mind: this.mind,
      synchro: this.synchro,
      iq: this.iq,
      photonBlasts: this.photonBlasts,
    }
  }
  private getStat(offset: number): [number, number] {
    const stat = this.readInt16LE(offset)
    return [Math.floor(stat / 100), stat % 100]
  }
  private setStat(val: [number, number], offset: number): void {
    const stat = (val[0] * 100) + Math.min(val[1], 99)
    this.writeInt16LE(stat, offset)
  }
}
