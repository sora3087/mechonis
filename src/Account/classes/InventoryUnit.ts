import { PlayerInventoryItem } from './PlayerInventoryItem'
import { UnitQuality } from '../../Common/enum/UnitQuality'

export class InventoryUnit extends PlayerInventoryItem {
  get quality(): UnitQuality {
    return this.readInt16LE(0x0C)
  }
  set quality(val: UnitQuality) {
    this.writeInt16LE(val, 0x0C)
  }
  protected getSerializableObject(): object {
    return {
      ...super.getSerializableObject(),
      quality: UnitQuality[this.quality]
    }
  }
}
