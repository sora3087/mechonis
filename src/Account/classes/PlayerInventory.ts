import { ArmorType } from '../../Common/enum/ArmorType'
import { ItemType } from '../../Common/enum/ItemType'
import { BufferView } from '../../Network/BufferView'
import { PlayerLanguage } from './BlockedGuildcard'
import { InventoryConsumable } from './InventoryConsumable'
import { InventoryFrame } from './InventoryFrame'
import { InventoryMag } from './InventoryMag'
import { InventoryShield } from './InventoryShield'
import { InventoryUnit } from './InventoryUnit'
import { InventoryWeapon } from './InventoryWeapon'
import { PlayerInventoryItem } from './PlayerInventoryItem'

/**
 * typedef struct inventory {
 *   uint8_t item_count;
 *   uint8_t hpmats_used;
 *   uint8_t tpmats_used;
 *   uint8_t language;
 *   item_t items[30];
 * } PACKED inventory_t;
 */

export class PlayerInventory extends BufferView {
  public items: PlayerInventoryItem[] = []
  protected readonly size = 0x34C
  constructor(buffer: Buffer, offset: number) {
    super(buffer, offset)
    for (let i = 0; i < this.itemCount; i++) {
      const itemOffset = offset + 4 + (0x1C * i)
      if (buffer[itemOffset] === 0x01) {
        // item isSet
        switch (buffer[itemOffset + 0x08]) {
          case ItemType.Weapon:
            this.items.push(new InventoryWeapon(buffer, itemOffset))
            break
          case ItemType.Armor:
            switch (buffer[itemOffset + 0x09]) {
              case ArmorType.Frame:
                this.items.push(new InventoryFrame(buffer, itemOffset))
                break
              case ArmorType.Shield:
                this.items.push(new InventoryShield(buffer, itemOffset))
                break
              case ArmorType.Unit:
                this.items.push(new InventoryUnit(buffer, itemOffset))
                break
              default:
                break
            }
            break
          case ItemType.Mag:
            this.items.push(new InventoryMag(buffer, itemOffset))
            break
          case ItemType.Consumable:
            this.items.push(new InventoryConsumable(buffer, itemOffset))
          default:
            break
        }
      }
    }
  }
  get itemCount(): number {
    return this.readUInt8(0)
  }
  set itemCount(val: number) {
    this.writeUInt8(val, 0)
  }
  get hpMats(): number {
    return this.readUInt8(1)
  }
  set hpMats(val: number) {
    this.writeUInt8(val, 1)
  }
  get tpMats(): number {
    return this.readUInt8(2)
  }
  set tpMats(val: number) {
    this.writeUInt8(val, 2)
  }
  get language(): PlayerLanguage {
    return this.readUInt8(3)
  }
  set language(val: PlayerLanguage) {
    this.writeUInt8(val, 3)
  }
  setItem(item: PlayerInventoryItem, idx: number) {
    if (idx >= 30) {
      throw new RangeError('Item index is out of range')
    }
    if (idx > this.itemCount) {
      idx = this.itemCount
    }
    item.constructor()
  }
}
