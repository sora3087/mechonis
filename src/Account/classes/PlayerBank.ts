import { BufferView } from '../../Network/BufferView'

/**
 * typedef struct {
 *   DWORD numItems;
 *   DWORD meseta;
 *   PLAYER_BANK_ITEM items[200];
 * } PLAYER_BANK;
 */

export class PlayerBank extends BufferView {
  protected readonly size = 0xc
}
