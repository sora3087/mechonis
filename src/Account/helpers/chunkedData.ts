import { GuildcardChunk } from '../packets/GuildcardChunk';
import { ParamChunk } from '../packets/ParamChunk';

export const MAX_CHUNK_LENGTH = 0x6800

export const MAX_PARAM_CHUNK_LENGTH = MAX_CHUNK_LENGTH

export const getParamChunk = (data: Buffer, id: number): ParamChunk => {
  const offset = MAX_PARAM_CHUNK_LENGTH * id
  const len = Math.min(data.length - offset, MAX_PARAM_CHUNK_LENGTH)
  const chunk = new ParamChunk(data.slice(offset, offset + len))
  chunk.chunkId = id

  return chunk
}

export const getGuildcardChunk = (data: Buffer, id: number): GuildcardChunk => {
  const offset = MAX_CHUNK_LENGTH * id
  const len = Math.min(data.length - offset, MAX_CHUNK_LENGTH)
  const chunk = new GuildcardChunk(data.slice(offset, offset + len))
  chunk.chunkId = id

  return chunk
}
