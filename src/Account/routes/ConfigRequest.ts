import { readFileSync } from 'fs'
import { RegisteredGuildcard } from '../../DB/entities/RegisteredGuildcard'
import { CommandRoute } from '../../Network/CommandRouter'
import { redis } from '../../Network/RedisClient'
import Logger from '../../Util/Logger'
import { AccountSession } from '../classes/AccountSession'
import { PlayerLanguage } from '../classes/BlockedGuildcard'
import { characters } from '../db/repositories/characters'
import { registeredGuildcards } from '../db/repositories/registeredGuildcards'
import { ConfigResponse } from '../packets/ConfigResponse'
import { ControlsDefault } from '../packets/ControlsDefault'
import { accountSessions } from '../sessions'

const e2Default = readFileSync('./e2default.bin')

const configDefault = new ConfigResponse()
// send E2 information
configDefault.controls = e2Default

export const ConfigRequest: CommandRoute = (msg, next) => {
  // get session data from redis
  redis.getSession(msg.session).then(async user => {
    console.log('Found session from redis', user)
    // lookup account information now for later requests
    // store them in the session on the server until they are needed
    const session = new AccountSession()
    session.user = user

    let rid = 0
    let bid = 0
    const friends = await registeredGuildcards.find({ where: { user } })
    friends.forEach((friend: RegisteredGuildcard) => {
      if (friend.blocked) {
        session.registered[rid].class = friend.class
        session.registered[rid].comment = friend.comment
        session.registered[rid].desc = friend.desc
        session.registered[rid].guildcard = friend.id
        session.registered[rid].launguage = PlayerLanguage.English
        session.registered[rid].name = friend.name
        session.registered[rid].section = friend.section
        session.registered[rid].team = friend.teamName
        rid++
      } else {
        session.blocked[bid].class = friend.class
        session.blocked[bid].desc = friend.desc
        session.blocked[bid].guildcard = friend.id
        session.blocked[bid].launguage = PlayerLanguage.English
        session.blocked[bid].name = friend.name
        session.blocked[bid].section = friend.section
        session.blocked[bid].team = friend.teamName
        bid++
      }
    })
    await (await characters.find({ where: { user } })).forEach(char => {
      session.characters[char.slot] = char
    })

    // assign session in memory
    Logger.info(`Created session for ${msg.session}`)
    accountSessions.createSession(msg.session, session)

    const config = new ConfigResponse()
    config.controls = user.controls || ControlsDefault // use to default controls
    next(config)
  })
}
