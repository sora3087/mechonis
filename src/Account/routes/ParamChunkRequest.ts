import { BBPacket } from '../../Network/BBPacket'
import { CommandRoute } from '../../Network/CommandRouter'
import Logger from '../../Util/Logger'
import { ParamChunks } from './GetParamFiles'

export const ParamChunkRequest: CommandRoute = (msg, next) => {
  const id = msg.data.readUInt32LE(0x04)
  if (id < ParamChunks.length) {
    next(ParamChunks[id])
  } else {
    Logger.warn('Disconnected a client requesting a param chunk that did not exist')
    next(new BBPacket(8, 0, 0, 8), true)
  }
}
