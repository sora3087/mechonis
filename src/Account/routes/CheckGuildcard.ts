import { CommandRoute } from '../../Network/CommandRouter'
import CRC32 from '../../Patch/CRC32'
import { GuildcardCheckPacket } from '../packets/GuildcardCheckPacket'
import { GuildcardSummary } from '../packets/GuildcardSummary'
import { accountSessions } from '../sessions'

const defaultE8 = new GuildcardCheckPacket()
defaultE8.subId = 0x02
defaultE8.crc = 1

export const CheckGuildcard: CommandRoute = (msg, next) => {
  const packet = new GuildcardCheckPacket(msg.data)

  switch (packet.subId) {
    case 0x01:
      // client sending expected checksum
      // send E8
      next(defaultE8)
      break
    case 0x03:
      // send friends list (guildcards)
      const session = accountSessions.getSession(msg.session)
      const summary = new GuildcardSummary()
      summary.checksum = CRC32.hash(session.guildcardData)
      summary.length = session.guildcardData.length
      next(summary)
      break
    default:
      break
  }
  return
}
