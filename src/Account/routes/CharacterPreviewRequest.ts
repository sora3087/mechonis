import { CommandRoute } from '../../Network/CommandRouter'
import { CharacterMissing } from '../packets/CharacterMissing'
import { CharacterPreview } from '../packets/CharacterPreview'
import { CharacterRequest } from '../packets/CharacterRequest'
import { CharacterSelectAck } from '../packets/CharacterSelectAck'
import { accountSessions } from '../sessions'

export const CharacterPreviewRequest: CommandRoute = (msg, next) => {
  const packet = new CharacterRequest(msg.data)
  // send E4 for not created and E5 if it is stored in the database
  const session = accountSessions.getSession(msg.session)
  if (msg.data.readUInt32LE(0x0c) === 1) {
    // client character select
    next(new CharacterSelectAck(packet.slot))
    return
  }
  if (session.characters[packet.slot]) {
    if (msg.data.readUInt32LE(0x0c) !== 1) {
      next(new CharacterPreview(session.characters[packet.slot], session.user.guildcard))
    }
  } else {
    // maybe buffer missing character slots
    next(new CharacterMissing(packet.slot))
  }
}
