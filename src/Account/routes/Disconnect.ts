import { CommandRoute } from '../../Network/CommandRouter'
import { accountSessions } from '../sessions'

export const Disconnect: CommandRoute = (msg) => {
  accountSessions.deleteSession(msg.session)
}
