import { CommandRoute } from '../../Network/CommandRouter';
import { getGuildcardChunk } from '../helpers/chunkedData';
import { accountSessions } from '../sessions';

export const GuildcardDataContinue: CommandRoute = (msg, next) => {
  const id = msg.data.readUInt32LE(0x0C)
  const session = accountSessions.getSession(msg.session)
  if (id > session.lastChunkSent) {
    // this is to stop the service from infinite cycling the gc chunk requests
    session.lastChunkSent = id
    next(getGuildcardChunk(session.guildcardData, id))
  }
}
