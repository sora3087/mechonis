import { red } from 'colors/safe'
import { readFile } from 'fs'
import { basename } from 'path'
import { promisify } from 'util'
import { CommandRoute } from '../../Network/CommandRouter'
import CRC32 from '../../Patch/CRC32'
import Logger from '../../Util/Logger'
import { getParamChunk, MAX_PARAM_CHUNK_LENGTH } from '../helpers/chunkedData'
import { ParamChunk } from '../packets/ParamChunk'
import { IParamHeaderEntryOpts } from '../packets/ParamHeaderEntry'
import { ParamHeaderPacket } from '../packets/ParamHeaderPacket'

export const ParamChunks: ParamChunk[] = []

export let ParamHeader: ParamHeaderPacket

const MAX_PARAM_SIZE = 0x100000

export async function InitParamChunks(fileNames): Promise<void> {
  let currentParamSize = 0
  if (fileNames || fileNames.length === 0) {
    const files: Buffer[] = []
    const entries: IParamHeaderEntryOpts[] = []
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < fileNames.length; i++) {
      const file = await promisify(readFile)(fileNames[i])
      if (file && file.length < (MAX_PARAM_SIZE - currentParamSize)) {
        Logger.info('Loaded param file: ' + fileNames[i])
        files.push(file)
        entries.push({
          size: file.length,
          crc: CRC32.hash(file),
          name: basename(fileNames[i]),
          offset: currentParamSize
        })
        currentParamSize += file.length
      } else {
        Logger.error('Could not load any more param files')
        break
      }
    }
    ParamHeader = new ParamHeaderPacket(entries)
    const paramData = Buffer.concat(files)
    const paramChunkCount = Math.ceil(paramData.length / MAX_PARAM_CHUNK_LENGTH)
    for (let i = 0; i < paramChunkCount; i++) {
      ParamChunks.push(getParamChunk(paramData, i))
    }
  } else {
    throw new Error(`Found ${red('0')} param files`)
  }
  return
}

export const GetParamFiles: CommandRoute = (msg, next) => {
  next(ParamHeader)
}
