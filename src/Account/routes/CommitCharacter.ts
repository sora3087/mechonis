import { Color } from '../../DB/embeddedEntities/Color'
import { Proportion } from '../../DB/embeddedEntities/Proportion'
import { Character } from '../../DB/entities/Character'
import { CommandRoute } from '../../Network/CommandRouter'
import { redis } from '../../Network/RedisClient'
import Logger from '../../Util/Logger'
import { characters } from '../db/repositories/characters'
import { CharacterPreview } from '../packets/CharacterPreview'
import { CharacterSelectAck } from '../packets/CharacterSelectAck'

export const CommitCharacter: CommandRoute = async (msg, next) => {
  const user = await redis.getSession(msg.session)
  const parsedChar = new CharacterPreview(msg.data)
  let dbChar = await characters.findOne({
    where: { slot: parsedChar.slot, user }
  })
  if (!dbChar) {
    Logger.verbose('no existing character for slot found')
    dbChar = new Character()
  }
  dbChar.user =  user
  dbChar.class = parsedChar.class
  dbChar.costume = parsedChar.costume
  dbChar.exp = parsedChar.exp
  dbChar.face = parsedChar.face
  dbChar.hair = parsedChar.hair
  dbChar.hairColor = new Color()
  dbChar.hairColor.r = parsedChar.hairRed
  dbChar.hairColor.g = parsedChar.hairGreen
  dbChar.hairColor.b = parsedChar.hairBlue
  dbChar.head = parsedChar.head
  dbChar.level = parsedChar.level
  dbChar.name = parsedChar.name.replace(' ', '')
  dbChar.playTime = 0
  dbChar.proportion = new Proportion()
  dbChar.proportion.x = parsedChar.proportionX
  dbChar.proportion.y = parsedChar.proportionY
  dbChar.section = parsedChar.section
  dbChar.slot = parsedChar.slot
  dbChar.skin = parsedChar.skin
  dbChar.v1Flags = parsedChar.v1Flags
  dbChar.v2Flags = parsedChar.v2Flags
  dbChar.version = parsedChar.version
  const savedChar = await characters.save(dbChar)
  if (savedChar) {
    // saved properly
    Logger.info('Saved new character')
    Logger.debug(savedChar)
    next(new CharacterSelectAck(parsedChar.slot))
  } else {
    // did not save
  }
}
