import { Packet } from '../../Network/Packet'

export class CharacterMissing extends Packet {
  constructor(slot: number) {
    super(0x10, 0xE4)
    this.buffer[0x08] = slot
    this.buffer[0x0C] = 0x02
  }
}
