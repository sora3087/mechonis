import { Packet } from '../../Network/Packet'

export enum CharacterRequestMode {
  Requesting,
  Selecting
}

/**
 * Parser packet
 */
export class CharacterRequest extends Packet {
  public get slot(): number {
    return this.buffer[0x08]
  }
  public get mode(): CharacterRequestMode {
    return this.buffer[0x0C]
  }
}
