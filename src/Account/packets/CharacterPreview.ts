import { Character } from '../../DB/entities/Character'
import { Packet } from '../../Network/Packet'
import { CharacterClass } from '../../Common/enum/CharacterClass'
import { SectionId } from '../../Common/enum/SectionId'

/**
 *  DWORD exp;               4
 *  DWORD level;             4
 *  char guildcard[16];      16
 *  DWORD unknown3[2];       8
 *  DWORD nameColor ARGB;    4
 *  BYTE extraModel;         -
 *  BYTE unused[15];         16  42
 *  DWORD nameColorChecksum; 4   48
 *  BYTE sectionID;          -
 *  BYTE charClass;          -
 *  BYTE v2flags;            -
 *  BYTE version;            4   52
 *  DWORD v1flags;           4   56
 *  WORD costume;            2   58
 *  WORD skin;               2   60
 *  WORD face;               2   62
 *  WORD head;               2   64
 *  WORD hair;               2   66
 *  WORD hairRed;            2   68
 *  WORD hairGreen;          2   70
 *  WORD hairBlue;           2   72
 *  float propX;             4   76
 *  float propY;             4   80
 *  wchar_t playername[16];  32  112
 *  DWORD playTime;          4   116
 */
export class CharacterPreview extends Packet {
  constructor(data: Buffer);
  constructor(char: Character, guildcard: string);
  constructor(param1: Character | Buffer, guildcard?: string) {
    if (param1 instanceof Buffer) {
      super(param1)
    } else {
      super(0x88, 0xE5)
      this.exp = param1.exp
      this.level = param1.level
      this.guildcard = guildcard
      this.nameColor = [0xff, 0x00, 0xff, 0xff]
      this.section = param1.section
      this.class = param1.class
      this.v2Flags = 0
      this.version = 3
      this.v1Flags = 0x31
      this.skin = param1.skin
      this.face = param1.face
      this.head = param1.head
      this.hair = param1.hair
      this.hairRed = param1.hairColor.r
      this.hairGreen = param1.hairColor.g
      this.hairBlue = param1.hairColor.b
      this.proportionX = param1.proportion.x
      this.proportionY = param1.proportion.y
      this.name = param1.name
      this.playTime = param1.playTime
    }
  }
  get slot(): number {
    return this.buffer.readUInt16LE(0x08)
  }
  set slot(val: number) {
    this.buffer.writeUInt16LE(val, 0x08)
  }
  get exp(): number {
    return this.buffer.readUInt32LE(0x0C)
  }
  set exp(val: number) {
    this.buffer.writeUInt32LE(val, 0x0C)
  }
  get level(): number {
    return this.buffer.readUInt32LE(0x10)
  }
  set level(val: number) {
    this.buffer.writeUInt32LE(val, 0x10)
  }
  get guildcard(): string {
    return this.buffer.slice(0x14, 0x2B).toString().replace('\0', '')
  }
  set guildcard(val: string) {
    const name = Buffer.alloc(0x18)
    name.write('BB-' + val)
    name.copy(this.buffer, 0x14)
  }
  get nameColor(): [number, number, number, number] {
    return [this.buffer[0x2C], this.buffer[0x2D], this.buffer[0x2E], this.buffer[0x2F]]
  }
  set nameColor(val: [number, number, number, number]) {
    Buffer.from(val).copy(this.buffer, 0x2C)
  }
  get section(): SectionId {
    return this.buffer.readUInt8(0x44)
  }
  set section(val: SectionId) {
    this.buffer.writeUInt8(val, 0x44)
  }
  get class(): CharacterClass {
    return this.buffer.readUInt8(0x45)
  }
  set class(val: CharacterClass) {
    this.buffer.writeUInt8(val, 0x45)
  }
  get v2Flags(): number {
    return this.buffer.readUInt8(0x46)
  }
  set v2Flags(val: number) {
    this.buffer.writeUInt8(val, 0x46)
  }
  get version(): number {
    return this.buffer.readUInt8(0x47)
  }
  set version(val: number) {
    this.buffer.writeUInt8(val, 0x47)
  }
  get v1Flags(): number {
    return this.buffer.readUInt32LE(0x48)
  }
  set v1Flags(val: number) {
    this.buffer.writeUInt32LE(val, 0x48)
  }
  get costume(): number {
    return this.buffer.readUInt16LE(0x4C)
  }
  set costume(val: number) {
    this.buffer.writeUInt16LE(val, 0x4C)
  }
  get skin(): number {
    return this.buffer.readUInt16LE(0x4E)
  }
  set skin(val: number) {
    this.buffer.writeUInt16LE(val, 0x4E)
  }
  get face(): number {
    return this.buffer.readUInt16LE(0x50)
  }
  set face(val: number) {
    this.buffer.writeUInt16LE(val, 0x50)
  }
  get head(): number {
    return this.buffer.readUInt16LE(0x52)
  }
  set head(val: number) {
    this.buffer.writeUInt16LE(val, 0x52)
  }
  get hair(): number {
    return this.buffer.readUInt16LE(0x54)
  }
  set hair(val: number) {
    this.buffer.writeUInt16LE(val, 0x54)
  }
  get hairRed(): number {
    return this.buffer.readUInt16LE(0x56)
  }
  set hairRed(val: number) {
    this.buffer.writeUInt16LE(val, 0x56)
  }
  get hairGreen(): number {
    return this.buffer.readUInt16LE(0x58)
  }
  set hairGreen(val: number) {
    this.buffer.writeUInt16LE(val, 0x58)
  }
  get hairBlue(): number {
    return this.buffer.readUInt16LE(0x5A)
  }
  set hairBlue(val: number) {
    this.buffer.writeUInt16LE(val, 0x5A)
  }
  get proportionX(): number {
    return this.buffer.readFloatLE(0x5C)
  }
  set proportionX(val: number) {
    this.buffer.writeFloatLE(val, 0x5C)
  }
  get proportionY(): number {
    return this.buffer.readFloatLE(0x60)
  }
  set proportionY(val: number) {
    this.buffer.writeFloatLE(val, 0x60)
  }
  get name(): string {
    return this.buffer.slice(0x64, 0x84)
    .toString('utf16le')
    .replace(/^\t\w/, '')
    .replace('\0', '')
    // this.buffer.write('\tE' + val, 0x64, 0x20, 'utf16le')
  }
  set name(val: string) {
    this.buffer.write('\tE' + val, 0x64, 0x20, 'utf16le')
  }
  get playTime(): number {
    return this.buffer.readUInt32LE(0x84)
  }
  set playTime(val: number) {
    this.buffer.writeUInt32LE(val, 0x84)
  }
}
