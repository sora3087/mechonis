import { BBPacket } from '../../Network/BBPacket'

/**
 * 0000 DC01
 * 0000 0000
 * 0100 0000
 * llll 0000
 * chec ksum
 */

export class GuildcardSummary extends BBPacket {
  constructor() {
    super(0x14, 0xDC, 0x01, 8)
    this.buffer[0x08] = 1 // always 1?
  }
  public get length(): number {
    return this.buffer.readUInt16LE(0x0C)
  }
  public set length(val: number) {
    this.buffer.writeUInt16LE(val, 0x0C)
  }
  public get checksum(): number {
    return this.buffer.readUInt32LE(0x10)
  }
  public set checksum(val: number) {
    this.buffer.writeUInt32LE(val, 0x10)
  }
}
