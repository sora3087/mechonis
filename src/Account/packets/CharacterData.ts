import { CharacterClass } from '../../Common/enum/CharacterClass'
import { SectionId } from '../../Common/enum/SectionId'
import { BBPacket } from '../../Network/BBPacket'
import { PlayerBank } from '../classes/PlayerBank'
import { PlayerDisplayData } from '../classes/PlayerDisplayData'
import { PlayerInventory } from '../classes/PlayerInventory'

/**
 * typedef struct item {
 *   uint16_t equipped;
 *   uint16_t tech;
 *   uint32_t flags;
 *   union {
 *       uint8_t data_b[12];
 *       uint16_t data_w[6];
 *       uint32_t data_l[3];
 *   };
 *   uint32_t item_id;
 *   union {
 *       uint8_t data2_b[4];
 *       uint16_t data2_w[2];
 *       uint32_t data2_l;
 *   };
 * } PACKED item_t;
 */

/**
 * typedef struct {
 *   WORD atp;
 *   WORD mst;
 *   WORD evp;
 *   WORD hp;
 *   WORD dfp;
 *   WORD ata;
 *   WORD lck;
 * } PLAYER_STATS;
 */

/**
 * typedef struct {
 *   PLAYER_STATS stats;
 *   WORD unknown1;
 *   DWORD unknown2[2];
 *   DWORD level;
 *   DWORD exp;
 *   DWORD meseta;
 *   char guildcard[16];
 *   DWORD unknown3[2];
 *   DWORD nameColor;
 *   BYTE extraModel;
 *   BYTE unused[11];
 *   DWORD playTime; // not actually a game field; used only by newserv
 *   DWORD nameColorChecksum;
 *   BYTE sectionID;
 *   BYTE charClass;
 *   BYTE v2flags;
 *   BYTE version;
 *   DWORD v1flags;
 *   WORD costume;
 *   WORD skin;
 *   WORD face;
 *   WORD head;
 *   WORD hair;
 *   WORD hairRed;
 *   WORD hairGreen;
 *   WORD hairBlue;
 *   float propX;
 *   float propY;
 *   wchar_t playername[16];
 *   BYTE config[0xE8];
 *   BYTE techLevels[0x14];
 * } BB_PLAYER_DISPDATA;
 */

// ITEM_DATA is the same as item_t without the first three fields

/**
 * typedef struct {
 *   ITEM_DATA data;
 *   WORD amount;
 *   WORD showflags;
 * } PLAYER_BANK_ITEM;
 */

/**
 * typedef struct {
 *   PLAYER_INVENTORY inventory;      // 0000 // player
 *   BB_PLAYER_DISPDATA disp;         // 034C // player
 *   BYTE unknown[0x0010];            // 04DC //
 *   DWORD optionFlags;               // 04EC // account
 *   BYTE questData1[0x0208];         // 04F0 // player
 *   PLAYER_BANK bank;                // 06F8 // player
 *   DWORD serialNumber;              // 19C0 // player
 *   wchar_t name[0x18];              // 19C4 // player
 *   wchar_t teamname[0x10];          // 19F4 // player
 *   wchar_t guildcarddesc[0x58];     // 1A14 // player
 *   BYTE reserved1;                  // 1AC4 // player
 *   BYTE reserved2;                  // 1AC5 // player
 *   BYTE sectionID;                  // 1AC6 // player
 *   BYTE charClass;                  // 1AC7 // player
 *   DWORD unknown3;                  // 1AC8 //
 *   BYTE symbolchats[0x04E0];        // 1ACC // account
 *   BYTE shortcuts[0x0A40];          // 1FAC // account
 *   wchar_t autoreply[0x00AC];       // 29EC // player
 *   wchar_t infoboard[0x00AC];       // 2B44 // player
 *   BYTE unknown5[0x001C];           // 2C9C //
 *   BYTE challengeData[0x0140];      // 2CB8 // player
 *   BYTE techMenuConfig[0x0028];     // 2DF8 // player
 *   BYTE unknown6[0x002C];           // 2E20 //
 *   BYTE questData2[0x0058];         // 2E4C // player
 *   BB_KEY_TEAM_CONFIG keyConfig;    // 2EA4 // account
 * } BB_COMPLETE_PLAYER_DATA_FORMAT;    // total size: 39A0
 */

const OFFSET = 0x8

const INVENTORY_OFFSET = OFFSET
const DISPLAY_DATA_OFFSET = 0x34C + OFFSET
const OPTION_FLAGS_OFFSET = 0x4EC + OFFSET
const QUEST_DATA_1_OFFSET = 0x4F0 + OFFSET
const BANK_OFFSET = 0x06F8 + OFFSET
const SERIAL_NUMBER_OFFSET = 0x19C0 + OFFSET
const NAME_OFFSET = 0x19C4 + OFFSET
const TEAM_NAME_OFFSET = 0x19F4 + OFFSET
const GC_DESC_OFFSET = 0x1A14 + OFFSET
const RESERVED_1_OFFSET = 0x1AC4 + OFFSET
const RESERVED_2_OFFSET = 0x1AC5 + OFFSET
const SECTION_ID_OFFSET = 0x1AC6 + OFFSET
const CHAR_CLASS_OFFSET = 0x1AC7 + OFFSET
const SYMBOL_CHAT_OFFSET = 0x1ACC + OFFSET
const SHORTCUTS_OFFSET = 0x1FAC + OFFSET
const AUTOREPLY_OFFSET = 0x29EC + OFFSET
const INFOBOARD_OFFSET = 0x2B44 + OFFSET
const CHALLENGE_DATA_OFFSET = 0x2CB8 + OFFSET
const TECH_CONFIG_OFFSET = 0x2DF8 + OFFSET
const QUEST_DATA_2_OFFSET = 0x2E20 + OFFSET
const KEY_TEAM_CONFIG_OFFSET = 0x2EA4 + OFFSET

export class CharacterData extends BBPacket {
  private _inventory: PlayerInventory
  private _displayData: PlayerDisplayData
  private _bank: PlayerBank
  constructor(data?: Buffer) {
    super(0x399C, 0xE7, 0, 8)
    if (data) {
      data.copy(this.buffer, 0, 0, 0x399C)
    }
    this._inventory = new PlayerInventory(this.buffer, INVENTORY_OFFSET)
    this._displayData = new PlayerDisplayData(this.buffer, DISPLAY_DATA_OFFSET)
    this._bank = new PlayerBank(this.buffer, BANK_OFFSET)
  }
  get inventory(): PlayerInventory {
    return this._inventory
  }
  set inventory(val: PlayerInventory) {
    val.buffer.copy(this.buffer, INVENTORY_OFFSET, 0, PlayerInventory.length)
  }
  get displayData(): PlayerDisplayData {
    return this._displayData
  }
  set displayData(val: PlayerDisplayData) {
    val.buffer.copy(this.buffer, DISPLAY_DATA_OFFSET, 0, PlayerDisplayData.length)
  }
  get bank(): PlayerBank {
    return this._bank
  }
  set bank(val: PlayerBank) {
    val.buffer.copy(this.buffer, BANK_OFFSET, 0, PlayerBank.length)
  }
  get optionFlags(): number {
    return this.buffer.readUInt32LE(OPTION_FLAGS_OFFSET)
  }
  set optionFlags(val: number) {
    this.buffer.writeUInt32LE(val, OPTION_FLAGS_OFFSET)
  }
  get questData1(): Buffer {
    return this.buffer.slice(QUEST_DATA_1_OFFSET, QUEST_DATA_1_OFFSET + 0x208)
  }
  set questData1(val: Buffer) {
    val.copy(this.buffer, QUEST_DATA_1_OFFSET, 0, 0x208)
  }
  get serialNumber(): number {
    return this.buffer.readUInt32LE(SERIAL_NUMBER_OFFSET)
  }
  get name(): string {
    return this.buffer.slice(NAME_OFFSET, NAME_OFFSET + 0x18 * 2).toString('utf16le').replace(/\0/g, '')
  }
  set name(val: string) {
    const name = Buffer.alloc(0x18 * 2)
    name.write(val, 'utf16le')
    name.copy(this.buffer, NAME_OFFSET, 0, name.length)
  }
  get teamName(): string {
    return this.buffer.slice(TEAM_NAME_OFFSET, TEAM_NAME_OFFSET + 0x10 * 2).toString('utf16le').replace(/\0/g, '')
  }
  set teamName(val: string) {
    const name = Buffer.alloc(0x10 * 2)
    name.write(val, 'utf16le')
    name.copy(this.buffer, TEAM_NAME_OFFSET, 0, name.length)
  }
  get guildcardDesc(): string {
    return this.buffer.slice(GC_DESC_OFFSET, GC_DESC_OFFSET + 0x58 * 2).toString('utf16le').replace(/\0/g, '')
  }
  set guildcardDesc(val: string) {
    const name = Buffer.alloc(0x58 * 2)
    name.write(val, 'utf16le')
    name.copy(this.buffer, GC_DESC_OFFSET, 0, name.length)
  }
  get reserved1(): number {
    return this.buffer.readUInt8(RESERVED_1_OFFSET)
  }
  set reserved1(val: number) {
    this.buffer.writeUInt8(val, RESERVED_1_OFFSET)
  }
  get reserved2(): number {
    return this.buffer.readUInt8(RESERVED_2_OFFSET)
  }
  set reserved2(val: number) {
    this.buffer.writeUInt8(val, RESERVED_2_OFFSET)
  }
  get sectionId(): SectionId {
    return this.buffer.readUInt8(SECTION_ID_OFFSET)
  }
  set sectionId(val: SectionId) {
    this.buffer.writeUInt8(val, SECTION_ID_OFFSET)
  }
  get class(): CharacterClass {
    return this.buffer.readUInt8(CHAR_CLASS_OFFSET)
  }
  set class(val: CharacterClass) {
    this.buffer.writeUInt8(val, CHAR_CLASS_OFFSET)
  }
  get symbolChats(): Buffer {
    return this.buffer.slice(SYMBOL_CHAT_OFFSET, SYMBOL_CHAT_OFFSET + 0x4E0)
  }
  set symbolChats(val: Buffer) {
    val.copy(this.buffer, SYMBOL_CHAT_OFFSET, 0, val.length)
  }
  get shortcuts(): Buffer {
    return this.buffer.slice(SHORTCUTS_OFFSET, SHORTCUTS_OFFSET + 0xA40)
  }
  set shortcuts(val: Buffer) {
    val.copy(this.buffer, SHORTCUTS_OFFSET, 0, val.length)
  }
  get autoreply(): string {
    return this.buffer.slice(AUTOREPLY_OFFSET, AUTOREPLY_OFFSET + 0xAC * 2).toString('utf16le').replace(/\0/g, '')
  }
  set autoreply(val: string) {
    const str = Buffer.alloc(0xAC * 2)
    str.write(val, 'utf16le')
    str.copy(this.buffer, AUTOREPLY_OFFSET, 0, str.length)
  }
  get infoboard(): string {
    return this.buffer.slice(INFOBOARD_OFFSET, INFOBOARD_OFFSET + 0xAC * 2).toString('utf16le').replace(/\0/g, '')
  }
  set infoboard(val: string) {
    const str = Buffer.alloc(0xAC * 2)
    str.write(val, 'utf16le')
    str.copy(this.buffer, INFOBOARD_OFFSET, 0, str.length)
  }
  get challengeData(): Buffer {
    return this.buffer.slice(CHALLENGE_DATA_OFFSET, CHALLENGE_DATA_OFFSET + 0x140)
  }
  set challengeData(val: Buffer) {
    val.copy(this.buffer, CHALLENGE_DATA_OFFSET, 0, val.length)
  }
  get techMenuConfig(): Buffer {
    return this.buffer.slice(TECH_CONFIG_OFFSET, TECH_CONFIG_OFFSET + 0x28)
  }
  set techMenuConfig(val: Buffer) {
    val.copy(this.buffer, TECH_CONFIG_OFFSET, 0, val.length)
  }
  get questData2(): Buffer {
    return this.buffer.slice(QUEST_DATA_2_OFFSET, QUEST_DATA_2_OFFSET + 0x58)
  }
  set questData2(val: Buffer) {
    val.copy(this.buffer, QUEST_DATA_2_OFFSET, 0, val.length)
  }
  get keyConfig(): Buffer {
    return this.buffer.slice(KEY_TEAM_CONFIG_OFFSET, KEY_TEAM_CONFIG_OFFSET + 0xA40)
  }
  set keyConfig(val: Buffer) {
    val.copy(this.buffer, KEY_TEAM_CONFIG_OFFSET, 0, val.length)
  }
}
