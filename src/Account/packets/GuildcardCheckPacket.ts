import { BBPacket } from '../../Network/BBPacket'

export class GuildcardCheckPacket extends BBPacket {
  constructor(data?: Buffer) {
    if (data) {
      super(data)
    } else {
      super(0x0c, 0xE8, 0, 8)
    }
  }
  set crc(val: number) {
    this.buffer.writeUInt32LE(val, 0x08)
  }
  get crc(): number {
    return this.buffer.readUInt32LE(0x08)
  }
}
