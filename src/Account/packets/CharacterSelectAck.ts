import { BBPacket } from '../../Network/BBPacket';

export class CharacterSelectAck extends BBPacket {
  constructor(slot: number) {
    super(0x10, 0xE4)
    this.buffer.writeUInt32LE(slot, 0x08)
    this.buffer.writeUInt32LE(1, 0x0C)
  }
}
