import { BufferView } from '../../Network/BufferView'

export interface IParamHeaderEntryOpts {
  size: number
  crc: number
  offset: number
  name: string
}

export class ParamHeaderEntry extends BufferView {
  protected readonly size = 0x4c
  constructor(buffer: Buffer, offset: number) {
    super(buffer, offset)
  }
  public set fileSize(val: number) {
    this._buffer.writeUInt32LE(val, this.offset)
  }
  public get fileSize(): number{
    return this.buffer.readUInt32LE(this.offset)
  }
  public set checksum(val: number) {
    this._buffer.writeUInt32LE(val, this.offset + 0x04)
  }
  public get checksum(): number {
    return this._buffer.readUInt32LE(this.offset + 0x04)
  }
  public set dataOffset(val: number) {
    this._buffer.writeUInt32LE(val, this.offset + 0x08)
  }
  public get dataOffset(): number {
    return this._buffer.readUInt32LE(this.offset + 0x08)
  }
  public set name(val: string) {
    this._buffer.write(val, this.offset + 0x0C, 0x40)
  }
  public get name(): string {
    return this._buffer.slice(this.offset + 0x0C, this.offset + 0x4C).toString()
  }
}
