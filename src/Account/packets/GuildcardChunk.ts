import { BBPacket } from '../../Network/BBPacket'

export class GuildcardChunk extends BBPacket {
  constructor(data: Buffer) {
    super(data.length + 0x10, 0xDC, 0x02, 8)
    data.copy(this.buffer, 0x10)
  }
  set chunkId(val: number) {
    this.buffer.writeUInt32LE(val, 0x0C)
  }
}
