import { BBPacket } from '../../Network/BBPacket'

export class ParamChunk extends BBPacket {
  constructor(data: Buffer) {
    super(data.length + 0x0C, 0xEB, 0x02, 8)
    data.copy(this.buffer, 0x0C)
  }
  set chunkId(val: number) {
    this.buffer.writeUInt32LE(val, 0x08)
  }
}
