import { BBPacket } from '../../Network/BBPacket';

export class NewCharacterPacket extends BBPacket{
  constructor(data: Buffer) {
    super(data)
  }
  get slot(): number {
    return this.buffer.readUInt16LE(0x08)
  }
  get name(): string {
    return this.buffer.slice(0x68, 0x68 + 0x1C).toString().replace('\0', '')
  }
  get 
}
