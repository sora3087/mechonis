import { Packet } from '../../Network/Packet'

export class ConfigResponse extends Packet {
  constructor() {
    super(2808, 0xE2)
  }
  set controls(val: Buffer) {
    if (val.length !== 0x1A0) {
      throw new TypeError('controls buffer wasn\'t the correct size, actual size was 0x' + val.length.toString(16))
    }
    val.copy(this.buffer, 0x120)
  }
  get controls(): Buffer {
    return this.buffer.slice(0x120, 0x120 + 0x1A0)
  }
}
