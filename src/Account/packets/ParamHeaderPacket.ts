import { BBPacket } from '../../Network/BBPacket';
import { ParamHeaderEntry, IParamHeaderEntryOpts } from './ParamHeaderEntry'

export class ParamHeaderPacket extends BBPacket {
  public entries: ParamHeaderEntry[] = []
  constructor(entries: IParamHeaderEntryOpts[]) {
    super(0x08 + entries.length * 0x4c, 0xEB, 0x01, 8)
    this.buffer.writeUInt32LE(entries.length, 0x04)
    for (let i = 0; i < entries.length; i++) {
      const entry = new ParamHeaderEntry(this.buffer, 0x08 + (0x4c * i))
      entry.fileSize = entries[i].size
      entry.checksum = entries[i].crc
      entry.dataOffset = entries[i].offset
      entry.name = entries[i].name
      this.entries.push(entry)
    }
  }
}
