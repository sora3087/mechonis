import { SessionManager } from '../Util/SessionManager'
import { AccountSession } from './classes/AccountSession'

export const accountSessions = new SessionManager<AccountSession>()
