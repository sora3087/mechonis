import { CommandRouter } from '../Network/CommandRouter'
import { ConfigRequest } from './routes/ConfigRequest'
import { CharacterPreviewRequest } from './routes/CharacterPreviewRequest'
import { CommitCharacter } from './routes/CommitCharacter'
import { CheckGuildcard } from './routes/CheckGuildcard'
import { GuildcardDataContinue } from './routes/GuildcardDataContinue'
import { GetParamFiles } from './routes/GetParamFiles'
import { ParamChunkRequest } from './routes/ParamChunkRequest'
import { Disconnect } from './routes/Disconnect'
import { LeaveCharacterSelect } from './routes/LeaveCharacterSelect'

export const createAccountRouter = () => {
  const router = new CommandRouter()
  router.on(0x05, Disconnect)
  router.on(0xDC03, GuildcardDataContinue)
  router.on(0xE0, ConfigRequest)
  router.on(0xE3, CharacterPreviewRequest)
  router.on(0xE5, CommitCharacter)
  router.on(0xE8, CheckGuildcard)
  // 0xEB02 is response of param chunk
  router.on(0xEB03, ParamChunkRequest)
  router.on(0xEB04, GetParamFiles)
  router.on(0xEC, LeaveCharacterSelect)
  return router
}
