import { createHash } from 'crypto'

export function sha256Hash(input: string): string {
  const hash = createHash('sha256')
  hash.update(input)
  return hash.digest('hex')
}

export function sha256HashRounds(input: string, rounds: number): string {
  if (rounds <= 0) {
    rounds = 1
  }
  for (let i = 0; i < rounds; i++) {
    input = sha256Hash(input)
  }
  return input
}
