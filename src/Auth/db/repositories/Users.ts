import { green } from 'colors/safe'
import { Repository } from 'typeorm'
import { dbConnection } from '../../../DB'
import Logger from '../../../Util/Logger'
import { User } from '../../../DB/entities/User'

export let users: Repository<User>

dbConnection.then((connection) => {
  if (connection) {
    users = connection.getRepository(User)
    users.count().then(userCount => {
      Logger.info(`Registered user count: ${green(userCount.toString())}`)
    })
  }
})
