import { compile } from 'ejs'
import { readFileSync, realpathSync } from 'fs'
import { dirname } from 'path'
import Logger from '../../Util/Logger'
import { parseColorCodes } from './parseColorCodes'

const welcomeTemplatePath = './welcome.ejs'

let templateString = ''

try {
  templateString = readFileSync(welcomeTemplatePath).toString()
  // parse color tags to color codes here
  templateString = parseColorCodes(templateString).trim() + '\0'
} catch (e) {
  Logger.warn(`Welcome template was not found at "${realpathSync(dirname(welcomeTemplatePath))}/welcome.ejs", using fallback`)
  templateString = 'Hello, Player. Welcome to Mechonis patch server!\0'
}

export const WelcomeTemplate = compile(templateString)
