const ColorCodes: { [key: string]: string } = {
  black: '0',
  blue: '1',
  green: '2',
  cyan: '3',
  red: '4',
  magenta: '5',
  yellow: '6',
  white: '7',
  pink: '8',
  gold: 'G'
}

export function parseColorCodes(template: string): string {
  const tagReg = /<(\/?)color[\.|-]([^>]+)>/gi
  const colorHistory: Array<string> = [ColorCodes.white]
  let match
  // tslint:disable-next-line:no-conditional-assignment
  while ((match = tagReg.exec(template)) !== null) {
    let [tag, closeTag, colorName ] = match
    let code = ''
    if (closeTag === '/') {
      if (colorHistory.length > 2) {
        colorHistory.pop()
      }
      if (colorHistory.length > 1) {
        code = colorHistory.pop()
      } else {
        code = colorHistory[0]
      }
    } else {
      code = ColorCodes[colorName.toLowerCase()]
      // tslint:disable-next-line:no-conditional-assignment
      if (!code) {
        code = ColorCodes.white
      }
      if (colorHistory.length === 0) {
        colorHistory.push(ColorCodes.white)
      }
      colorHistory.push(code)
    }
    template = template.replace(tag, `\tC${code}`)
    tagReg.lastIndex = match.index
  }
  return template
}
