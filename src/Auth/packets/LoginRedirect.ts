import { toBuffer } from '../../Network/IP/lib/toBuffer'
import { toString } from '../../Network/IP/lib/toString'
import { Packet } from '../../Network/Packet'

// 00000000: 0C00 1400 0000 0000 0000 0000

export class LoginRedirect extends Packet {
  constructor(address?: string, port?: number) {
    super(0x0C, 0x14)
    if (address) {
      this.ipaddress = address
    }
    if (port) {
      this.port = port
    }
  }
  get ipaddress(): string {
    return toString(this.buffer.slice(0x04, 0x08))
  }
  set ipaddress(address: string) {
    toBuffer(address).copy(this.buffer, 0x04)
  }
  get port(): number {
    return this.buffer.readUInt16BE(0x08)
  }
  set port(port: number) {
    this.buffer.writeUInt16BE(port, 0x08)
  }
}
