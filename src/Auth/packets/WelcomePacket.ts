import { Packet } from '../../Network/Packet'
import { roundIncrement } from '../../Util/Numbers'

export class WelcomePacket extends Packet {
  constructor(text: string) {
    const messageData = Buffer.from(text, 'utf16le')
    super(roundIncrement(messageData.length + 0x04, 4), 0x13)
    messageData.copy(this.buffer, 0x04)
  }
}
