import { Packet } from '../../Network/Packet'

export class LoginFailed extends Packet {
  constructor() {
    super(0x04, 0x15)
  }
}
