import { Packet } from '../../Network/Packet'

export class RequestLogin extends Packet {
  constructor() {
    super(0x04, 0x04)
  }
}
