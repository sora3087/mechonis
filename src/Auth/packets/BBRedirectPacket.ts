// import { toBuffer, toString } from 'ip'
import { toBuffer } from '../../Network/IP/lib/toBuffer'
import { toString } from '../../Network/IP/lib/toString'
import { Packet } from '../../Network/Packet'

/**
 * 10 00 19 00 00 00 00 00
 * 7f 00 00 01 // 127.0.0.1
 * 2e e1 // 12001
 * 00 00
 */
export class BBRedirectPacket extends Packet {
  constructor(address?: string, port?: number) {
    super(0x10, 0x19)
    if (address) {
      this.ipaddress = address
    }
    if (port) {
      this.port = port
    }
  }
  get ipaddress(): string {
    return toString(this.buffer.slice(0x08, 0x08))
  }
  set ipaddress(address: string) {
    toBuffer(address).copy(this.buffer, 0x08)
  }
  get port(): number {
    return this.buffer.readUInt16LE(0x0C)
  }
  set port(port: number) {
    this.buffer.writeUInt16LE(port, 0x0C)
  }
}
