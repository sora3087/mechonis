import { Packet } from '../../Network/Packet'

export class PatchHandoff extends Packet {
  constructor() {
    super(0x04, 0x04, 0x01)
  }
}
