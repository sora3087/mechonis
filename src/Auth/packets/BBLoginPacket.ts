import { Packet } from '../../Network/Packet'

/**
 * Packet parser
 */
export class BBLoginPacket extends Packet {
  get username(): string {
    const val = this.buffer.slice(0x1C, 0x4C)
    return val.toString().substr(0, val.indexOf('\0'))
  }
  get password(): string {
    return this.buffer
      .slice(0x4C, 0x7C).toString().replace(/\0/g, '')
  }
  get hardwardInfo(): Buffer {
    return this.buffer.slice(0x84, this.buffer.length - 1)
  }
}
