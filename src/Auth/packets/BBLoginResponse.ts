import { Packet } from '../../Network/Packet'

export enum LoginResponseCode {
  OK,
  UnknownError,
  IncorrectLogin,
  IncorrectLoginAlt,
  Maintenance,
  AlreadyOnline,
  Banned,
  Banned2,
  NoUser,
  SubscriptionExpired,
  Locked,
  BadVersion,
  ForcedDisconnect,
}

export class BBLoginResponse extends Packet {
  constructor() {
    super(0x44, 0xE6, 0, 8)
  }
  get status(): LoginResponseCode {
    return this.buffer.readUInt32LE(0x8)
  }
  set status(code: LoginResponseCode) {
    this.buffer.writeUInt32LE(code, 0x8)
  }
  get tag(): number {
    return this.buffer.readUInt32LE(0xC)
  }
  set tag(code: number) {
    this.buffer.writeUInt32LE(code, 0xC)
  }
  get guildcard(): number {
    return this.buffer.readUInt32LE(0x10)
  }
  set guildcard(id: number) {
    this.buffer.writeUInt32LE(id, 0x10)
  }
  get security32(): number {
    return this.buffer.readUInt32LE(0x14)
  }
  set security32(id: number) {
    this.buffer.writeUInt32LE(id, 0x14)
  }
  get securityCode64(): Buffer {
    return this.buffer.slice(0x38, 0x3F)
  }
  set securityCode64(buffer: Buffer) {
    buffer.copy(this.buffer, 0x38)
  }
  get capabilities(): number{
    return this.buffer.readUInt32LE(0x40)
  }
  set capabilities(code: number) {
    this.buffer.writeUInt32LE(code, 0x40)
  }
}
