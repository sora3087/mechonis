import { Packet } from '../../Network/Packet'

/**
 * Packet parser
 */
export class PSOLoginPacket extends Packet {
  get username(): string {
    return this.buffer.slice(0x10, 0x1f).toString('ascii').trim()
  }
  get password(): string {
    return this.buffer.slice(0x20, 0x2f).toString('ascii').trim()
  }
}
