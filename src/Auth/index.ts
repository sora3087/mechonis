import { dbConnection } from '../DB'
import { EncryptionType } from '../Network/EncryptionType'
import { redis } from '../Network/RedisClient'
import { Service } from '../Network/ServiceBase'
import { createAuthRouter } from './router'

const SERVICE_NAME = 'auth'

const {
  MECHONIS_MACHINE_ID,
  MECHONIS_REDIS_HOST
} = process.env

const start = async () => {
  await dbConnection
  await redis.init(MECHONIS_REDIS_HOST)
  const router = createAuthRouter()
  const service = new Service({
      serviceName: SERVICE_NAME,
      hostId: MECHONIS_MACHINE_ID,
      redis: {
        host: MECHONIS_REDIS_HOST
      },
      ports: {
        11000: { type: EncryptionType.PSO, redirect: true },
        12000: { type: EncryptionType.BB, redirect: true }
      }
    },
    router
  )

  const exit = async () => {
    const destroyed = await service.destroy()
    console.log('awaited destroy', destroyed)
    process.exit()
  }

  process.on('beforeExit', exit)
  process.on('SIGINT', exit)
}

start()
