import { CommandRouter } from '../Network/CommandRouter'
import { BBLogin } from './routes/BBLogin'
import { PatchLogin } from './routes/PatchLogin'
import { PSOInitAck } from './routes/PSOInitAck'

export const createAuthRouter = () => {
  const router = new CommandRouter()
  router.on(0x02, PSOInitAck)
  router.on(0x04, PatchLogin)
  router.on(0x93, BBLogin)

  return router
}
