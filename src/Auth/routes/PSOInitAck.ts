import { CommandRoute } from '../../Network/CommandRouter'
import { RequestLogin } from '../packets/RequestLogin'

export const PSOInitAck: CommandRoute = (msg, next) => {
  next(new RequestLogin())
}
