import { BBPacket } from '../../Network/BBPacket'
import { CommandRoute } from '../../Network/CommandRouter'
import { NetworkMessage } from '../../Network/NetworkMessage'
import { redis } from '../../Network/RedisClient'
import { users } from '../db/repositories/Users'
import { sha256HashRounds } from '../helpers'
import { BBLoginPacket } from '../packets/BBLoginPacket'
import { BBLoginResponse, LoginResponseCode } from '../packets/BBLoginResponse'
import { BBRedirectPacket } from '../packets/BBRedirectPacket'

const {
  MECHONIS_CONSUL_HOST
} = process.env

const REDIRECT_HOST = MECHONIS_CONSUL_HOST || '127.0.0.1'

export const BBLogin: CommandRoute = (msg: NetworkMessage, next) => {
  const login = new BBLoginPacket(msg.data)
  const {username, password} = login
  users.findOne({
    where: { username },
    relations: ['team']
  }).then((user) => {
    const response = new BBLoginResponse()
    if (user) {
      if (!user.active) {
        // respond account deactivated
        response.status = LoginResponseCode.Locked
      }
      const pass = sha256HashRounds(password, 15)
      if (pass === user.password) {
        // respond logged in
        response.status = LoginResponseCode.OK
        redis.createSession(msg.session, user)
      } else {
        // respond invalid password
        response.status = LoginResponseCode.IncorrectLogin
      }
    } else {
      // respond no account
      response.status = LoginResponseCode.NoUser
    }
    return response
  })
  .catch(() => {
    // disconnect on failure
    const response = new BBLoginResponse()
    response.status = LoginResponseCode.ForcedDisconnect
    return Promise.resolve(response)
  })
  .then((response: BBLoginResponse) => {
    next(response, response.status !== LoginResponseCode.OK)
    if (response.status === LoginResponseCode.OK) {
      // send login redirect
      if (msg.data[0x03] === 1 << 7) {
        // this will need to determine which gate address and acct service to redirect to
        next(new BBRedirectPacket('127.0.0.1', 12001) , true)
      } else if (msg.data[0x16] === 0x04) {
        // phase 4 of login, redirect to world state service
        msg.data[0x03] = 0x04
        next(new BBPacket(msg.data), false, true)
      }
    }
    // if the login reponse was OK and the service didn't need to redirect then
    // the client will send an E0 command that is handled with another service
  })
}
