import { CommandRoute } from '../../Network/CommandRouter'
import { Packet } from '../../Network/Packet'
import Logger from '../../Util/Logger'
import { LoginFailed } from '../packets/LoginFailed'
import { LoginRedirect } from '../packets/LoginRedirect'
import { PatchHandoff } from '../packets/PatchHandoff'
import { PSOLoginPacket } from '../packets/PSOLoginPacket'
import { WelcomePacket } from '../packets/WelcomePacket'
import { WelcomeTemplate } from '../utils/WelcomeTemplate'

const PatchRedirect = new LoginRedirect('127.0.0.1', 11001)
export const PatchLogin: CommandRoute = (msg, next) => {
  const login = new PSOLoginPacket(msg.data)
  const redirect = msg.data[3] >> 7
  if (!login.username || !login.password) {
    next(new LoginFailed(), true)
    return
  }
  // send welcome text
  const welcomeText = WelcomeTemplate({
    user: login.username,
    // TODO: pass total users online when redis connected
    userCount: 0
  })
  next(new WelcomePacket(welcomeText), false)
  if (redirect) {
    // redirect to patch and disconnect
    Logger.info(`redirecting ${msg.session} to Patch service`)
    next(PatchRedirect, true)
  } else {
    // send a message interally as to handoff to patch
    next(new PatchHandoff(), false, true)
  }
  return
}
