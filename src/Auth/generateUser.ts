import { Command } from 'commander'
import { Connection } from 'typeorm'
import { ControlsDefault } from '../Account/packets/ControlsDefault'
import { dbConnection } from '../DB'
import { Character } from '../DB/entities/Character'
import { User } from '../DB/entities/User'

const program = new Command()
program
.requiredOption('-u', '--user [username]', 'sets username for new user')
.requiredOption('-p', '--password [password]', 'sets password for new user')
.requiredOption('-e', '--email [email]', 'sets email for new user')
.parse()

const CONFIG = program.opts<{ username: string, password: string, email: string }>()

// spinner('Creating...')
dbConnection.then(async(db: Connection) => {
  if (db) {
    const query = db.getRepository(User).createQueryBuilder()
    query.select('MAX(guildcard)', 'max')
    let { max } = await query.getRawOne()
    console.log(max)
    if (!max) {
      max = 100000
    } else {
      max += 1
    }
    const user = new User()
    user.guildcard = max
    user.password = CONFIG.password
    user.username = CONFIG.username
    user.controls = ControlsDefault
    user.email = CONFIG.email
    await db.manager.save(user)
    const char = new Character()
    char.slot = 0
    char.name = 'test'
    char.user = user
    await db.manager.save(char)
    // spinner('Done', true)
    db.close()
  } else {
    // spinner('Failed', true)
  }
})
