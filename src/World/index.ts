import { EncryptionType } from '../Network/EncryptionType'
import { redis } from '../Network/RedisClient'
import { Service } from '../Network/ServiceBase'
import { router } from './router'

const SERVICE_NAME = 'world'

const {
  MECHONIS_MACHINE_ID,
  MECHONIS_REDIS_HOST
} = process.env

const start = async () => {
  await redis.init(MECHONIS_REDIS_HOST)
  const service = new Service({
      serviceName: SERVICE_NAME,
      hostId: MECHONIS_MACHINE_ID,
      redis: {
        host: MECHONIS_REDIS_HOST
      },
      ports: {
        12001: { type: EncryptionType.BB, redirect: false }
      }
    },
    router
  )

  const exit = async () => {
    const destroyed = await service.destroy()
    console.log('awaited destroy', destroyed)
    process.exit()
  }

  process.on('beforeExit', exit)
  process.on('SIGINT', exit)
}

start()
