import { CommandRouter } from '../Network/CommandRouter' 
import { LoginPhase4 } from './routes/LoginPhase4'

export const router = new CommandRouter()

router.on(0x9304, LoginPhase4)