export type ShipInfo = {
  name: string
  flags: number
}