import { CommandRoute } from '../../Network/CommandRouter';
import { BannerTextPacket } from '../packets/BannerTextPacket';
import { ServerTimePacket } from '../packets/ServerTimePacket'
import { ShipsListPacket } from '../packets/ShipsListPacket'
import { ShipInfo } from '../types/ShipInfo'

const ships: ShipInfo[] = [
  {
    name: 'No ships online!',
    flags: 0
  }
]

const shipsPacket = new ShipsListPacket(ships)

const bannerPacket = new BannerTextPacket('Welcome to Mechonis server')

export const LoginPhase4: CommandRoute = (msg, next) => {
  next(shipsPacket)
  next(new ServerTimePacket())
  next(bannerPacket)
}