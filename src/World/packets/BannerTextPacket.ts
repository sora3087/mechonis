import { BBPacket } from '../../Network/BBPacket';

export class BannerTextPacket extends BBPacket {
  constructor(text: string) {
    super(0x50, 0xEE)
    this.text = text
  }
  set text(value: string) {
    const buf = Buffer.alloc(0x40)
    buf.write(value.substring(0, 0x20), 0, 'utf16le')
    buf.copy(this.buffer, 0x10)
  }
}
