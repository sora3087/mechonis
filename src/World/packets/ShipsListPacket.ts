import { BBPacket } from '../../Network/BBPacket';
import { ShipInfo } from '../types/ShipInfo';

enum MenuType {
  Type1 = 0x00001200,
  Type2 = 0x00002000
}

export class ShipsListPacket extends BBPacket {
  constructor(list: ShipInfo[]) {
    let size = list.length * 0x2C
    size += 8 - (size%8)
    super(size, 0xA0)
    this.flags = list.length
    list.forEach((el, id) => this.setShipInfo(el, id))
  }
  private setShipInfo(ship: ShipInfo, idx: number): void {
    const offset = idx * 0x2C + 0x08
    this.buffer.writeUInt32LE(MenuType.Type1, offset)
    this.buffer.writeUInt32LE(idx + 1, offset + 0x04)
    this.buffer.writeUInt16LE(ship.flags, offset + 0x08)
    this.buffer.write(ship.name.substring(0, 17), offset + 0x0A, 'utf16le')
  }
}