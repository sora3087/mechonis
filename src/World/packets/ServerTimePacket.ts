import { BBPacket } from '../../Network/BBPacket'

// Time format
// "2022:01:30: 11:23:27.000"

function pad(value: number, length: number): string {
  let newVal = value.toString()
  if(newVal.length < length){
    newVal = "0".repeat(length - newVal.length) + newVal
  }
  return newVal
}

export class ServerTimePacket extends BBPacket {
  constructor() {
    super(0x20, 0xB1)

    this.writeDate(new Date())
  }
  private writeDate (date: Date): void {
    const formatted = `${pad(date.getUTCFullYear(), 2)}:${pad(date.getUTCMonth()+1, 2)}:${pad(date.getUTCDate(), 2)}: ${pad(date.getUTCHours(), 2)}:${pad(date.getUTCMinutes(), 2)}:${pad(date.getUTCSeconds(), 2)}.${pad(date.getUTCMilliseconds(), 3)}`
    this.buffer.write(formatted, 0x08)
  }
}