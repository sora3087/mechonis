export default class FIFOBuffer {
  public high: number
  public head: number = 0
  public tail: number = 0
  private buffer: Buffer
  constructor(maxSize: number = 1024 * 1024) {
     this.buffer = Buffer.alloc(maxSize)
     this.high = maxSize
  }

  // append copy of  _buffer. If size() + _buffer.length > maxSize, enq returns false, otherwise true.
  public enq(buffer: Buffer): boolean {
     const n = buffer.length
     if (this.size + n > this.high) {
       return false
     }
     if (this.high - this.tail < n) { // enough space but clustered, make current = 0
        this.buffer.copy(this.buffer, 0, this.head, this.tail)
        this.tail -= this.head
        this.head = 0
     }
     buffer.copy(this.buffer, this.tail)
     this.tail += n
     return true
  }

  // return first n bytes, returns null if less bytes are available
  // peekonly opional and private use from peek().
  public deq(nbytes: number, peekonly?: boolean): Buffer | null {
     if (nbytes > this.size) {
       return null
     }
     const retBuf = Buffer.alloc(nbytes)
     this.buffer.copy(retBuf, 0, this.head, this.head + nbytes)
     if (peekonly) {
       return retBuf
     }
     this.head += nbytes
     if (this.size === 0) { // if tail == head we can jump to 0 again
        this.head = this.tail = 0
     }
     return retBuf
  }

  // returns a copy of nbytes from current start of buffer, or null if less bytes available
  public peek(nbytes: number): Buffer | null {
     return this.deq(nbytes, true)
  }

  // same as deq(size)
  public drain(): Buffer {
     return this.deq(this.size)
  }
  // Returns n bytes available in buffer
  public get size(): number {
     return this.tail - this.head
  }
}
