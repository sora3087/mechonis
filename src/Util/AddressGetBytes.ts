const AddressGetBytes = (address: string): Buffer =>  {
  return Buffer.from(address.split('.').map((octet) => parseInt(octet, 10)))
}

export { AddressGetBytes }
