import { red, yellow, green, blue } from 'colors/safe'
import { hexy, FormatOptions as HexyFormatOptions } from 'hexy'
import { WriteStream } from 'tty'
const hexyFormat: HexyFormatOptions = {
  format: 'twos'
}

export enum LogLevel {
  fatal,
  error,
  warn,
  info,
  verb,
  debug
}

class Logging {
  public logLevel: LogLevel = LogLevel.debug
  public out: WriteStream = process.stdout
  public err: WriteStream = process.stderr
  public log(level: LogLevel, content: any): void {
    if (level <= this.logLevel) {
      if (content instanceof Buffer) {
        content = '\r\n' + hexy(content)
      }
      if (level <= LogLevel.error) {
        this.err.write(`[${red(LogLevel[level])}] ${content}\n`)
      } else {
        this.out.write(`[${LogLevel[level]}] ${content}\n`)
      }
    }
  }
  public fatal(content: any): void {
    this.log(LogLevel.fatal, content)
  }
  public error(content: any): void {
    this.log(LogLevel.error, content)
  }
  public warn(content: any): void {
    this.log(LogLevel.warn, content)
  }
  public info(content: any): void {
    this.log(LogLevel.info, content)
  }
  public verbose(content: any): void {
    this.log(LogLevel.verb, content)
  }
  public debug(content: any): void {
    this.log(LogLevel.debug, content)
  }
}

const Logger = new Logging()
export default Logger
