function UInt32(value: number): number {
  return (value & 0xFFFFFFFF) >>> 0
}

function UInt16(value: number): number {
  return (value & 0xFFFF)
}

function UIntGetByte(value: number, byte: number = 0): number {
  return (value >>> (byte * 8)) & 0xFF
}

export {
  UInt16,
  UInt32,
  UIntGetByte
}
