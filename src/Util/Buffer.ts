export function randomBufferFill(data: Buffer): Buffer {
  for (let i = 0; i < data.length; i++) {
    data[i] = Math.round(Math.random() * 0xFF)
  }
  return data
}

export function byteAlign(data: Buffer, align: number): Buffer {
  if (align < 1) {
    return data
  }
  const bytesOver = data.length % align
  if (bytesOver !== 0) {
    return bufferGrow(data, data.length + (align - bytesOver))
  }
  return data
}

export function bufferGrow(buffer: Buffer, length: number): Buffer {
  if (length <= buffer.length) {
    return
  }
  const newBuf = Buffer.alloc(length)
  buffer.copy(newBuf)
  return newBuf
}
