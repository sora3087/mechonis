export function GetPropertiesOf(obj: any): object {
  const ret: any = {}
  const props = Object.getOwnPropertyDescriptors(Object.getPrototypeOf(obj))
  Object.keys(props).forEach(key => {
    if (key !== 'constructor' && props[key].get) {
      ret[key] = obj[key]
    }
  })
  return ret
}
