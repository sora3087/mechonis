// export function swapBytes32(num: number): number {
//   let out: number = 0
//   out = (num >>> 24) & 0xff
//   | (num >>> 16) & 0xff << 8 >>> 0
//   | (num >>> 8) & 0xff << 16 >>> 0
//   | num & 0xff << 24 >>> 0
//   return out
// }
export function swapBytes16(uint16: number): number {
  return (uint16 >>> 8) + ((uint16 & 0xff) << 8)
}
