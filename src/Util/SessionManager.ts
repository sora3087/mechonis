export class SessionManager<T> {
  private sessions: Record<string, T> = {}
  public createSession(id: string, obj: T): void {
    this.sessions[id] = obj
  }
  public getSession(id: string): T {
    return this.sessions[id]
  }
  public deleteSession(id: string): void {
    this.sessions[id] = undefined
    delete this.sessions[id]
  }
}
