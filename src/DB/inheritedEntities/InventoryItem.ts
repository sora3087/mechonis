import { Column } from 'typeorm'
import { BaseEntity } from './BaseEntity'
import { ItemType } from '../../Common/enum/ItemType'

export class InventoryItem extends BaseEntity {
  @Column({
    type: 'enum',
    enum: ItemType
  })
  public type: ItemType

  @Column('int')
  public itemId: number

  @Column('int')
  public subId: number

  @Column('int')
  public qty: number
}
