import { CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'

export class BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number

  @CreateDateColumn()
  public created: Date

  @UpdateDateColumn()
  public modified: Date
}
