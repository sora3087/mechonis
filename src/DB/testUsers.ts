import { dbConnection } from './'
import { Character } from './entities/Character'
import { RegisteredGuildcard } from './entities/RegisteredGuildcard'
import { User } from './entities/User'

dbConnection.then(async (db) => {
  if (db) {
    const user = await db.getRepository(User).findOne({
      where: { guildcard: 100000 },
      // relations: [ 'characters' ]
    })
    console.log(user)
    const characters = await db.getRepository(Character).find({ where: {user} })
    console.log(characters)
    const registered = await db.getRepository(RegisteredGuildcard).find({ where: {user} })
    console.log(registered)
    db.close()
  }
})
