import { createConnection } from 'typeorm'
import Logger from '../Util/Logger'
import { User } from './entities/User'
// import { Controls } from './entities/Controls'
import { Bank } from './entities/Bank'
import { BankItem } from './entities/BankItem'
import { RegisteredGuildcard } from './entities/RegisteredGuildcard'
import { Character } from './entities/Character'
import { UserMeta } from './entities/UserMeta'
import { BagItem } from './entities/BagItem'
import { Team } from './entities/Team'
import { UserSubscriber } from './subscribers/UserSubscriber'

const {
  MECHONIS_DB_HOST,
  MECHONIS_DB_PORT,
  MECHONIS_DB_USER,
  MECHONIS_DB_PASS,
  MECHONIS_DB_NAME
} = process.env

export const dbConnection = createConnection({
  type: 'mysql',
  host: MECHONIS_DB_HOST || '127.0.0.1',
  port: parseInt(MECHONIS_DB_PORT, 10) || 3306,
  username: MECHONIS_DB_USER || 'mechonis',
  password: MECHONIS_DB_PASS || 'phantasy',
  database: MECHONIS_DB_NAME || 'mechonis',
  entityPrefix: 'mechonis_',
  entities: [
    BagItem,
    Bank,
    BankItem,
    Character,
    RegisteredGuildcard,
    Team,
    User,
    UserMeta
  ],
  subscribers: [
    UserSubscriber
  ],
  logging: true
})
.catch(err => {
  Logger.error('Could not connect to mysql')
  Logger.debug(err)
})
