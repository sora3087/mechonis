import { EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent } from 'typeorm'
import { sha256HashRounds } from '../../Auth/helpers'
import { User } from '../entities/User'

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<User> {
  public listenTo(): typeof User {
    return User
  }
  public beforeInsert(event: InsertEvent<User>): void {
    event.entity.password = sha256HashRounds(event.entity.password, 15)
  }
  public beforeUpdate(event: UpdateEvent<User>): void {
    if (event.entity.password) {
      event.entity.password = sha256HashRounds(event.entity.password, 15)
    }
  }
}
