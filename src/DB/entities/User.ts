import { Column, Entity, ManyToOne, OneToMany } from 'typeorm'
import { Bank } from './Bank'
import { Character } from './Character'
import { RegisteredGuildcard } from './RegisteredGuildcard'
import { Team } from './Team'
import { BaseEntity } from '../inheritedEntities/BaseEntity'

@Entity({
  name: 'users'
})
export class User extends BaseEntity {
  @Column({
    type: 'varchar',
    unique: true
  })
  public username: string

  @Column({
    type: 'varchar'
  })
  public password: string

  @Column({
    type: 'varchar',
    length: 16,
    unique: true
  })
  public guildcard: string

  @ManyToOne(type => Team, team => team.id)
  public team: Team

  @Column({
    type: 'varchar'
  })
  public email: string

  @Column({
    type: 'bool',
    default: true
  })
  public active: boolean

  @Column({
    type: 'blob'
  })
  public controls: Buffer

  @OneToMany(type => Character, character => character.user)
  public characters: Character[]

  @OneToMany(type => Bank, bank => bank.user)
  public banks: Bank[]

  @OneToMany(type => RegisteredGuildcard, gc => gc.user)
  public friends: RegisteredGuildcard[]
}
