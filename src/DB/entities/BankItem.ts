import { Column, Entity, ManyToOne } from 'typeorm'
import { Bank } from './Bank'
import { InventoryItem } from '../inheritedEntities/InventoryItem'

@Entity({
  name: 'bankItems'
})
export class BankItem extends InventoryItem {
  @ManyToOne(bank => Bank, bank => bank.id)
  public bank: Bank

  @Column('int')
  public showFlags: number
}
