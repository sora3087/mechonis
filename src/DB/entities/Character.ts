import { Column, Entity, ManyToOne, OneToMany } from 'typeorm'
import { User } from './User'
import { BaseEntity } from '../inheritedEntities/BaseEntity'
import { CharacterClass } from '../../Common/enum/CharacterClass'
import { BagItem } from './BagItem'
import { Color } from '../embeddedEntities/Color'
import { Proportion } from '../embeddedEntities/Proportion'
import { TechLevels } from '../embeddedEntities/TechLevels'
import { SectionId } from '../../Common/enum/SectionId'

@Entity({
  name: 'characters'
})
export class Character extends BaseEntity {
  @ManyToOne(type => User, user => user.characters)
  public user: User

  @Column({
    type: 'int',
    default: 0
  })
  public playTime: number

  @Column('int')
  public slot: number

  @Column('varchar')
  public name: string

  @Column('int', {
    default: 0
  })
  // amount of levels more than 1
  public level: number

  @Column('int', {
    default: 0
  })
  public exp: number

  @Column('int', {
    default: 0
  })
  public meseta: number

  @Column('int', {
    default: 0
  })
  public atp: number

  @Column('int', {
    default: 0
  })
  public mst: number

  @Column('int', {
    default: 0
  })
  public evp: number

  @Column('int', {
    default: 0
  })
  public hp: number

  @Column('int', {
    default: 0
  })
  public dfp: number

  @Column('int', {
    default: 0
  })
  public ata: number

  @Column('int', {
    default: 0
  })
  public lck: number

  @Column('int', {
    default: 0
  })
  public hpMat: number

  @Column('int', {
    default: 0
  })
  public tpMat: number

  @Column('int', {
    default: 0
  })
  public v1Flags: number

  @Column('int', {
    default: 0
  })
  public v2Flags: number

  @Column('int', {
    default: 1
  })
  public version: number

  @Column({
    type: 'enum',
    enum: SectionId,
    default: SectionId.Viridia
  })
  public section: SectionId

  @Column({
    type: 'enum',
    enum: CharacterClass,
    default: CharacterClass.HUmar
  })
  public class: CharacterClass

  @Column('int', {
    default: 0
  })
  public model: number

  @Column('int', {
    default: 0
  })
  public costume: number

  @Column('int', {
    default: 0
  })
  public skin: number

  @Column('int', {
    default: 0
  })
  public face: number

  @Column('int', {
    default: 0
  })
  public head: number

  @Column('int', {
    default: 0
  })
  public hair: number

  @Column(() => Color)
  public hairColor: Color

  @Column(() => Proportion)
  public proportion: Proportion

  @OneToMany(type => BagItem, item => item.id)
  public items: BagItem[]

  @Column(() => TechLevels)
  public techLevel: TechLevels

  @Column({
    type: 'blob'
  })
  public questData1: Buffer = Buffer.alloc(0x208)

  @Column({
    type: 'blob'
  })
  public questData2: Buffer = Buffer.alloc(0x58)
}
