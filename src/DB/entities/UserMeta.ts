import { Column, Entity, ManyToOne } from 'typeorm'
import { BaseEntity } from '../inheritedEntities/BaseEntity'
import { User } from './User'

@Entity()
export class UserMeta extends BaseEntity {
  @ManyToOne(type => User, user => user.id)
  public user: User

  @Column({
    type: 'varchar'
  })
  public key: string

  @Column({
    type: 'varchar'
  })
  public value: string
}
