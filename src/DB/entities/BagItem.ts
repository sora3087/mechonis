import { Column, Entity, ManyToOne } from 'typeorm'
import { Character } from './Character'
import { InventoryItem } from '../inheritedEntities/InventoryItem'

@Entity({
  name: 'bagItems'
})
export class BagItem extends InventoryItem {
  @ManyToOne(type => Character, character => character.id)
  public character: Character

  @Column('boolean')
  public equipped: boolean

  @Column('int')
  public flags: number
}
