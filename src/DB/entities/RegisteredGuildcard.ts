import { Column, Entity, ManyToOne, OneToOne } from 'typeorm'
import { User } from './User'
import { BaseEntity } from '../inheritedEntities/BaseEntity'
import { Character } from './Character'

// uint32_t guildcard; 4
// uint16_t name[0x18]; 48
// uint16_t team[0x10]; 32
// uint16_t desc[0x58]; 176
// uint8_t reserved1; 1
// uint8_t language; 1 enum
// uint8_t section; 1 enum
// uint8_t ch_class; 1 enum

@Entity({
  name: 'RegisteredGuildcards'
})
export class RegisteredGuildcard extends BaseEntity {
  @ManyToOne(type => User, user => user.id )
  public user: User

  // @OneToOne(type => User, user => user.id, { cascade: true })
  // public friend: User

  @Column('varchar')
  public guildcard: string

  @Column('varchar')
  public teamName: string
  
  // @OneToOne(type => Character, character => character.id, { cascade: true })
  // public character: Character
  @Column('varchar')
  public name: string

  @Column('int')
  public class: number

  @Column('int')
  public section: number

  @Column()
  public blocked: boolean

  @Column('varchar')
  public desc: string

  @Column('varchar')
  public comment: string
}
