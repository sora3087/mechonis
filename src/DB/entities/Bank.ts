import { Column, Entity, ManyToOne, OneToMany } from 'typeorm'
import { User } from './User'
import { BaseEntity } from '../inheritedEntities/BaseEntity'
import { BankItem } from './BankItem'

@Entity({
  name: 'banks'
})
export class Bank extends BaseEntity {
  @ManyToOne(type => User, user => user.banks)
  public user: User

  @Column({
    type: 'int'
  })
  public meseta: number

  @OneToMany(type => BankItem, bankItem => bankItem.bank)
  public items: BankItem[]
}
