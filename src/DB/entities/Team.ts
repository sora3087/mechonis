import { Column, Entity } from 'typeorm'
import { BaseEntity } from '../inheritedEntities/BaseEntity'

@Entity({
  name: 'teams'
})
export class Team extends BaseEntity {
  @Column('varchar')
  public name: string

  @Column({
    type: 'blob'
  })
  public flag: Buffer
}
