import { Column } from 'typeorm'

export class TechLevels {
  @Column('int', {
    default: 0
  })
  public 1: number
  @Column('int', {
    default: 0
  })
  public 2: number
  @Column('int', {
    default: 0
  })
  public 3: number
  @Column('int', {
    default: 0
  })
  public 4: number
  @Column('int', {
    default: 0
  })
  public 5: number
  @Column('int', {
    default: 0
  })
  public 6: number
  @Column('int', {
    default: 0
  })
  public 7: number
  @Column('int', {
    default: 0
  })
  public 8: number
  @Column('int', {
    default: 0
  })
  public 9: number
  @Column('int', {
    default: 0
  })
  public 10: number
  @Column('int', {
    default: 0
  })
  public 11: number
  @Column('int', {
    default: 0
  })
  public 12: number
  @Column('int', {
    default: 0
  })
  public 13: number
  @Column('int', {
    default: 0
  })
  public 14: number
  @Column('int', {
    default: 0
  })
  public 15: number
  @Column('int', {
    default: 0
  })
  public 16: number
  @Column('int', {
    default: 0
  })
  public 17: number
  @Column('int', {
    default: 0
  })
  public 18: number
  @Column('int', {
    default: 0
  })
  public 19: number
  @Column('int', {
    default: 0
  })
  public 20: number
}
