import { Column } from 'typeorm'

export class Color {
  @Column('int', {
    default: 0
  })
  public r: number
  @Column('int', {
    default: 0
  })
  public g: number
  @Column('int', {
    default: 0
  })
  public b: number
}
