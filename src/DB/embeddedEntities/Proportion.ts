import { Column } from 'typeorm'

export class Proportion {
  @Column('float', {
    default: 0
  })
  public x: number
  @Column('float', {
    default: 0
  })
  public y: number
}
