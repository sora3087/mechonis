import { MigrationInterface, QueryRunner } from 'typeorm'
import { CharacterData } from '../../Account/classes/CharacterData'
import { Character } from '../entities/Character'
import { User } from '../entities/User'

type TethCharacter = {
  guildcard: number
  slot: number
  data: Buffer
  header: Buffer
}

let lastGC = -1
export class UpgradeTethCharacters1 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    if (queryRunner.hasTable('character_data')) {
      const tethCharacters: TethCharacter[] = await queryRunner.manager.query('SELECT * FROM character_data ORDER BY guildcard, slot')
      let user: User
      for (let i = 0; i < tethCharacters.length; i++) {
        const tethCharacter = tethCharacters[i]
        if (lastGC !== tethCharacter.guildcard) {
          user = await queryRunner
          .manager.getRepository(User)
          .findOne({ where: { guildcard: 'TH-' + tethCharacter.guildcard.toString()} })
          lastGC = tethCharacter.guildcard
        }
        if (user) {
          const mechonisCharacter = new Character()
          const data = new CharacterData(tethCharacter.data, 0, CharacterData.size)
          mechonisCharacter.user = user
          mechonisCharacter.name = data.name
          mechonisCharacter.
        }
      }
    }
  }
  public async down(queryRunner: QueryRunner): Promise<any> { /* */ }
}
