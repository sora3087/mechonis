import { MigrationInterface, QueryRunner } from "typeorm";
import { ControlsDefault } from '../../Account/packets/ControlsDefault';
import { Bank } from '../entities/Bank';
import { Character } from '../entities/Character';
import { RegisteredGuildcard } from '../entities/RegisteredGuildcard';
import { User } from '../entities/User';
import { sha256HashRounds } from '../../Auth/helpers'

export class CreateTestData1631554328583 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    const users = queryRunner.manager.getRepository(User)
    const characters = queryRunner.manager.getRepository(Character)
    const banks = queryRunner.manager.getRepository(Bank)
    const registrations = queryRunner.manager.getRepository(RegisteredGuildcard)

    let baseGC = 100000;
    const insertUsers = [
      ['true', '12345', 'true@test.com', 'Char1'],
      ['Vision', 'secret12345', 'vision@test.com', 'Char2'],
      ['Test', 'something', 'test@test.com', 'Char3'],
      ['Bad', 'somethingelse', 'bad@test.com', 'Char4']
    ]
    for(let i = 0; i < insertUsers.length; i++) {
      const player = insertUsers[i]
      const user = new User()
      user.username = player[0]
      user.guildcard = `${baseGC + i}`
      user.controls = ControlsDefault
      user.password = sha256HashRounds(player[1], 15)
      user.email = player[2]
      await users.save(user)

      const char = new Character()
      char.user = user
      char.slot = 0
      char.name = player[3]
      await characters.save(char)

      const bank = new Bank()
      bank.user = user
      bank.meseta = Math.round(Math.random() * 100000)
      await banks.save(bank)
    }
    const insertedUsers = await users.find()
    const friends = [
      [insertedUsers[0], insertedUsers[1]],
      [insertedUsers[1], insertedUsers[0]]
    ]
    for(let i = 0; i < friends.length; i++){
      const friend = friends[i]
      const registration = new RegisteredGuildcard()
      registration.user = friend[0]
      const char = await characters.findOne({ where: { user: friend[1] } })
      registration.name = char.name
      registration.guildcard = friend[1].guildcard
      registration.teamName = ''
      registration.class = 0
      registration.section = 0
      registration.comment = 'test commment'
      registration.desc = 'test desc'
      registration.blocked = false
      await registrations.save(registration)
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
  }

}
