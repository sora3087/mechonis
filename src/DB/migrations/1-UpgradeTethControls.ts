import { MigrationInterface, QueryRunner } from 'typeorm'
import { User } from '../entities/User'

type KeyData = {
  guildcard: number
  controls: Buffer
}

export class UpgradeTethControls0 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    if (queryRunner.hasTable('key_data')) {
      const tethControls: KeyData[] = await queryRunner.manager.query('SELECT * FROM key_data')
      for (let i = 0; i < tethControls.length; i++) {
        const tethControl = tethControls[i]
        const user = await queryRunner
        .manager.getRepository(User)
        .findOne({ where: { guildcard: 'TH-' + tethControl.guildcard.toString() }})
        if (user) {
          const importedControls = tethControl.controls.slice(0x04)
          if (importedControls.length === 0x1A0) {
            user.controls = tethControl.controls.slice(0x04)
            await queryRunner.manager.save(user)
          }
        }
      }
    }
  }
  public async down(queryRunner: QueryRunner): Promise<any> { /* */ }
}
