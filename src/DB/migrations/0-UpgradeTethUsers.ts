import { MigrationInterface, QueryRunner } from 'typeorm'
import { ControlsDefault } from '../../Account/packets/ControlsDefault'
import { User } from '../entities/User'

type TethUser = {
  username: string
  password: string
  email: string
  regtime: number
  lastip: string
  lasthwinfo: Buffer
  guildcard: number
  isgm: number
  isbanned: number
  islogged: number
  isactive: number
  teamid: number
  privlevel: number
  lastchar: Buffer
}

export class UpgradeTethUsers0 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    if (queryRunner.hasTable('account_data')) {
      const tethUsers: TethUser[] = await queryRunner.manager.query('SELECT * FROM account_data')
      for (let i = 0; i < tethUsers.length; i++) {
        const tethUser = tethUsers[i]
        const mechonisUser = new User()
        mechonisUser.username = tethUser.username
        mechonisUser.active = tethUser.isactive === 1
        mechonisUser.created = this.convertRegisteredTime(tethUser.regtime)
        mechonisUser.controls = ControlsDefault
        mechonisUser.guildcard = 'TH-' + tethUser.guildcard.toString()
        mechonisUser.email = tethUser.email

        await queryRunner.manager.save(mechonisUser)
      }
    }
  }
  public async down(queryRunner: QueryRunner): Promise<any> { /* */ }
  private convertRegisteredTime(regtime: number): Date {
    const jsTime = ((regtime < 1000000) ? regtime * 3600 : regtime) * 1000
    return new Date(jsTime)
  }
}
