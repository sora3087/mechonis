import { PatchSession } from './PatchSession'
import { SessionManager } from '../Util/SessionManager'
 
export const patchSessions = new SessionManager<PatchSession>()
