import { CommandRoute } from '../../Network/CommandRouter'
import { PatchFilesComplete } from '../Packets'
import { PatchSession } from '../PatchSession'
import { patchSessions } from '../sessions'
import { patchStore } from '../Store'

export const RedirectedLogin: CommandRoute = (msg, next) => {
  patchSessions.createSession(msg.session, new PatchSession())

  // patch is disabled so tell the client the patch is compete
  if (!patchStore.patchEnabled || patchStore.fileList.length === 0) {
    next(new PatchFilesComplete(), true)
    return
  }

  patchStore.patchListBuffer.forEach(packetBuf => {
    // send the entire list back to the client for verification
    next(packetBuf)
  })
  return
}
