import { CommandRoute } from '../../Network/CommandRouter'
import { SendFileData } from '../clientCommands/SendFileData'
import { PatchFilesNeeded } from '../Packets'
import { patchSessions } from '../sessions'

export const ClientDoneListingVerifications: CommandRoute = (msg, next) => {
  const session = patchSessions.getSession(msg.session)
  // create patches summary
  let totalSize = 0

  session.filesNeeded.forEach((fileId) => {
    totalSize += session.files[fileId].size
  })

  const packet = new PatchFilesNeeded()
  packet.totalSize = totalSize
  packet.fileCount = session.filesNeeded.length

  next(packet)

  SendFileData(msg, next)
}
