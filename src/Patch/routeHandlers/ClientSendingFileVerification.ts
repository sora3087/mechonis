
import { CommandRoute } from '../../Network/CommandRouter'
import Logger from '../../Util/Logger'
import { PatchDisconnect, PatchFileVerification } from '../Packets'
import { patchSessions } from '../sessions'
import { patchStore } from '../Store'

export const ClientSendingFileVerification: CommandRoute = (msg, next) => {

  const session = patchSessions.getSession(msg.session)
  if (!session) {
    // dc if the client session wasn't started
    next(new PatchDisconnect(), true)
    return
  }

  const {fileId, checksum, fileSize} = new PatchFileVerification(msg.data)

  const fileMissing = (checksum === 0 && fileSize === 0)
  let sendUpdate: boolean = false
  if (fileId === 0) {
    // this is the channel tag file
    if (fileMissing) {
      Logger.info(`client had no configured patch channel`)
      // send back the file and set it to master
      sendUpdate = true
      // tslint:disable-next-line:no-string-literal
      session.files = patchStore.channels[patchStore.channelList['master']].files
      // copy the list of patch files to the session to use later
    } else {
      const patchChannel = patchStore.channels[checksum.toString(16)]
      if (!patchChannel) {
        Logger.warn(`client specified patch channel that doesn't exist\n looked for ${checksum.toString(16)}`)
        // channel specified doesn't exist in the patch server
        // send back the file and set it to master
        sendUpdate = true
        // set back to default master channel
        // tslint:disable-next-line:no-string-literal
        session.files = patchStore.channels[patchStore.channelList['master']].files
      } else {
        Logger.debug(`client using patch channel ${patchChannel.name}`)
        // copy the list of patch files to the session to use later
        session.files = patchChannel.files
      }
    }
  } else {
    // all other regular files added to patch directory
    const patchFile = session.files[fileId]
    if (patchFile && patchFile.checksum !== checksum) {
      Logger.debug(`checksum mismatch local: ${patchFile.checksum.toString(16)}, remote: ${checksum.toString(16)}`)
    }
    if (patchFile && (fileMissing || (!patchFile.optional && patchFile.checksum !== checksum))) {
      // if checked file is in sessions channel
      // and
        // file is missing
        // or
          // file is not optional and checksum didnt match
      sendUpdate = true
    }
  }
  if (sendUpdate) {
    // register on the session that the client needs to be sent this file
    session.filesNeeded.push(fileId)
  }
}

export default ClientSendingFileVerification
