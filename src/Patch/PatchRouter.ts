import { CommandRouter } from '../Network/CommandRouter'
import { RedirectedLogin } from './routeHandlers/RedirectedLogin'
import { ClientSendingFileVerification } from './routeHandlers/ClientSendingFileVerification'
import { ClientDoneListingVerifications } from './routeHandlers/ClientDoneListingVerifications'

export const PatchRouter = new CommandRouter()
PatchRouter.on(0x0401, RedirectedLogin)
PatchRouter.on(0x0f, ClientSendingFileVerification)
PatchRouter.on(0x10, ClientDoneListingVerifications)
