import { commandNextFn, CommandRoute } from '../../Network/CommandRouter'
import { Packet } from '../../Network/Packet'
import createFolderCommands from '../helpers/createFolderCommands'
import { PatchFileEnd, PatchFilesComplete } from '../Packets'
import { patchSessions } from '../sessions'

class PacketQueue {
  private queue: Packet[] = []
  private timer: NodeJS.Timer
  private processing: boolean = false
  constructor(private next: commandNextFn){}
  public add(data: Packet | Packet[]): void {
    if (data instanceof Packet) {
      data = [data]
    }
    this.queue = this.queue.concat(data)
  }
  public process(): void {
    if (this.processing) {
      return
    }
    this.timer = setInterval(() => this.shiftSend(), 200)
  }
  public onEnd(cb: () => void): void {
    this._onEnd = cb
  }
  private shiftSend(): void {
    const packet = this.queue.shift()
    if (!packet) {
      this.processing = false
      clearInterval(this.timer)
      this._onEnd()
      return
    }
    this.next(packet)
  }
  private _onEnd = () => { /* void */ }
}

export const SendFileData: CommandRoute = (msg, next) => {
  const session = patchSessions.getSession(msg.session)
  let currentDir = ''
  const queue = new PacketQueue(next)
  session.filesNeeded.forEach((fileId) => {
    const file = session.files[fileId]
    if (currentDir !== file.dir) {
      queue.add(createFolderCommands(currentDir, file.dir))
      currentDir = file.dir
    }
    queue.add(file.filePackets)
    queue.add(new PatchFileEnd())
  })
  queue.onEnd(() => {
    next(new PatchFilesComplete(), true)
    patchSessions.deleteSession(msg.session)
  })
  queue.process()
  return
}
