import { CommandRouter } from '../Network/CommandRouter'
// import DataAck from './routeHandlers/DataAck'
// import ClientSendingLogin from './routeHandlers/ClientSendingLogin'
// import ClientSendingFileVerification from './routeHandlers/ClientSendingFileVerification'
// import ClientDoneListingVerifications from './routeHandlers/ClientDoneListingVerifications'

const DataRouter = new CommandRouter()
// DataRouter.on(0x02, DataAck)
// DataRouter.on(0x04, ClientSendingLogin)
// DataRouter.on(0x0f, ClientSendingFileVerification)
// DataRouter.on(0x10, ClientDoneListingVerifications)
export default DataRouter
