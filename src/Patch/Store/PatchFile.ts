import { readFileSync } from 'fs'
import { dirname, basename } from 'path'
import * as colors from 'colors/safe'
import CRC32 from '../CRC32'
import { PatchChunk, PatchFileInfo } from '../Packets'
import Logger from '../../Util/Logger'
import { Packet } from '../../Network/Packet'
const maxFileMemory = 1024 * 1024 * 1024

const CHUNK_SIZE = 0x6000
let totalBufferedData = 0

function bufferChunks(buffer: Buffer, chunkSize: number): Array<Buffer> {
  const result = []
  const len = buffer.length
  let i = 0
  while (i < len) {
    result.push(buffer.slice(i, i += chunkSize))
  }
  return result
}

interface IPatchFileOptions {
  path: string
  root: string
}

export default class PatchFile {
  // public data: Buffer
  public size: number = 0
  public filePackets: Array<Packet> = []
  public path: string = ''
  public dir: string = ''
  public filename: string = ''
  public checksum: number
  public optional: boolean = false
  public globalIdx: number = 0
  constructor(data: Buffer, options: IPatchFileOptions)
  constructor(options: IPatchFileOptions)
  constructor(paramOne: Buffer | IPatchFileOptions, paramTwo?: IPatchFileOptions) {
    let data
    let options
    if (paramOne instanceof Buffer) {
      data = paramOne
      options = paramTwo
    } else {
      data = readFileSync(`${paramOne.root}/${paramOne.path}`)
      options = paramOne
    }
    this.path = options.path
    this.dir = dirname(options.path)
    this.filename = basename(options.path)
    this.size = data.length
    totalBufferedData += data.length
    if (totalBufferedData > maxFileMemory) {
      Logger.error(`[${this.constructor.name}] ${colors.red('Fatal error')}: memory limit reached when trying to cache file.\n  Memory Limit: ${maxFileMemory}, Cached size: ${totalBufferedData}`)
      process.kill(process.pid)
    }
    this.checksum = CRC32.hash(data, 0, data.length)
    this.createChunks(data)
  }
  private createChunks(data: Buffer): void {
    this.filePackets.push(new PatchFileInfo(data.length, this.filename))
    bufferChunks(data, CHUNK_SIZE).forEach((chunk, id) => {
      this.filePackets.push(new PatchChunk(id, chunk))
    })
  }
}
