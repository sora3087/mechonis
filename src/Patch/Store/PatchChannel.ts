import PatchFile from './PatchFile'
import { patchStore } from './index'
import { Stats, realpathSync, readFileSync } from 'fs'

import { GlobSync } from 'glob'
import toposort = require('toposort')
import Logger from '../../Util/Logger'

function pathToList(path: string): Array<string> {
  return readFileSync(path).toString()
      .split(/\n/g)
      .map((name) => name.trim())
      .filter((name) => name !== '')
}

export default class PatchChannel {
  public name: string = ''
  public rootDir: string = ''
  public dependencies: Array<[ string, string ]> = []
  public files: Array<PatchFile> = []
  constructor(name: string, path: string, stat: Stats) {
    this.name = name
    this.rootDir = path
    let optionals: Array<string> = []
    if (stat.isDirectory()) {
      if (this.name === 'master') {
        const channeltag = new PatchFile(Buffer.from('master'), {
          root: this.rootDir,
          path: patchStore.fileList[0]
        })
        channeltag.optional = true
        this.files[0] = channeltag
      }
      // Find some optional meta files
      try {
        const depsPath = realpathSync(this.rootDir + '/.depends')
        this.parseDeps(depsPath)
      } catch ( e ) {
        // no deps file
      }
      try {
        const optsPath = realpathSync(this.rootDir + '/.optional')
        optionals = pathToList(optsPath)
      } catch (e) {
        // no optional files listed
      }

      // Begin loading and pasring files
      const files = new GlobSync('**/*.*', {
        cwd: this.rootDir,
        root: this.rootDir,
        dot: false,
        nodir: true
      })

      files.found.forEach((filePath) => {
        const patchfile = new PatchFile({
          root: this.rootDir,
          path: filePath
        })
        let patchIdx = patchStore.fileList.indexOf(filePath)
        if (patchIdx === -1) {
          patchIdx = patchStore.fileList.length
          patchStore.fileList.push(filePath)
        }
        patchfile.globalIdx = patchIdx
        if (optionals.indexOf(filePath) !== -1) {
          patchfile.optional = true
        }
        this.files[patchIdx] = patchfile
      })
    } else {
      this.parseDeps(this.rootDir)
    }
  }
  public getDeps(): Array<[string, string]> {
    let out: Array<[string, string]> = this.dependencies
    this.dependencies.forEach(([name, dependent]) => {
      out = patchStore.channels[patchStore.channelList[dependent]].getDeps().concat(out)
    })
    return out
  }
  public resolveDeps(): void {
    const depGraph = this.getDeps()
    if (depGraph.length > 0) {
      const depOrder = toposort(depGraph).reverse()
      const finalFiles: Array<PatchFile> = []
      depOrder.forEach((name) => {
        if (name !== this.name) {
          const channel = patchStore.channels[patchStore.channelList[name]]
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < channel.files.length; i++) {
            if (channel.files[i]) {
              // we are just going to overwrite the last file set
              // we want to override with the current channel's version
              finalFiles[i] = channel.files[i]
            }
          }
        }
      })
      for (let i = 0; i < this.files.length; i++) {
        if (this.files[i]) {
          finalFiles[i] = this.files[i]
        }
      }
      this.files = finalFiles
    }
  }
  private parseDeps(path: string): void {
    pathToList(path)
      .forEach((name) => {
        if (patchStore.channelList[name]) {
          this.dependencies.push([this.name, name])
        } else {
          Logger.error(`Missing dependent channel '${name}' for channel '${this.name}'`)
          process.exit()
        }
      })
  }
}
