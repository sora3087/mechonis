import PatchChannel from './PatchChannel'
import { GlobSync } from 'glob'
import { statSync, realpathSync, Stats, existsSync } from 'fs'
import { basename } from 'path'
import CRC32 from '../CRC32'
import createPatchListBuffer from '../helpers/createPatchListBuffer'
import Logger from '../../Util/Logger'
import { Packet } from '../../Network/Packet'

export interface IPatchStoreOptions {
  rootDir: string
  patchEnabled?: boolean
  channelsEnabled?: boolean
}

interface IFoundChannel {
  name: string
  path: string
  stat: Stats
  hash: string
}

class PatchStore implements IPatchStoreOptions {
  public rootDir: string
  public patchEnabled: boolean
  public channelsEnabled: boolean
  public fileList: Array<string> = ['.channel'] // first file needs to be the channel tracking file
  public channelList: { [key: string]: string } = {} // the first channel needs to be master
  public channels: {
    [key: string]: PatchChannel
  } = {}
  public patchListBuffer: Array<Packet>

  public configure(options: IPatchStoreOptions = { rootDir: './patches' }): void {
    const opts: IPatchStoreOptions = {
      rootDir: '',
      patchEnabled: true,
      channelsEnabled: true,
      ...options
    }

    const masterPath = opts.rootDir + '/master/'
    const pathExists = existsSync(opts.rootDir)
    if (!pathExists) {
      Logger.error('Root patch directory missing')
    }
    const masterExists = existsSync(masterPath)
    if (!masterExists) {
      Logger.error('Master patch branch directory missing')
    }
    this.patchEnabled = opts.patchEnabled && pathExists && masterExists
    this.channelsEnabled = opts.channelsEnabled
    if (this.patchEnabled) {
      // do more processing of filsystem if patching is enabled
      this.rootDir = this.requireFolder(opts.rootDir).path
      const master = this.requireFolder(masterPath)
      const hash = this.getChannelNameHash('master')
      const defaultChannels = [{
        name: 'master',
        path: master.path,
        hash,
        stat: master.stat
      }]
      this.channelList = { master: hash }
      if (this.channelsEnabled) {
        // find the rest of the channels if channels are enabled
        this.findChannels(defaultChannels)
      } else {
        // just start loading master if channels are disabled
        this.loadChannels(defaultChannels)
      }
    }
  }

  private requireFolder(path: string): { path: string, stat: Stats } {
    try {
      path = realpathSync(path)
    } catch (e) {
      Logger.error(`[${this.constructor.name}] required directory "${path}" doesn't exist.`)
      process.kill(process.pid)
    }
    const stat = statSync(path)
    if (!stat.isDirectory()) {
      Logger.error(`[${this.constructor.name}] required path "${path}" wasn't a directory.`)
      process.kill(process.pid)
    }
    return { path, stat }
  }
  private getChannelNameHash(name: string): string {
    return CRC32.hash(Buffer.from(name)).toString(16)
  }
  private findChannels(out: Array<IFoundChannel>): void {
    // Todo: clean up this section to not use redundant goddamn code
    const checksums: Array<string> = []
    const channelDirs = new GlobSync(this.rootDir + '/*/', {})
    channelDirs.found.forEach((path) => {
      const stat = channelDirs.statCache[path] as Stats
      if (stat.isDirectory) {
        const name = basename(path).toLowerCase()
        if (name !== 'master') {
          const hash = this.getChannelNameHash(name)
          if (checksums.indexOf(hash) === -1) {
            out.push({
              name,
              path,
              stat,
              hash
            })
            checksums.push(hash)
            this.channelList[name] = hash
          } else {
            Logger.warn(`Patch channel with name: ${name}, had a hash collision and was not loaded`)
          }
        }
      }
    })
    const channelVirtual = new GlobSync(this.rootDir + '/*.depends', {})
    channelVirtual.found.forEach((path) => {
      const stat = statSync(path)
      if (stat.size > 0 ) {
        const name = basename(path, '.depends').toLowerCase()
        const hash = this.getChannelNameHash(name)
        if (checksums.indexOf(hash) === -1) {
          out.push({
            name,
            path,
            stat,
            hash
          })
          checksums.push(hash)
          this.channelList[name] = hash
        } else {
          Logger.warn(`Patch channel with name: ${name}, had a hash collision and was not loaded`)
        }
      }
    })
    this.loadChannels(out)
  }

  private loadChannels(foundChannels: Array<IFoundChannel>): void {
    foundChannels.forEach((foundChannel) => {
      const channel = new PatchChannel(foundChannel.name, foundChannel.path, foundChannel.stat)
      if (channel.files.length === 0 && channel.dependencies.length === 0 && channel.name !== 'master') {
        Logger.warn(`Removed empty channel '${channel.name}' from list, if this was a dependency it will cause errors`)
        this.channelList[foundChannel.name] = undefined
        delete this.channelList[foundChannel.name]
      } else {
        this.channels[foundChannel.hash] = channel
      }
    })
    this.resolveDeps()
  }
  private resolveDeps(): void {
    // Todo
    for (const hash in this.channels) {
      if (this.channels.hasOwnProperty(hash)) {
        const channel = this.channels[hash]
        channel.resolveDeps()
      }
    }
    this.createPatchListBuffer()
  }
  private createPatchListBuffer(): void {
    this.patchListBuffer = createPatchListBuffer(this.fileList)
  }
}

export const patchStore = new PatchStore()
