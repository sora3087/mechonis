import { Packet } from '../Network/Packet'
import { roundIncrement } from '../Util/Numbers'
import CRC32 from './CRC32'
// tslint:disable max-classes-per-file

/**
 * Prebuilt packet]
 * 0400 15 00
 */
export class PatchDisconnect extends Packet {
  constructor() {
    super(0x04, 0x15)
  }
}

/**
 * Prebuilt packet\
 * 0400 0B 00
 */
export class PatchStartFileList extends Packet {
  constructor() {
    super(0x04, 0x0B)
  }
}

/**
 * Prebuilt packet\
 * 0400 0A 00
 */
export class PatchChangeDirectoryUp extends Packet {
  constructor() {
    super(0x04, 0x0A)
  }
}

/**
 * Packet builder\
 * 4400 09 00\
 * [64 byte directory name]
 */
export class PatchChangeDirectoryNamed extends Packet {
  constructor(dirname: string) {
    super(0x44, 0x09)
    this.buffer.write(dirname, 0x04)
  }
}

/**
 * Packet builder\
 * 2800 0C 00\
 * [UInt32 file ID]\
 * [32 byte filename]
 */
export class PatchFileVerificationRequest extends Packet {
  constructor(fileId: number, filename: string) {
    if (filename.length > 32) {
      throw new Error('filename length too long, 32 characters maximum')
    }
    super(0x28, 0x0C)
    this.buffer.writeUInt32LE(fileId, 0x04)
    this.buffer.write(filename, 0x08)
  }
  get fileId(): number {
    return this.buffer.readUInt32LE(0x04)
  }
  get filename(): string{
    return this.buffer.slice(0x08).toString().trim()
  }
}

/**
 * Parser packet\
 * 0000 0F 00
 * [unit32LE ]
 */
export class PatchFileVerification extends Packet {
  get fileId(): number {
    return this.buffer.readUInt32LE(0x04)
  }
  get checksum(): number {
    return this.buffer.readUInt32LE(0x08)
  }
  get fileSize(): number {
    return this.buffer.readUInt32LE(0x0C)
  }
}

/**
 * Prebuilt packet\
 * 0400 0D 00
 */
export class PatchEndFileList extends Packet {
  constructor() {
    super(0x04, 0x0D)
  }
}

/**
 * 0C00 11 00\
 * [UInt32 total bytes of all pending files]\
 * [UInt32 total number of pending files]
 */
export class PatchFilesNeeded extends Packet {
  constructor() {
    super(0x0C, 0x11)
  }
  get totalSize(): number {
    return this.buffer.readUInt32LE(0x04)
  }
  set totalSize(val: number) {
    this.buffer.writeUInt32LE(val, 0x04)
  }
  get fileCount(): number {
    return this.buffer.readUInt32LE(0x08)
  }
  set fileCount(val: number) {
    this.buffer.writeUInt32LE(val, 0x08)
  }
}

/**
 * Packet builder\
 * 3C00 06 00\
 * [4 byte padding]\
 * [uint32 file size]\
 * [48 byte filename]
 */
export class PatchFileInfo extends Packet {
  constructor(fileSize: number, filename: string) {
    super(0x3C, 0x06)
    this.buffer.writeUInt32LE(fileSize, 0x08)
    this.buffer.write(filename, 0x0C)
  }
}

/**
 * Prebuilt packet\
 * 0400 08 00
 */
export class PatchFileEnd extends Packet {
  constructor() {
    super(0x04, 0x08)
  }
}

/**
 * Prebuilt packet\
 * 0400 12 00
 */
export class PatchFilesComplete extends Packet {
  constructor() {
    super(0x04, 0x12)
  }
}

/**
 * Packet builder\
 * xxxx 07 00\
 * [uint32 chunk number]\
 * [uint32 chunk checksum]\
 * [uint32 data size]\
 * [data]
 */
export class PatchChunk extends Packet {
  constructor(chunkId: number, chunkData: Buffer) {
    if (chunkData.length > 0x6000) {
      throw new Error('')
    }
    super(roundIncrement(0x10 + chunkData.length, 4), 0x07)
    this.buffer.writeUInt32LE(chunkId, 0x04)
    this.buffer.writeUInt32LE(CRC32.hash(chunkData), 0x08)
    this.buffer.writeUInt32LE(chunkData.length, 0x0C)
    chunkData.copy(this.buffer, 0x10)
  }
}
