import { green, red } from 'colors/safe'
import { EncryptionType } from '../Network/EncryptionType'
import { Service } from '../Network/ServiceBase'
import Logger from '../Util/Logger'
import { PatchRouter } from './PatchRouter'
import { patchStore } from './Store'

const {
  MECHONIS_MACHINE_ID,
  MECHONIS_REDIS_HOST
} = process.env

const SERVICE_NAME = 'patch'

patchStore.configure()

if (!patchStore.patchEnabled) {
  Logger.info('Patching is: ' + red('disabled'))
} else {
  Logger.info('Patching is: ' + green('enabled'))
}

const service = new Service({
    serviceName: SERVICE_NAME,
    hostId: MECHONIS_MACHINE_ID,
    redis: {
      host: MECHONIS_REDIS_HOST
    },
    ports: {
      11001: { type: EncryptionType.PSO, redirect: false }
    }
  },
  PatchRouter
)

const exit = async () => {
  const destroyed = await service.destroy()
  console.log('awaited destroy', destroyed)
  process.exit()
}

process.on('beforeExit', exit)
process.on('SIGINT', exit)
