import createFolderCommands from './createFolderCommands'
import { dirname, basename } from 'path'
import { PatchStartFileList, PatchFileVerificationRequest, PatchEndFileList } from '../Packets'
import { Packet } from '../../Network/Packet'

export default function createPatchListBuffer(fileList: Array<string>): Array<Packet> {
  let commands: Array<Packet> = []
  let currentDir: string = ''
  commands.push(new PatchStartFileList())
  fileList.forEach((path, id) => {
    let patchDir = dirname(path)
    if (patchDir === '.') {
      patchDir = ''
    }
    const filename = basename(path)
    if (currentDir !== patchDir) {
      commands = [...commands, ...createFolderCommands(currentDir, patchDir)]
      currentDir = patchDir
    }
    const fileCommand = new PatchFileVerificationRequest(id, filename)
    commands.push(fileCommand)
  })
  commands.push(new PatchEndFileList())
  return commands
}
