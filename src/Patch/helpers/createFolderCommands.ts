import {
  PatchChangeDirectoryNamed,
  PatchChangeDirectoryUp
} from '../Packets'
import { relative, delimiter } from 'path'
import { Packet } from '../../Network/Packet'

/**
 * Creates a list of packets to navigate between two relative directories for patching
 * @param currentDir The current directory of the patches
 * @param patchDir The next folder to navigate to
 */
export default function createFolderCommands(currentDir: string, patchDir: string): Array<Packet> {
  const commands: Array<Packet> = []
  const relativePath = relative(
    currentDir,
    patchDir
  )
  if (relativePath !== '') {
    relativePath
    .split(delimiter)
    .forEach((part) => {
      if (part === '..') {
        commands.push(new PatchChangeDirectoryUp())
      } else {
        commands.push((new PatchChangeDirectoryNamed(part)))
      }
    })
  }
  return commands
}
