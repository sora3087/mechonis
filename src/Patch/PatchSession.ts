import { CommandRouter } from '../Network/CommandRouter'
import PatchFile from './Store/PatchFile'

export enum SessionMode {
  PATCH,
  DATA
}

export class PatchSession {
  public files: Array<PatchFile> = []
  public filesNeeded: Array<number> = []
}
