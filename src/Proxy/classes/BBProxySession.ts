import { ProxySession } from './ProxySession'
import { BBHandshake } from '../../Gate/packets/BBHandshake'
import { BBEncryption } from '../../Gate/classes/BBEncyption'
import { hexy } from 'hexy'

export class BBProxySession extends ProxySession {
  protected byteAlign = 8
  protected initEncryption(packet: Buffer): void {
    const message = new BBHandshake()
    packet.copy(message.buffer)
    // console.log('Initialized BB encryption with:')
    // console.log(hexy(message.clientVector))
    // console.log(hexy(message.serverVector))
    this.clientCipher = new BBEncryption(message.clientVector)
    this.upstreamCipher = new BBEncryption(Buffer.from(message.serverVector))
    this.downstreamCipher = new BBEncryption(Buffer.from(message.serverVector))
  }
}
