import { ProxySession } from './ProxySession'
import { PatchHandshake } from '../../Gate/packets/PatchHandshake'
import { PSOEncryption } from '../../Gate/classes/PSOEncryption'
import { hexy } from 'hexy'

export class PSOProxySession extends ProxySession {
  protected byteAlign = 4
  protected initEncryption(packet: Buffer): void {
    const message = new PatchHandshake()
    packet.copy(message.buffer)
    // console.log('Initialized PSO encryption with:')
    // console.log(hexy(message.clientVector))
    // console.log(hexy(message.serverVector))
    this.clientCipher = new PSOEncryption(message.clientVector)
    this.upstreamCipher = new PSOEncryption(message.serverVector)
    this.downstreamCipher = new PSOEncryption(message.serverVector)
  }
}
