import { bgBlue, bgGreen, bgYellow, black, blue, green, yellow } from 'colors/safe'
import { writeFile } from 'fs'
import { hexy } from 'hexy'
import { Socket } from 'net'
import { EventEmitter } from 'stream'
import { v4 as uuidv4 } from 'uuid'
import { IEncryption } from '../../Gate/types/IEncryption'
import FIFOBuffer from '../../Util/FIFOBuffer'
import Logger from '../../Util/Logger'

const outputDir = './packetDumps/'

export abstract class ProxySession extends EventEmitter {
  public upstream: Socket
  public readonly id: string = ''
  protected encrypted: boolean = false
  protected clientCipher: IEncryption
  protected downstreamCipher: IEncryption
  protected upstreamCipher: IEncryption
  protected byteAlign = 1
  private upstreamBuffer: FIFOBuffer
  private reading: boolean = false
  constructor(public client: Socket, private downstreamPort: number, upstreamPort: number, upstreamHost: string) {
    super()
    this.id = uuidv4()
    this.upstreamBuffer = new FIFOBuffer()
    this.upstream = new Socket()
    this.upstream.connect(upstreamPort, upstreamHost)
    this.upstream.on('error', () => {
      Logger.error('Failed connecting to upstream service')
      this.emit('close')
    })
    this.upstream.on('data', (data) => {
      if (!this.encrypted) {
        this.initEncryption(data)
        this.encrypted = true
        this.client.write(data)
      } else {
        this.decryptOutbound(data)
      }
    })
    this.client.on('data', (data) => {
      if (this.encrypted) {
        this.decryptInbound(data)
      }
      this.upstream.write(data)
    })
    this.upstream.on('close', () => {
      this.emit('close')
      if (!this.client.destroyed) {
        this.client.destroy()
      }
    })
    this.client.on('close', () => {
      this.emit('close')
      if (!this.upstream.destroyed) {
        this.upstream.destroy()
      }
    })
  }
  protected abstract initEncryption(message: Buffer): void
  private decryptOutbound(data: Buffer): void {
    this.upstreamCipher.decrypt(data, 0, data.length)
    this.upstreamBuffer.enq(data)
    if (!this.reading) {
      // if not reading then we can parse a message from the buffer
      this.recieveParse()
    }
  }
  private decryptInbound(originalData: Buffer): void {
    const messageCopy = Buffer.from(originalData)
    this.clientCipher.decrypt(messageCopy, 0, messageCopy.length)
    writeFile(
      outputDir + `${Date.now()}-${this.downstreamPort}-in-${messageCopy.slice(0x02, 0x4).toString('hex')}.bin`,
      messageCopy, { flag: 'w+' }, () => { /**/ }
    )
    // Logger.info(`${bgBlue(black('\ue0b0') + ' INBOUND ') + blue('\ue0b0')}\n` + hexy(messageCopy))
  }
  private sendDownstream(data: Buffer): void {
    if (data[0x02] === 0x14) {
      this.modifyPatchRedirect(data)
    } else if (data[0x02] === 0x19) {
      this.modifyBBRedirect(data)
    }
    if (this.byteAlign !== 1 && data.length % this.byteAlign !== 0) {
      const newData = Buffer.alloc(data.length + (this.byteAlign - (data.length % this.byteAlign)))
      data.copy(newData)
      data = newData
    }
    // Logger.info(`${green('\ue0b2') + bgGreen(' OUTBOUND ' + black('\ue0b2'))}\n` + hexy(data))
    writeFile(
      outputDir + `${Date.now()}-${this.downstreamPort}-out-${data.slice(0x02, 0x4).toString('hex')}.bin`,
      Buffer.from(data),
      { flag: 'w+' },
      () => { /**/ }
    )
    this.downstreamCipher.encrypt(data, 0, data.length)
    this.client.write(data)
  }
  private modifyPatchRedirect(data: Buffer): void {
    const port = data.readUInt16BE(0x08)
    data.writeUInt16BE(port - 100, 0x08)
  }
  private modifyBBRedirect(data: Buffer): void {
    const port = data.readUInt16LE(0x0C)
    data.writeUInt16LE(port - 100, 0x0C)
  }
  private recieveParse(): void {
    this.reading = true
    let peekBuf
    // tslint:disable-next-line:no-conditional-assignment
    while (peekBuf = this.upstreamBuffer.peek(2)) {
      // seek the buffer 2 bytes at a time and to get a message length
      const messageLength = peekBuf.readUInt16LE(0)
      if (messageLength === 0 ) {
        // if the message length is 0 it means that we are probably looking at padding
        if (this.upstreamBuffer.size >= 2) {
          // dequeue the data
          this.upstreamBuffer.deq(2)
        } else {
          // dump the data
          this.upstreamBuffer.drain()
        }
        continue
      }
      if (this.upstreamBuffer.size < messageLength) {
        // message length includes size
        break
      }
      // we have a complete message to send to the router
      // pull data from buffer
      const message = this.upstreamBuffer.deq(messageLength)
      // send the buffer to the router
      this.sendDownstream(message)
    }
    // done reading data
    this.reading = false
  }
}
