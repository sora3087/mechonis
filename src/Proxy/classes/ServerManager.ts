import { Server, Socket } from 'net'
import { EncryptionType } from '../../Network/EncryptionType'
import Logger from '../../Util/Logger'
import { ServerDefinition } from '../types/ServerDefinition'
import { BBProxySession } from './BBProxySession'
import { ProxySession } from './ProxySession'
import { PSOProxySession } from './PSOProxySession'

export class ServerManager{
  private servers: Record<number, Server> = {}
  private sessions: Record<string, ProxySession> = {}
  constructor(serverDefs: Array<ServerDefinition>) {
    serverDefs.forEach(def => this.registerServer(def))
  }
  public registerServer(definition: ServerDefinition ): void {
    const [port, upstreamPort, upstreamHost, encryptionType] = definition
    if (this.servers[port]) {
      Logger.warn(`server already registered on port: ${port}`)
      return
    }
    const server = new Server()
    server.on('listening', () => {
      Logger.info('Opened a new port ' + port)
    })
    server.on('connection', (socket: Socket) => {
      Logger.info(`accepted a connection from ${socket.remoteAddress} on port ${port}`)
      socket.on('close', () => Logger.info(`socket to ${socket.remoteAddress} closed`))
      let session: ProxySession
      if (encryptionType === EncryptionType.BB) {
        session = new BBProxySession(socket, port, upstreamPort, upstreamHost)
      } else {
        session = new PSOProxySession(socket, port, upstreamPort, upstreamHost)
      }
      // session is initialized and handshake is sent by the session itselfw
      session.on('closed', () => {
        Logger.info(`Session ${session.id} closed`)
        this.sessions[session.id] = undefined
        delete this.sessions[session.id]
      })
      this.sessions[session.id] = session
    })
    this.servers[port] = server
    server.listen(port)
  }
}
