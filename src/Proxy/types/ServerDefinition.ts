import { EncryptionType } from '../../Network/EncryptionType'

export type ServerDefinition = [number, number, string, EncryptionType]
