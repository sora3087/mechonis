import {EncryptionType} from '../Network/EncryptionType'
import { ServerManager } from './classes/ServerManager'
import { ServerDefinition } from './types/ServerDefinition'

const serverDefs: Array<ServerDefinition> = [
  [11000, 11100, '127.0.0.1', EncryptionType.PSO],
  [11001, 11101, '127.0.0.1', EncryptionType.PSO],
  [12000, 12100, '127.0.0.1', EncryptionType.BB],
  [12001, 12101, '127.0.0.1', EncryptionType.BB]
]

new ServerManager(serverDefs)