export enum CharacterClass {
  HUmar,
  HUnewearl,
  HUcast,
  HUcaseal,
  RAmar,
  RAmarl,
  RAcast,
  RAcaseal,
  FOmar,
  FOmarl,
  FOnewm,
  FOnewearl,
}
