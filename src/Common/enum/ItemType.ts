export enum ItemType {
  Weapon,
  Armor,
  Mag,
  Consumable,
}