export enum WeaponBonusType {
  Native = 1,
  ABeast,
  Machine,
  Dark,
  Hit
}
