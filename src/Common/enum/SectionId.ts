export enum SectionId {
  Viridia,
  Greenill,
  Skyly,
  Bluefull,
  Purplenum,
  Pinkal,
  Redria,
  Oran,
  Yellowboze,
  Whitill,
}
