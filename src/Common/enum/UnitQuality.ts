export enum UnitQuality {
  DoubleMinus = -2,
  Minus = -1,
  Standard = 0,
  Plus = 1,
  Unknown = 2,
  DoublePlus = 3,
}
