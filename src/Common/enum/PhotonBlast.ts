export enum PhotonBlast {
  Farlla      = 0x00, //000
  Estlla      = 0x01, //001
  Golla       = 0x02, //010
  Pilla       = 0x03, //011
  Leilla      = 0x04, //100
  MyllaYoulla = 0x05, //101
}

//11011100