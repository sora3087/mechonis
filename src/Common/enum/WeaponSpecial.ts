export enum WeaponSpecial {
  None = 0x00,
  Draw = 0x01,
  Drain = 0x02,
  Fill = 0x03,
  Gush = 0x04,
  Heart = 0x05,
  Mind = 0x06,
  Soul = 0x07,
  Geist = 0x08,
  Masters = 0x09,
  Lords = 0x0A,
  Kings = 0x0B,
  Charge = 0x0C,
  Spirit = 0x0D,
  Berserk = 0x0E,
  Ice = 0x0F,
  Frost = 0x10,
  Freeze = 0x11,
  Blizzard = 0x12,
  Bind = 0x13,
  Hold = 0x14,
  Seize = 0x15,
  Arrest = 0x16,
  Heat = 0x17,
  Fire = 0x18,
  Flame = 0x19,
  Burning = 0x1A,
  Shock = 0x1B,
  Thunder = 0x1C,
  Storm = 0x1D,
  Tempest = 0x1E,
  Dim = 0x1F,
  Shadow = 0x20,
  Dark = 0x21,
  Hell = 0x22,
  Panic = 0x23,
  Riot = 0x24,
  Havoc = 0x25,
  Chaos = 0x26,
  Devils = 0x27,
  Demons = 0x28,
  Wrapped = 0x50,
  WrappedUntekked = 0xFF,
  Untekked = 0xAA,
}