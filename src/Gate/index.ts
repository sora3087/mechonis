
// import { Command } from 'commander'
import { createServer } from 'http'
import { Server } from 'socket.io'
import { v4 as uuidv4 } from 'uuid'
import { Alvis } from '../Network/Alvis'
import { getPublicIp, localIp } from '../Network/localIp'
import { redis } from '../Network/RedisClient'
import Logger from '../Util/Logger'
import { ServerManager } from './classes/ServerManager'
import { ServiceRouter } from './classes/ServiceRouter'

const {
  MECHONIS_MACHINE_ID,
  MECHONIS_REDIS_HOST
} = process.env

const SERVICE_NAME = 'gate'

// const program = new Command()
// program
// .option('-c, --consul [host]', 'specify consul host to register with', MECHONIS_CONSUL_HOST || '127.0.0.1')
// .parse()

// const CONFIG = program.opts<{consul: string}>()

// console.log(CONFIG)

const SERVICE_ID = uuidv4()
const server = createServer()

const io = new Server(server)

server.on('listening', () => {
  Logger.info('Gate listening for services on port ' + 3000)
})

const start = async () => {
  const ip = await getPublicIp()
  await redis.init(MECHONIS_REDIS_HOST)
  const alvis = new Alvis({
    serviceName: 'gate',
    hostId: MECHONIS_MACHINE_ID || '',
    cacheServices: true,
    redis: {
      host: MECHONIS_REDIS_HOST
    },
    taggedAddresses: {
      wan: { Address: ip, Port: 3000 },
      lan: { Address: localIp(), Port: 3000 }
    }
  })
  const deregisterService = () => {
    server.close()
    alvis.deregister()
    .then(() => {
      process.exit()
    })
  }
  process.on('beforeExit', deregisterService)
  process.on('SIGINT', deregisterService)
  await alvis.init()
  await alvis.register()

  // Gate service only registers itself so that other services can find and connect to it

  server.listen(3000)
  const serviceRouter = new ServiceRouter(io)
  const serverManager = new ServerManager(io, serviceRouter)
}

start()
