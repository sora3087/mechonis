import { Packet } from '../../Network/Packet'

export class PatchHandshake extends Packet {
  constructor() {
    super(0x4c, 0x02)
    this.buffer.write('Patch Server. Copyright SonicTeam, LTD. 2001', 0x04)
  }
  private writeVector(vector: Buffer, offset: number): void {
    if (vector.length !== 0x04) {
      throw new Error('Incorrect vector size')
    }
    vector.copy(this.buffer, offset)
  }
  public set clientVector(value: Buffer) {
    this.writeVector(value, 0x48)
  }
  public get clientVector(): Buffer {
    return this.buffer.slice(0x48, 0x48 + 0x04)
  }
  public set serverVector(value: Buffer) {
    this.writeVector(value, 0x44)
  }
  public get serverVector(): Buffer {
    return this.buffer.slice(0x44, 0x44 + 0x04)
  }
}
