import { Packet } from '../../Network/Packet'

export class BBHandshake extends Packet {
  constructor() {
    super(0xC8, 0x3)
    this.buffer.write('Phantasy Star Online Blue Burst Game Server. Copyright 1999-2004 SONICTEAM.', 0x8)
  }
  private writeVector(vector: Buffer, offset: number): void {
    if (vector.length !== 0x30) {
      throw new Error('Incorrect vector size')
    }
    vector.copy(this.buffer, offset)
  }
  set serverVector(value: Buffer) {
    this.writeVector(value, 0x68)
  }
  get serverVector(): Buffer {
    return this.buffer.slice(0x68, 0x68 + 0x30)
  }
  set clientVector(value: Buffer) {
    this.writeVector(value, 0x98)
  }
  get clientVector(): Buffer {
    return this.buffer.slice(0x98, 0x98 + 0x30)
  }
}
