import { PSOEncryption } from './PSOEncryption'
import { PatchHandshake } from '../packets/PatchHandshake'
import { Session } from './Session'

export class PSOSession extends Session {
  public init(): void {
    // create encryption for server and client
    // maybe user random buffer fill instead
    const clientIV = Buffer.alloc(4)
    clientIV.writeUInt32LE((Math.round(Math.random() * Date.now()) & 0xffffffff) >>> 0, 0)
    this.clientEncryption = new PSOEncryption(clientIV)
    const serverIV = Buffer.alloc(4)
    serverIV.writeUInt32LE((Math.round(Math.random() * Date.now()) & 0xffffffff) >>> 0, 0)
    this.serverEncryption = new PSOEncryption(serverIV)

    // create packet to send vectors to client
    const handshake = new PatchHandshake()
    handshake.clientVector = clientIV
    handshake.serverVector = serverIV

    // send vectors to client
    this.write(handshake.buffer)

    // now the messages are encrypted
    this.isEncrypted = true
  }
}
