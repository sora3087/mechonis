import Logger from '../../Util/Logger'
import { IEncryption } from '../types/IEncryption'

export class PSOEncryption implements IEncryption {
  private keys: Uint32Array
  private position: number
  constructor(iv: Buffer) {
    if (iv.length !== 4) {
      Logger.fatal('pso encryption was created with the wrong length key')
    }
    // create new array of keys to use in encryption
    this.keys = new Uint32Array(56)
    this.createKeys(iv.readUInt32LE(0))
  }
  public encrypt(data: Buffer): void {
    this.cryptData(data, 0, data.length)
  }
  public decrypt(data: Buffer): void {
    this.cryptData(data, 0, data.length)
  }
  private createKeys(IV: number): void {
    let index

    let key2 = 1
    this.keys[55] = IV

    for (let i1 = 0x15; i1 <= 0x46E; i1 += 0x15) {
        index = (i1 % 55)
        IV -= key2
        this.keys[index] = key2
        key2 = IV
        IV = this.keys[index]
    }

    this.mixKeys()
    this.mixKeys()
    this.mixKeys()
    this.mixKeys()
    this.position = 56
  }
  private mixKeys(): void {
    for (let i1 = 0x18, i2 = 0x01; i1 > 0; i1--, i2++) {
        this.keys[i2] -= this.keys[i2 + 0x1F]
    }
    for (let i1 = 0x1F, i2 = 0x19; i1 > 0; i1--, i2++) {
        this.keys[i2] -= this.keys[i2 - 0x18]
    }
  }
  /**
   * Encrypts and decrytps data depending on source
   * @param data
   * @param index
   * @param length
   */
  private cryptData(data: Buffer, index: number, length: number): void {
    length += index
    for (let i = index; i < length; i += 4) {
      this.cryptU32(data, i)
    }
  }
  private UInt32GetByte(uint32: number, byteNum: number): number {
    return uint32 >>> (byteNum * 8) & 0xff
  }
  private cryptU32(data: Buffer, offset: number): void {
      if (this.position === 56) {
          this.mixKeys()
          this.position = 1
      }
      const key = this.keys[this.position]
      data[offset + 0] = (data[offset + 0] ^ this.UInt32GetByte(key, 0))
      data[offset + 1] = (data[offset + 1] ^ this.UInt32GetByte(key, 1))
      data[offset + 2] = (data[offset + 2] ^ this.UInt32GetByte(key, 2))
      data[offset + 3] = (data[offset + 3] ^ this.UInt32GetByte(key, 3))
      this.position++
  }
}
