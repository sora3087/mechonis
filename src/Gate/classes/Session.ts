import { bgGreen, bgRed, bgYellow, black, blue } from 'colors/safe'
import { Socket } from 'net'
import { v4 as uuidv4 } from 'uuid'
import { redis } from '../../Network/RedisClient'
import FIFOBuffer from '../../Util/FIFOBuffer'
import Logger from '../../Util/Logger'
import { IEncryption } from '../types/IEncryption'
import { ServiceRouter } from './ServiceRouter'

const PING_INTERVAL = 5 * 1000

export abstract class Session {
  public disconnectAfterSend: boolean = false
  protected byteBoundry: number = 4
  protected clientEncryption: IEncryption
  protected serverEncryption: IEncryption
  protected isEncrypted: boolean = false
  protected rateLimited: boolean = false
  private _reading: boolean = false
  private incomingBuffer: FIFOBuffer
  private _id: string
  constructor(protected socket: Socket, protected router: ServiceRouter) {
    this._id = uuidv4()
    this.init()
    this.incomingBuffer = new FIFOBuffer()
    socket.on('data', this._onData)
    socket.on('error', () => redis.deleteSession(this._id))
  }
  public get id(): string {
    return this._id
  }
  public onEnd(cb: () => void): void {
    this.socket.on('end', cb)
  }
  public onClosed(cb: () => void): void {
    this.socket.on('close', cb)
  }
  public onData(cb: (data: Buffer) => void): void {
    this.dataCallback = cb
  }
  public abstract init(): void
  public get reading(): boolean {
    return this._reading
  }
  public set reading(val: boolean) {
    this._reading = val
  }
  public write(data: Buffer | Array<Buffer>): void {
    if (!(data instanceof Buffer)) {
      // normalize data to single buffer
      data = Buffer.concat(data)
    }
    Logger.verbose(`\n[${blue((new Date()).toISOString())}] ${black(bgGreen(' SEND '))} => ${this.socket.remoteAddress}`)
    if (data.length > 0x60) {
      Logger.debug(data.slice(0, 0x60))
      Logger.debug(`Buffer logging was truncated from ${data.length} bytes`)
    } else {
      Logger.debug(data)
    }

    if (this.isEncrypted) {
      // pad bytes to mod 4
      this.serverEncryption.encrypt(data, 0, data.length)
    }
    try {
      this.socket.write(data)
    } catch (e) {
      // the socket got disconnected unexpectedly
      Logger.error('Trying to send to a disconnected socket')
      return
    }
    if (this.disconnectAfterSend) {
      this.disconnect()
    }
  }
  public disconnect(): void {
    redis.deleteSession(this._id)
    this.socket.end()
    this.socket.destroy()
  }
  private recieveParse(): void {
    this.reading = true
    let peekBuf
    // tslint:disable-next-line:no-conditional-assignment
    while (peekBuf = this.incomingBuffer.peek(2)) {
      // seek the buffer 2 bytes at a time and to get a message length
      const messageLength = peekBuf.readUInt16LE(0)
      if (messageLength === 0 ) {
        // if the message length is 0 it means that we are probably looking at padding
        if (this.incomingBuffer.size >= 2) {
          // dequeue the data
          this.incomingBuffer.deq(2)
        } else {
          // dump the data
          this.incomingBuffer.drain()
        }
        continue
      }
      if (this.incomingBuffer.size < messageLength) {
        // message length includes size
        break
      }
      // we have a complete message to send to the router
      // pull data from buffer
      const message = this.incomingBuffer.deq(messageLength)
      Logger.verbose(`\n[${blue((new Date()).toISOString())}] ${black(bgYellow(' PRSD '))} `)
      Logger.debug(message)
      // send the buffer to the router
      this.dataCallback(message)
    }
    // done reading data
    this.reading = false
  }
  private _onData = (data: Buffer) => {
    if (this.isEncrypted) {
      this.clientEncryption.decrypt(data, 0, data.length)
    }
    // tslint:disable-next-line:max-line-length
    Logger.verbose(`\n[${blue((new Date()).toISOString())}] ${black(bgRed(' RECV '))} <= ${this.socket.remoteAddress}`)
    Logger.debug(data)

    // add data to buffer in case we got a partial packet
    this.incomingBuffer.enq(data)
    if (!this.reading) {
      // if not reading then we can parse a message from the buffer
      this.recieveParse()
    }
  }
  private dataCallback = (data: Buffer): void => { /* void */ }
}
