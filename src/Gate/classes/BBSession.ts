import { randomBufferFill } from '../../Util/Buffer'
import { BBEncryption } from './BBEncyption'
import { BBHandshake } from '../packets/BBHandshake'
import { Socket } from 'net'
import { Session } from './Session'
import { ServiceRouter } from './ServiceRouter'
import { hexy } from 'hexy'

export class BBSession extends Session {
  constructor(socket: Socket, router: ServiceRouter) {
    super(socket, router)
    this.byteBoundry = 8
  }
  public init(): void {
    // create encryption for server and client
    const clientIV = randomBufferFill(Buffer.alloc(0x30))
    const serverIV = randomBufferFill(Buffer.alloc(0x30))

    // create packet to send vectors to client
    const handshake = new BBHandshake()
    handshake.clientVector = clientIV
    handshake.serverVector = serverIV

    this.clientEncryption = new BBEncryption(clientIV)
    this.serverEncryption = new BBEncryption(serverIV)
    // send vectors to client
    this.write(handshake.buffer)

    // now the messages are encrypted
    this.isEncrypted = true
  }
}
