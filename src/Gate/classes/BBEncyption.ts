import { IEncryption } from '../types/IEncryption'
import { UInt16, UIntGetByte, UInt32 } from '../../Util/NumberTypes'
import { swapBytes16 } from '../../Util/Endianness'
import { BBTable } from './BBTable'

export class BBEncryption implements IEncryption {
  private p: Uint32Array
  private s: Array<Uint32Array>
  private key: Buffer
  constructor(iv: Buffer) {
    this.p = Uint32Array.from(BBTable.p)
    this.s = []
    BBTable.s.forEach((set, i) => {
      this.s.push(Uint32Array.from(BBTable.s[i]))
    })
    this.key = iv
    this.PSOBBInitKey()
    this.Init()
  }
  public encrypt(data: Buffer, offset: number, length: number): void {
    let a, b
    for (let i1 = 0; i1 < (length / 8); i1++) {
      a = offset + i1 * 8
      b = a + 4
      const [v1, v2] = this.EncryptBlock(
        data.readUInt32LE(a),
        data.readUInt32LE(b),
        false
      )
      data.writeUInt32LE(v1, a)
      data.writeUInt32LE(v2, b)
    }
  }
  public decrypt(data: Buffer, offset: number, length: number): void {
    let a, b
    for (let i1 = 0; i1 < (length / 8); i1++) {
      a = offset + i1 * 8
      b = a + 4
      const [v1, v2] = this.DecryptBlock(
        data.readUInt32LE(a),
        data.readUInt32LE(b),
        false
      )
      data.writeUInt32LE(v1, a)
      data.writeUInt32LE(v2, b)
    }
  }
  public Init(): void {
    // PSOBB p scramble
    this.PRotate()
    this.PMix()
    // Standard blowfish init, use full rounds on encrypt calls
    let v1 = 0, v2 = 0
    for (let i1 = 0; i1 < 18; i1 += 2) {
      [v1, v2] = this.EncryptBlock(v1, v2, true)
      this.p[i1] = v1
      this.p[i1 + 1] = v2
    }
    for (let  i1 = 0; i1 < 4; i1++) {
      for (let i2 = 0; i2 < 256; i2 += 2) {
        [v1, v2] = this.EncryptBlock(v1, v2, true)
        this.s[i1][i2] = v1
        this.s[i1][i2 + 1] = v2
      }
    }
  }
  /**
   * XOR key before calling Cipher.Init, PSOBB does it but since it's
   * not part of the Init procedure (not even on BB) it goes separated
   * and the user is in charge of xoring the key before being used.
   * @param key
   */
  private PSOBBInitKey(): void {
    for (let i1 = 0; i1 < 48; i1 += 3) {
      this.key[i1] = (this.key[i1] ^ 0x19) >>> 0
      this.key[i1 + 1] = (this.key[i1 + 1] ^ 0x16) >>> 0
      this.key[i1 + 2] = (this.key[i1 + 2] ^ 0x18) >>> 0
    }
  }
  private PRotate(): void {
    for (let i1 = 0; i1 < 18; i1++) {
      const pT = swapBytes16(UInt16(this.p[i1]))
      this.p[i1] = ((((this.p[i1] >>> 16) ^ pT) << 16) >>> 0) + pT
    }
  }
  private PMix(): void {
    for (let i1 = 0; i1 < 18; i1++) {
      const ko = (i1 % 12) * 4
      const k = this.key.readInt32BE(ko)
      this.p[i1] = (this.p[i1] ^ k) >>> 0
    }
  }
  private EncryptBlock(L: number, R: number, standard: boolean): [number, number] {
    for (let i1 = 0; i1 < (standard ? 16 : 4); i1 += 2) {
      L = (L ^ this.p[i1]) >>> 0
      R = (R ^ this.F(L)) >>> 0
      R = (R ^ this.p[i1 + 1]) >>> 0
      L = (L ^ this.F(R)) >>> 0
    }
    L = (L ^ this.p[(standard ? 16 : 4) + 0]) >>> 0
    R = (R ^ this.p[(standard ? 16 : 4) + 1]) >>> 0
    return [R, L]
  }
  private DecryptBlock(L: number, R: number, standard: boolean): [number, number] {
    for (let i1 = (standard ? 16 : 4); i1 > 0; i1 -= 2) {
      L = (L ^ this.p[i1 + 1]) >>> 0
      R = (R ^ this.F(L)) >>> 0
      R = (R ^ this.p[i1]) >>> 0
      L = (L ^ this.F(R)) >>> 0
    }
    L = (L ^ this.p[1]) >>> 0
    R = (R ^ this.p[0]) >>> 0
    return [R, L]
  }
  private F(x: number): number {
    let e = this.s[0][UIntGetByte(x, 3)]
    e = UInt32(e + this.s[1][UIntGetByte(x, 2)])
    e = (e ^ this.s[2][UIntGetByte(x, 1)]) >>> 0
    e = UInt32(e + this.s[3][UIntGetByte(x, 0)])
    return e
  }
}
