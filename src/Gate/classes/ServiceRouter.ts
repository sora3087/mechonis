import { hexy } from 'hexy'
import { Server } from 'socket.io'
import { NetworkMessage } from '../../Network/NetworkMessage'
import Logger from '../../Util/Logger'

export class ServiceRouter {
  private routes: { [command: number]: string } = {}
  constructor(private io: Server) {}
  public registerRoutes(serviceName: string, commands: Array<number>): void {
    commands.forEach((command) => {
      this.routes[command] = serviceName
    })
  }
  public routeData(sessionId: string, data: Buffer): void {
    const channel = this.getChannel(data.readUInt16BE(2)) || this.getChannel(data[2])
    if (channel) {
      const msg: NetworkMessage = {
        gate: '',
        session: sessionId,
        data
      }
      this.io.to(channel).emit('message', msg)
    } else {
      Logger.warn(`Message with command 0x${data.readUInt16BE(2).toString(16)} went unhandled`)
      Logger.debug('\r\n' + hexy(data))
    }
  }
  private getChannel(command: number): string | undefined {
    return this.routes[command]
  }
}
