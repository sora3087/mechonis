import { Server, Socket} from 'net'
import { Server as Io, Socket as IoSocket } from 'socket.io'
import { EncryptionType } from '../../Network/EncryptionType'
import { IServiceRegistration } from '../../Network/IServiceRegistration'
import { NetworkMessage } from '../../Network/NetworkMessage'
import { redis } from '../../Network/RedisClient'
import Logger from '../../Util/Logger'
import { BBSession } from './BBSession'
import { PSOSession } from './PSOSession'
import { ServiceRouter } from './ServiceRouter'
import { Session } from './Session'
import * as readline from 'readline'

export class ServerManager {
  private servers: { [id: number]: Server } = {}
  private sessions: { [uuid: string]: Session } = {}
  constructor(io: Io, private router: ServiceRouter) {
    // when a service connects bind events
    io.on('connection', socket => {
      Logger.info('io socket connection from ' + socket.id)
      socket.on(
        'service-register',
        (registration: IServiceRegistration, ack: () => void) =>
          this.register(registration, ack, socket)
      )
      socket.on('service-respond', this.respond)
      socket.on('service-redirect', this.redirect)
    })
    readline.emitKeypressEvents(process.stdin)
    process.stdin.setRawMode(true)
    process.stdin.on('keypress', (str, key) => {
      if (key.shift && key.name === 'd') {
        console.log('force disconnecting all clients')
        Object.keys(this.sessions).forEach(sid => {
          this.sessions[sid].disconnect()
        })
      } else if (key.ctrl && key.name === 'c') {
        process.emit('beforeExit', 0)
      }
    })
  }
  public register = (registration: IServiceRegistration, ack: () => void, socket: IoSocket): void => {
    Logger.info('Registration from ' + registration.name)
    socket.join(registration.name)
    this.router.registerRoutes(registration.name, registration.commands)
    Logger.info(registration.ports)
    Object.keys(registration.ports).forEach((port) => {
      const portNum = parseInt(port, 10)
      this.registerPort(portNum, registration.ports[portNum])
    })
    ack()
  }
  public registerPort(port: number, options: { type: EncryptionType, redirect: boolean } ): void {
    if (this.servers[port]) {
      Logger.warn(`server already registered on port: ${port}`)
      return
    }
    const server =  new Server()
    server.on('listening', () => {
      Logger.info('Opened a new port ' + port)
    })
    server.on('connection', (socket: Socket) => {
      Logger.info(`accepted a connection from ${socket.remoteAddress} on port ${port}`)
      socket.on('close', () => Logger.info(`socket to ${socket.remoteAddress} closed`))
      let session: Session
      if (options.type === EncryptionType.BB) {
        session = new BBSession(socket, this.router)
      } else {
        session = new PSOSession(socket, this.router)
      }
      session.onData((data: Buffer) => {
        if (options.redirect) {
          // append flag for a base port that needs redirect
          data[3] = data[3] | 1 << 7
        }
        if (data[2] === 0x05) {
          session.disconnect()
        }
        // still send the disconnect to other services so they can clean up their sessions
        this.router.routeData(session.id, data)
      })
      socket.on('error', (err) => {
        Logger.info(`Session ${session.id} error`)
        Logger.error(err)
        redis.deleteSession(session.id).then(() => {
          this.sessions[session.id] = undefined
          delete this.sessions[session.id]
        })
      })
      // session is initialized and handshake is sent by the session itselfw
      session.onClosed(() => {
        redis.deleteSession(session.id).then(() => {
          Logger.info(`Session ${session.id} closed`)
          this.sessions[session.id] = undefined
          delete this.sessions[session.id]
        })
      })
      this.sessions[session.id] = session
    })
    this.servers[port] = server
    server.listen(port)
  }
  public respond = (msg: NetworkMessage): void => {
    // this is a handler for when a service sends a message to a client
    // get client from sessions and write data to it
    const client = this.sessions[msg.session]
    if (client) {
      client.disconnectAfterSend = msg.disconnect || false
      client.write(msg.data)
    }
  }
  public redirect = (msg: NetworkMessage): void => {
    this.router.routeData(msg.session, msg.data)
  }
}
