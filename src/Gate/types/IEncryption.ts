export interface IEncryption {
  encrypt: (data: Buffer, offset: number, length: number) => void
  decrypt: (data: Buffer, offset: number, length: number) => void
}
