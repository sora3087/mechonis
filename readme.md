# ⚠️ This readme is outdated
Mechonis has gone through numerous extensive architectural changes and a new readme is coming to go with those changes.

# Mechonis
Portable TypeScript based PSOBB game server suite

Massive thanks to __Soly__ for his invaluable information with navigating the pitfalls of coding a PSOBB server

> This code currently only supports serving patch files

## Features
### Patch
- Patch Channels
  - Client can select a set of files to track and switch on the fly
  - Channels include a dependency system to include files from other channels
- Optional Files
  - Files on the server can be configured as "optional" and will not be overwritten on client
- Templated welcome text
  - Load a template file instead of a plain text
  - Write and run dynamic welcome messages

## Upcoming Features
- Better Logging
  - Log to file
  - Log verbosity level
  - Log by event level
- Use gitignore style file syntax for optional file manifests

## Building
```sh
cd <mechonis>
yarn
npm run build
```

## Setup
### Config files
The config file root location on windows is `%USERPROFILE%\.config\mechonis`.
There is one folder within the config root for each service and a `config.json` in each.

Upon first run, the server will create the folder and file if it does not exist with all available properties and their default values in it.

Defaults:
```json
{
  "ipAddress": "127.0.0.1",
  "publicIP": "",
  "basePort": 11000,
  "patchRoot": "./patches",
  "welcomeTemplatePath": "./welcome.ejs",
  "maxFileMemory": 1073741824,
  "uploadRate": 0,
  "maxClients": 0
}
```

### Patch config options

- **ipAddress**<br>
This is the network address to bind to.
- **publicIP**<br>
This is the public address to redirect to once login has succeeded. You will only need to set this if your bound ip is different from where your clients are connecting to.
- **basePort**<br>
The base port to bind to. Don't change this unless you know what you are doing.
- **patchRoot**<br>
This is used to specify where the root path of [patch channel files](#patch-channels).
- **welcomeTemplatePath**<br>
This is the path of the [template](#templated-welcome-text) file to load for processing the welcome text.
- **maxFileMemory** <br>
This is a limit used to limit how much file data is buffered into memory when starting the server. **This is number only represents the amount of file data and not the footprint of the node service and script itself.**
- **uploadRate** <br>
Used to limit how fast each client can download from the server.
- **maxClients** <br>
Used to limit how many simultaneous clients can connect to the server.

## Patch Channels
Patch channels is a feature exclusive to Mechonis.
It allows the server admin to create separate patch channels and allows the user to switch between them at whim. It functions within the limitations of the patch server protocol.

To create a new patch channel simply add a new directory inside the [patchRoot](#patchRoot) directory with the name of the channel you would like to add. **The `master` channel is reserved and mandatory. You must create at least this channel folder to serve files from.** The `master` channel is the fallback for anyone without a selcted channel or a specified channel that doesn't exits on the server.

A user can switch their active branch by simply text editing the `.channel` file in their client's root directory. The user needs to enter the string exactly as it is used on the server. If the channel name differs in any way (including whitespace) the checksum will fail and they will default to `master`. This also means that the user can only specify one channel at a time.

**Warning about Channels:**
- This feature is not stress tested! Please report any errors you run into!
- Since there is no command to tell the PSOBB client to delete a file, there is unfortunately no way to clean up "extra" files between channel switches.

### Example folder structure
```
patchRoot/
  + master
  + beta
  + updated-hud
  + replaced-music
  ` all improvements
```
This will create five channels `master`, `beta`, `updated-hud`, `replaced-music`, and `all-improvements`. The user can now select any of these by writing it in their `.channel` file.

## Channel Dependencies (.depends)
Why store all the redundant files in another channel if you only have a few different or new files in it? Well you don't have to!

- Adding a depenency to a channel will import all its files.
- Files depended on will be imported before any files included in the current directory.
- Files that are the same in the new channel will override the contents of any matched files from depended channels.
- Multiple channels can be depended on simultaneously
- Channels can depend on other channels that themselves depend on others.
- `master` channel will ignore any dependencies specified
- There can be more than one channel without any dependencies

To add channel dependencies simply create a `.depends` file in the root of the channel with one channel name per line.

### Example
```
patchRoot/
  + master
  + beta
  + updated-hud
  |  + .depends // "master" 
  |  ` ...hud files
  + replaced-music
  |  + .depends // "master" 
  |  ` ...music files
  ` all improvements
     ` .depends // "updated-hud\nreplaced-music" 
```

### Bonus feature: Virtual Channels

If you want to add a channel that only depends on other but does not include files of it own you can do that by adding a `<channel name>.depends` in the [patchRoot](#patchRoot). Using the example above we can simply add a file to the patchRoot called `all-improvements.depends` and put the dependent channels in it.


## Optional patch files (.optional)
Optional files allow the patch server to deliver a file if it is missing but do not overwrite if the checksum is incorrect. To configure these, add a `.optional` file in the root of the channel. Specify a full relative path for each file on a new line to set it as optional.
- Only overloaded files can be marked as optional
- Virtual channels can not use `.optional`

### Example
```
patchRoot/
  ...
  + updated-hud
  |  + .depends // "master" 
  |  + .optional // "data/f256_hyouji.prs"
  |  ` ...hud files
  ...
```

## Templated welcome text
The welcome text is actually templated by ejs ([read more](https://ejs.co/#docs)). Anything you can do with EJS to dynamically assemble html or text you can do with the welcome template. **This feature has not been stress tested!** There are bound to be problems.

### Color codes
To make writing them easier you can use html like tags to insert color codes into the welcome text.
<br>Like so:
```html
<color.[colorName]></color.[colorName]>
```

### Session data
Its basic, for now only look up the username but the template is passed a model that includes an object `session` which has a propert `userName`.

### Basic example
```html
// welcome.ejs
// output relative time of day dynamically, inserts user name, and colors server name
<%
  let period = 'evening';
  let hour = (new Date()).getHours();
  if (hour >= 8 && hour <= 12){
    period = 'morning';
  } else if (hour > 12 && hour < 17) {
    period = 'afternoon';
  }
  if (session.userName == '') {
    session.userName = 'Player';
  }
-%>
Good <%=period%> <%= session.userName %>, Welcome to <color.blue>Mechonis <color.red>Patch</color.red> Server</color.blue>
```

## Running
```sh
cd <mechonis>
npm run patch
```
Tested working with node v12.13.0